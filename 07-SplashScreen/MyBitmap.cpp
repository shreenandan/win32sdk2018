/*
Assignment No: 7
Create splash screen with bitmap resource
*/

// headers
#include<windows.h>
#include "MyBitmap.h"

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
HINSTANCE theInstance = NULL; // NOTE: Required when Loading Bitmap in WM_CREATE

// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("ShreeSplashScreen");

	// code
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc; // NOTE
	wndclass.hInstance = hInstance;
	theInstance = hInstance; // Save instance
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindow(szAppName,
		TEXT("Shree Splash Screen"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		817,
		640,
		NULL,
		NULL,
		hInstance,
		NULL);
	//IDI_APPLICATION 
	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	// message loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	PAINTSTRUCT ps;
	RECT rc;
	static HBITMAP hBitmap = NULL; // NOTE: Needs to be maintained across calls
	HDC hdcBitmap = NULL;
	HGDIOBJ rt_gdi1;
	int rt_getObj;
	BOOL rt_copyDc;
	BOOL rt_Del;
	BITMAP bmp;
	int rt_hBmp;
	int rt_h;

	// code
	switch (iMsg)
	{
	case WM_CREATE:
		hBitmap = LoadBitmap(theInstance, MAKEINTRESOURCE(MY_BITMAP));
		if (hBitmap == NULL)
		{
			MessageBox(hwnd, TEXT("Error loading bitmap!!"), "NULL", MB_OK | MB_ICONERROR);
			DestroyWindow(hwnd);
		}
		break;

	case WM_PAINT:
		hdc = BeginPaint(hwnd, &ps);
		hdcBitmap = CreateCompatibleDC(hdc);
		rt_gdi1 = SelectObject(hdcBitmap, hBitmap); //hgdiobj
		rt_getObj = GetObject(hBitmap, sizeof(BITMAP), (LPVOID*)&bmp);
		rt_copyDc = BitBlt(hdc, 0, 0, bmp.bmWidth, bmp.bmHeight, hdcBitmap, 0, 0, SRCCOPY);
		
		/** 
		//rt_copyDc = StretchBlt(hdc, 0, 0, bmp.bmWidth, bmp.bmHeight, hdcBitmap, 0, 0, 800, 700, SRCCOPY); // Can stretch to specified size
		
		//rt_h = GetDeviceCaps(hdc, RASTERCAPS); // | RC_BITBLT
		//rt_hBmp = GetDeviceCaps(hdcBitmap, RASTERCAPS); // | RC_BITBLT
		//rt_copyDc = PatBlt(hdc, 0, 0, 800, 600, PATCOPY); // Only compatible devices can show
		**/

		rt_gdi1 = SelectObject(hdc, hBitmap);
		//rt_Del = DeleteDC(hdcBitmap); // NOTE: This will retain the image as "hdc" is maintained by deleting hdcBitmap

		rt_Del = DeleteDC(hdc); // NOTE: This will delete hdc and erase the image
		EndPaint(hwnd, &ps);
		break;

	case WM_DESTROY:
		rt_Del = DeleteObject(hBitmap);
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}
