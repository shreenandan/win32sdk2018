#define UNICODE
#include <Windows.h>
#include "ChemGayLussacCFServer.h"

// class declarations
class CChemGayLussac : public IChemGayLussac_Temperature, IChemGayLussac_Pressure
{
private:
	long m_cRef;
public:
	CChemGayLussac(void);
	~CChemGayLussac(void);

	// IUnknown specific method declarations (inherited)
	HRESULT __stdcall QueryInterface(REFIID, void **);
	ULONG __stdcall AddRef(void);
	ULONG __stdcall Release(void);

	// IChemGayLussac_Temperature specific methods
	HRESULT __stdcall CalcInitTemperature(double, double, double, double *);
	HRESULT __stdcall CalcFinalTemperature(double, double, double, double *);

	// IChemGayLussac_Pressure specific methods
	HRESULT __stdcall CalcInitPressure(double, double, double, double *);
	HRESULT __stdcall CalcFinalPressure(double, double, double, double *);
};

class CChemGayLussacClassFactory : public IClassFactory
{
private:
	long m_cRef;
public:
	CChemGayLussacClassFactory(void);
	~CChemGayLussacClassFactory(void);

	// IUnknown specific method declarations (inherited)
	HRESULT __stdcall QueryInterface(REFIID, void **);
	ULONG __stdcall AddRef(void);
	ULONG __stdcall Release(void);

	// IUnknown specific method declarations (inherited)
	HRESULT __stdcall CreateInstance(IUnknown *, REFIID, void **);
	HRESULT __stdcall LockServer(BOOL);
};

// global variable declarations
long glNoOfActiveComponents = 0;
long glNoOfServerLocks = 0; // no of locks on this dll

BOOL WINAPI DllMain(HINSTANCE hDll, DWORD dwReason, LPVOID reserved)
{
	// code
	switch (dwReason)
	{
		// Why Sir wrote only these two? Remember this thing
	case DLL_PROCESS_ATTACH:
	case DLL_PROCESS_DETACH:
		break;

	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
		break;
	}

	return (TRUE);
}

// CChemGayLussac Implementation
CChemGayLussac::CChemGayLussac(void)
{
	m_cRef = 1; // hardcoded initialization to anticipate possible failure of QueryInterface()
	InterlockedIncrement(&glNoOfActiveComponents);
}

CChemGayLussac::~CChemGayLussac(void)
{
	InterlockedDecrement(&glNoOfActiveComponents);
}

// Implementation of IUnknown's 3 methods
HRESULT CChemGayLussac::QueryInterface(REFIID riid, void **ppv) // ptr to ptr to void
{
	// code
	if (riid == IID_IUnknown) // Order is important, Adjustor Thunk
		*ppv = static_cast<IChemGayLussac_Temperature *>(this);
	else if (riid == IID_IChemGayLussac_Temperature)
		*ppv = static_cast<IChemGayLussac_Temperature *>(this);
	else if (riid == IID_IChemGayLussac_Pressure)
		*ppv = static_cast<IChemGayLussac_Pressure *>(this);
	else
	{
		*ppv = NULL;
		return (E_NOINTERFACE);
	}

	reinterpret_cast<IUnknown *>(*ppv)->AddRef();
	return (S_OK);
}

ULONG CChemGayLussac::AddRef(void)
{
	InterlockedIncrement(&m_cRef);
	return (m_cRef);
}

ULONG CChemGayLussac::Release(void)
{
	InterlockedDecrement(&m_cRef);

	// logic
	if (m_cRef == 0)
	{
		delete(this);
		return (0);
	}

	return (m_cRef);
}

// Implementation IChemGayLussac's methods
// Gas Equation: Pi / Ti = Pf / Tf
HRESULT CChemGayLussac::CalcInitPressure(double initTemp, double finalPress, double finalTemp, double *pInitPress)
{
	*pInitPress = (finalPress * initTemp) / finalTemp;
	return (S_OK);
}

HRESULT CChemGayLussac::CalcInitTemperature(double initPress, double finalPress, double finalTemp, double *pInitTemp)
{
	*pInitTemp = (initPress * finalTemp) / finalPress;
	return (S_OK);
}

HRESULT CChemGayLussac::CalcFinalPressure(double initPress, double initTemp, double finalTemp, double *pFinalPress)
{
	*pFinalPress = (initPress * finalTemp) / initTemp;
	return (S_OK);
}

HRESULT CChemGayLussac::CalcFinalTemperature(double initPress, double initTemp, double finalPress, double *pFinalTemp)
{
	*pFinalTemp = (finalPress * initTemp) / initPress;
	return (S_OK);
}


// Class Factory Implementation
CChemGayLussacClassFactory::CChemGayLussacClassFactory(void)
{
	m_cRef = 1; // hardcoded initialization to anticipate possible failure of QueryInterface()
}

CChemGayLussacClassFactory::~CChemGayLussacClassFactory(void)
{

}

// Implementation of IUnknown's 3 methods
HRESULT CChemGayLussacClassFactory::QueryInterface(REFIID riid, void **ppv) // ptr to ptr to void
{
	// code
	if (riid == IID_IUnknown) // Order is important, Adjustor Thunk
		*ppv = static_cast<IClassFactory *>(this);
	else if (riid == IID_IClassFactory)
		*ppv = static_cast<IClassFactory *>(this);
	else
	{
		*ppv = NULL;
		return (E_NOINTERFACE);
	}

	reinterpret_cast<IUnknown *>(*ppv)->AddRef();
	return (S_OK);
}

ULONG CChemGayLussacClassFactory::AddRef(void)
{
	InterlockedIncrement(&m_cRef);
	return (m_cRef);
}

ULONG CChemGayLussacClassFactory::Release(void)
{
	InterlockedDecrement(&m_cRef);

	// logic
	if (m_cRef == 0)
	{
		delete(this);
		return (0);
	}

	return (m_cRef);
}


// Implementation of IClassFacory's 2 methods
HRESULT CChemGayLussacClassFactory::CreateInstance(IUnknown *pUnkOuter, REFIID riid, void **ppv)
{
	CChemGayLussac *pCChemGayLussac = NULL;
	HRESULT hr;

	if (pUnkOuter != NULL)
		return (CLASS_E_NOAGGREGATION);

	// create the instance of component
	pCChemGayLussac = new CChemGayLussac;
	if (pCChemGayLussac == NULL)
	{
		return (E_OUTOFMEMORY);
	}

	// get the requested interface
	hr = pCChemGayLussac->QueryInterface(riid, ppv);
	pCChemGayLussac->Release(); // anticipate possible failure of QueryInterface()
	return (hr);
}

HRESULT CChemGayLussacClassFactory::LockServer(BOOL fLock)
{
	if (fLock)
		InterlockedIncrement(&glNoOfServerLocks);
	else
		InterlockedDecrement(&glNoOfServerLocks);

	return (S_OK);
}


// Implementation of gloabl Exported functions from this DLL
// extern "C" required from 2000 onwards
extern "C" HRESULT __stdcall DllGetClassObject(REFCLSID rclsid, REFIID riid, void **ppv)
{
	CChemGayLussacClassFactory *pCChemGayLussacClassFactory = NULL;
	HRESULT hr;

	if (rclsid != CLSID_CChemGayLussac)
		return (CLASS_E_CLASSNOTAVAILABLE);

	// create class factory
	pCChemGayLussacClassFactory = new CChemGayLussacClassFactory;
	if (pCChemGayLussacClassFactory == NULL)
		return (E_OUTOFMEMORY);

	hr = pCChemGayLussacClassFactory->QueryInterface(riid, ppv);
	pCChemGayLussacClassFactory->Release(); // anticipate possible failure of QueryInterface()

	return (hr);
}

extern "C" HRESULT __stdcall DllCanUnloadNow(void)
{
	if (glNoOfActiveComponents == 0 && glNoOfServerLocks == 0)
		return (S_OK);
	else
		return (S_FALSE);
}
