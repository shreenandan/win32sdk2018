// headers
#define UNICODE
#include<windows.h>
#include "ChemGayLussacCFServer.h"
#include <wchar.h>
#include "ChemGayLussacCFWrapper.h"

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// global variable declarations
IChemGayLussac_Temperature *pIChemGayLussac_Temperature = NULL;
IChemGayLussac_Pressure *pIChemGayLussac_Pressure = NULL;

// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("ChemGayLussacCFDllClient");
	HRESULT hr; // New wrt COM interface

	// COM Initialization
	hr = CoInitialize(NULL);
	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("COM library can not be initialized.\nProgram will now exit."), TEXT("Program Error"), MB_OK);
		exit(0);
	}

	// code
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc; // NOTE
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindow(szAppName,
		TEXT("ChemGayLussacCFDllClient"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	// message loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	// COM UnInitialization
	CoUninitialize();

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declarations
	void SafeInterfaceRelease(void);
	// Variable declarations
	HRESULT hr;
	double initPress, initTemp, finalPress, finalTemp;
	TCHAR str[255];
	bool successful;
	static HMODULE hLib = NULL;
	typedef double(*pfnChemFunc)(double, double, double);
	typedef bool(*pfnInitFunc)();
	typedef void(*pfnUninitFunc)();
	static pfnChemFunc chemFunc = NULL;
	static pfnInitFunc initFunc = NULL;
	static pfnUninitFunc uninitFunc = NULL;

	// code
	switch (iMsg)
	{
		// Code For Without Wrapper, Direct Server usage
		/*
	case WM_CREATE:
		hr = CoCreateInstance(CLSID_CChemGayLussac, NULL, CLSCTX_INPROC_SERVER, IID_IChemGayLussac_Temperature, (void **)&pIChemGayLussac_Temperature);
		if (FAILED(hr))
		{
			MessageBox(hwnd, TEXT("IChemGayLussac_Temperature interface can not be obtained."), TEXT("Error"), MB_OK);
			DestroyWindow(hwnd);
		}

		initPress = 125;
		initTemp = 200;
		finalPress = 100;
		pIChemGayLussac_Temperature->CalcFinalTemperature(initPress, initTemp, finalPress, &finalTemp);
		swprintf(str, 100 , TEXT("Final Temp = %.2lf when Pi = %.2lf, Ti = %.2lf, Fp = %.2lf"), finalTemp, initPress, initTemp, finalPress);
		MessageBox(hwnd, str, TEXT("Final Temp"), MB_OK);

		hr = pIChemGayLussac_Temperature->QueryInterface(IID_IChemGayLussac_Pressure, (void **)&pIChemGayLussac_Pressure);
		if (FAILED(hr))
		{
			MessageBox(hwnd, TEXT("IChemGayLussac_Pressure interface can not be obtained."), TEXT("Error"), MB_OK);
			DestroyWindow(hwnd);
		}
		// as IChemGayLussac_Temperature is not needed onwards, release it
		pIChemGayLussac_Temperature->Release();
		pIChemGayLussac_Temperature = NULL; // make released interface NULL

		pIChemGayLussac_Pressure->CalcFinalPressure(initPress, initTemp, finalTemp, &finalPress);
		pIChemGayLussac_Pressure->Release();
		pIChemGayLussac_Pressure = NULL;
		swprintf(str, 100, TEXT("Final Press = %.2lf when Pi = %.2lf, Ti = %.2lf, Ft = %.2lf"), finalPress, initPress, initTemp, finalTemp);
		MessageBox(hwnd, str, TEXT("Final Press"), MB_OK);

		SafeInterfaceRelease(); // Verify
		break;
		*/

		// Code For Wrapper
	case WM_CREATE:
		hLib = LoadLibrary(TEXT("ChemGayLussacCFWrapper.dll")); // Step 1 Load DLL
		if (hLib == NULL)
		{
			MessageBox(hwnd, TEXT("ChemGayLussacCFWrapper.dll not found!!"), L"Error", MB_OK | MB_ICONERROR);
			DestroyWindow(hwnd);
		}

		initFunc = (pfnInitFunc)GetProcAddress(hLib, "InitializeChemistryWrapper");
		successful = initFunc();
		if (!successful)
		{
			MessageBox(hwnd, TEXT("IChemGayLussac_Temperature interface can not be obtained."), TEXT("Error"), MB_OK);
			FreeLibrary(hLib); // Step 4
			DestroyWindow(hwnd);
		}

		initPress = 125;
		initTemp = 200;
		finalPress = 100;
		chemFunc = (pfnChemFunc)GetProcAddress(hLib, "GetFinalTemperature"); // Step 2
		finalTemp = chemFunc(initPress, initTemp, finalPress); // Step 3 Explicitly linked custom DLL function call
		if (finalTemp == -1)
		{
			MessageBox(hwnd, TEXT("Something went wrong when calculating Final Temperature."), TEXT("Error"), MB_OK);
		}

		swprintf(str, 100, TEXT("Final Temp = %.2lf when Pi = %.2lf, Ti = %.2lf, Fp = %.2lf"), finalTemp, initPress, initTemp, finalPress);
		MessageBox(hwnd, str, TEXT("Final Temp"), MB_OK);

		chemFunc = (pfnChemFunc)GetProcAddress(hLib, "GetFinalPressure"); // Step 2
		finalPress = 0;
		finalPress = chemFunc(initPress, initTemp, finalTemp); // Step 3 Explicitly linked custom DLL function call
		if (finalPress == -1)
		{
			MessageBox(hwnd, TEXT("Either IChemGayLussac_Pressure interface can not be obtained or Something went wrong when calculating Final Pressure."), TEXT("Error"), MB_OK);
		}

		swprintf(str, 100, TEXT("Final Press = %.2lf when Pi = %.2lf, Ti = %.2lf, Ft = %.2lf"), finalPress, initPress, initTemp, finalTemp);
		MessageBox(hwnd, str, TEXT("Final Press"), MB_OK);

		uninitFunc = (pfnUninitFunc)GetProcAddress(hLib, "UninitializeChemistryWrapper");
		uninitFunc();
		break;

	case WM_DESTROY:
		FreeLibrary(hLib); // Step 4
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void SafeInterfaceRelease(void)
{
	if (pIChemGayLussac_Temperature)
	{
		pIChemGayLussac_Temperature->Release();
		pIChemGayLussac_Temperature = NULL;
	}
	if (pIChemGayLussac_Pressure)
	{
		pIChemGayLussac_Pressure->Release();
		pIChemGayLussac_Pressure = NULL;
	}
}
