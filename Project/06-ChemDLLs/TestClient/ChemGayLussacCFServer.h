class IChemGayLussac_Temperature : public IUnknown
{
public:
	virtual HRESULT __stdcall CalcInitTemperature(double initPress, double finalPress, double finalTemp, double *pInitTemp) = 0; // pure virtual
	virtual HRESULT __stdcall CalcFinalTemperature(double initPress, double initTemp, double finalPress, double *pFinalTemp) = 0; // pure virtual
};

class IChemGayLussac_Pressure : public IUnknown
{
public:
	virtual HRESULT __stdcall CalcInitPressure(double initTemp, double finalPress, double finalTemp, double *pInitPress) = 0; // pure virtual
	virtual HRESULT __stdcall CalcFinalPressure(double initPress, double initTemp, double finalTemp, double *pFinalPress) = 0; // pure virtual
};

// CLSID of CoClass CChemGayLussac // {DBA68DAB-F1FC-4640-B4C4-4C322C12291E}
const CLSID CLSID_CChemGayLussac = { 0xdba68dab, 0xf1fc, 0x4640, { 0xb4, 0xc4, 0x4c, 0x32, 0x2c, 0x12, 0x29, 0x1e } };

// interface Temperature // {0DF3C602-750B-4B86-B333-427ECEB5208B}
const IID IID_IChemGayLussac_Temperature = { 0xdf3c602, 0x750b, 0x4b86, { 0xb3, 0x33, 0x42, 0x7e, 0xce, 0xb5, 0x20, 0x8b } };

// interface Pressure // {DBE8400E-1375-47C5-AFC9-1100E5463A08}
const IID IID_IChemGayLussac_Pressure = { 0xdbe8400e, 0x1375, 0x47c5, { 0xaf, 0xc9, 0x11, 0x0, 0xe5, 0x46, 0x3a, 0x8 } };
