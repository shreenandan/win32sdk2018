extern "C" bool InitializeChemistryWrapper();
extern "C" void UninitializeChemistryWrapper();
extern "C" double GetInitTemperature(double initPress, double finalPress, double finalTemp);
extern "C" double GetFinalTemperature(double initPress, double initTemp, double finalPress);
extern "C" double GetInitPressure(double initTemp, double finalPress, double finalTemp);
extern "C" double GetFinalPressure(double initPress, double initTemp, double finalTemp);