#include <Windows.h>
#include "ChemGayLussacCFWrapper.h"
#include "ChemGayLussacCFServer.h"

// global variable declarations
IChemGayLussac_Temperature *pIChemGayLussac_Temperature = NULL;
IChemGayLussac_Pressure *pIChemGayLussac_Pressure = NULL;

BOOL WINAPI DllMain(HMODULE hDll, DWORD dwReason, LPVOID lpReserved)
{
	// Code
	switch (dwReason)
	{
	case DLL_PROCESS_ATTACH:
	case DLL_PROCESS_DETACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
		break;
	}

	return TRUE; // Important
}

// NOTE: There are 2 possible ways to use Class Factory Server in Wrapper
// 1] Way 1 Call CoCreateInstance() every time in each chemistry function
// by passing riid any Required Interface. Can be Released then and there in the same function itself.
// 2] Way 2 Call CoCreateInstance() only once in Initialize function
// by passing riid fixed Primary Interface. Here maintain primary interface object till last.
// QI other interfaces on this primary. Non primary can be released immediately after use.

// Using Way 2

// Decision: Temperature will be primary interface. First as per Adjustor Thunk rule.
// It will be maintained till last. Other interfaces obtained by QI on Temperature.
// Can be released during intermediate processing.
extern "C" bool InitializeChemistryWrapper()
{
	HRESULT hr;
	hr = CoCreateInstance(CLSID_CChemGayLussac, NULL, CLSCTX_INPROC_SERVER, IID_IChemGayLussac_Temperature, (void **)&pIChemGayLussac_Temperature);
	if (FAILED(hr))
	{
		return false;
	}
	return true;
}

extern "C" double GetInitTemperature(double initPress, double finalPress, double finalTemp)
{
	// One level of safety. Avoid null reference exception on QI call.
	// Will come into picture if wrapper consumer didn't call Initialize.
	// Second case - Anytime earlier if Initialize failed.
	if (!pIChemGayLussac_Temperature)
	{
		if (!InitializeChemistryWrapper())
			return -1;
	}

	double initTemp = -1;
	pIChemGayLussac_Temperature->CalcInitTemperature(initPress, finalPress, finalTemp, &initTemp);
	return initTemp;
}

extern "C" double GetFinalTemperature(double initPress, double initTemp, double finalPress)
{
	// One level of safety. Avoid null reference exception on QI call.
	// Will come into picture if wrapper consumer didn't call Initialize.
	// Second case - Anytime earlier if Initialize failed.
	if (!pIChemGayLussac_Temperature)
	{
		if (!InitializeChemistryWrapper())
			return -1;
	}
	
	double finalTemp = -1;
	pIChemGayLussac_Temperature->CalcFinalTemperature(initPress, initTemp, finalPress, &finalTemp);
	return finalTemp;
}

extern "C" double GetInitPressure(double initTemp, double finalPress, double finalTemp)
{
	// One level of safety. Avoid null reference exception on QI call.
	// Will come into picture if wrapper consumer didn't call Initialize.
	// Second case - Anytime earlier if Initialize failed.
	if (!pIChemGayLussac_Temperature)
	{
		if (!InitializeChemistryWrapper())
			return -1;
	}

	HRESULT hr;
	hr = pIChemGayLussac_Temperature->QueryInterface(IID_IChemGayLussac_Pressure, (void**)&pIChemGayLussac_Pressure);
	if (FAILED(hr))
	{
		return -1;
	}

	double initPress = -1;

	pIChemGayLussac_Pressure->CalcInitPressure(initTemp, finalPress, finalTemp, &initPress);
	pIChemGayLussac_Pressure->Release();
	pIChemGayLussac_Pressure = NULL;

	return initPress;
}

extern "C" double GetFinalPressure(double initPress, double initTemp, double finalTemp)
{
	// One level of safety. Avoid null reference exception on QI call.
	// Will come into picture if wrapper consumer didn't call Initialize.
	// Second case - Anytime earlier if Initialize failed.
	if (!pIChemGayLussac_Temperature)
	{
		if (!InitializeChemistryWrapper())
			return -1;
	}

	HRESULT hr;
	hr = pIChemGayLussac_Temperature->QueryInterface(IID_IChemGayLussac_Pressure, (void**)&pIChemGayLussac_Pressure);
	if (FAILED(hr))
	{
		return -1;
	}

	double finalPress = -1;

	pIChemGayLussac_Pressure->CalcFinalPressure(initPress, initTemp, finalTemp, &finalPress);
	pIChemGayLussac_Pressure->Release();
	pIChemGayLussac_Pressure = NULL;

	return finalPress;
}

extern "C" void UninitializeChemistryWrapper()
{
	// SafeInterfaceRelease
	if (pIChemGayLussac_Temperature)
	{
		pIChemGayLussac_Temperature->Release();
		pIChemGayLussac_Temperature = NULL;
	}
	// Ideally should be null considering normal flow. If not, on safer side release explicitly.
	if (pIChemGayLussac_Pressure)
	{
		pIChemGayLussac_Pressure->Release();
		pIChemGayLussac_Pressure = NULL;
	}
}
