/*
Project part 4] Integrate splash screen, dialog box and consume Physics DLL.
Used explicit linking: LoadLibrary(), FreeLibrary(), function pointer.

NOTES: 1] Splash screen will be visible until first activation ie spacebar key input.
2] Spalsh screen can not be maximized, resized. If needed to have, can be achieved. Did for learning diff window options.
3] UI Design is simple. Focus was on integration. UI will be improved in later parts.
4] Has wide char support. Results are shown with UNICODE characters.
5] Main window is always shown at center of the screen.
6] Code is not structured, not seggregated and modularized into seperate functions.
	Just did at basic level for learning purpose.
*/

// headers
#include <windows.h>
#include "SplashBitmap.h"
#include <stdio.h>
#include "PhysicsDialogBox.h"
#include "PhysicsDefDLLServer.h"

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
BOOL CALLBACK PhysicsDlgProc(HWND, UINT, WPARAM, LPARAM);
void ShowPhysicsDialog(HINSTANCE hInst, HWND hwnd, char* theMessage, RECT rc);

// Globally variables
HINSTANCE theInstance = NULL; // NOTE: Required when Loading Bitmap in WM_CREATE
BandColors b1 = Red, b2 = Green, b3 = Yellow, b4 = Blue;

// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("PhysicsIntegration");

	// code
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc; // NOTE
	wndclass.hInstance = hInstance;
	theInstance = hInstance; // Save instance
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// create window at center of the screen
	int xPos = (GetSystemMetrics(SM_CXSCREEN) - 817) / 2;
	int yPos = (GetSystemMetrics(SM_CYSCREEN) - 670) / 2; // CW_USEDEFAULT

	hwnd = CreateWindow(szAppName,
		TEXT("Physics Integration"),
		WS_OVERLAPPEDWINDOW ^ WS_THICKFRAME ^ WS_MAXIMIZEBOX,
		xPos,
		yPos,
		817,
		670,
		NULL,
		NULL,
		hInstance,
		NULL);
	
	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	// message loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	PAINTSTRUCT ps;
	static RECT rc;
	static HBITMAP hBitmap = NULL; // NOTE: Needs to be maintained across calls
	HDC hdcBitmap = NULL;
	HGDIOBJ rt_gdi1;
	int rt_getObj;
	BOOL rt_copyDc;
	BOOL rt_Del;
	BITMAP bmp;
	static bool IsInitiated = false;
	static char theMessage[350] = "";
	static HINSTANCE hInst = NULL;

	// code
	switch (iMsg)
	{
	case WM_CREATE:
		hBitmap = LoadBitmap(theInstance, MAKEINTRESOURCE(MY_SPLASH_BITMAP));
		if (hBitmap == NULL)
		{
			MessageBox(hwnd, TEXT("Error loading bitmap!!"), "NULL", MB_OK | MB_ICONERROR);
			DestroyWindow(hwnd);
		}
		wsprintf(theMessage, "Please press space bar to proceed.");
		break;

	case WM_PAINT:
		HFONT hFont;
		if (!IsInitiated)
		{
			hdc = BeginPaint(hwnd, &ps);
			hdcBitmap = CreateCompatibleDC(hdc);

			rt_gdi1 = SelectObject(hdcBitmap, hBitmap); //hgdiobj
			rt_getObj = GetObject(hBitmap, sizeof(BITMAP), (LPVOID*)&bmp);
			rt_copyDc = BitBlt(hdc, 0, 0, bmp.bmWidth, bmp.bmHeight, hdcBitmap, 0, 0, SRCCOPY);

			rt_gdi1 = SelectObject(hdc, hBitmap);
			rt_Del = DeleteDC(hdcBitmap); // NOTE: This will retain the image as "hdc" is maintained by deleting hdcBitmap

			GetClientRect(hwnd, &rc);
			SetTextColor(hdc, RGB(0, 255, 0));
			SetBkColor(hdc, RGB(0, 0, 0));
			hFont = CreateFont(28, 14, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_OUTLINE_PRECIS,
				CLIP_DEFAULT_PRECIS, CLEARTYPE_QUALITY, VARIABLE_PITCH, TEXT("Times New Roman"));
			SelectObject(hdc, hFont);
			DrawText(hdc, theMessage, -1, &rc, DT_BOTTOM | DT_SINGLELINE | DT_CENTER);
			DeleteObject(hFont);

			rt_Del = DeleteDC(hdc); // NOTE: This will delete hdc and erase the image
			EndPaint(hwnd, &ps);
		}
		else
		{
			long style = GetWindowLong(hwnd, GWL_STYLE);
			style = style | WS_MAXIMIZEBOX | WS_THICKFRAME;
			SetWindowLong(hwnd, GWL_STYLE, style);

			hdc = BeginPaint(hwnd, &ps);
			GetClientRect(hwnd, &rc);
			SetTextColor(hdc, RGB(255, 0, 0));
			SetBkColor(hdc, RGB(0, 0, 0));
			DrawText(hdc, theMessage, -1, &rc, DT_VCENTER | DT_CENTER);
			EndPaint(hwnd, &ps);

			ShowPhysicsDialog(hInst, hwnd, theMessage, rc);
			InvalidateRect(hwnd, &rc, TRUE);
		}
		break;

	case WM_DESTROY:
		rt_Del = DeleteObject(hBitmap);
		PostQuitMessage(0);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_SPACE:
			if (!IsInitiated)
			{
				IsInitiated = true;
				wsprintf(theMessage, "Application started.");
				InvalidateRect(hwnd, &rc, TRUE);
			}
			break;
		}
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ShowPhysicsDialog(HINSTANCE hInst, HWND hwnd, char* theMessage, RECT rc)
{
	hInst = (HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE);
	// call to DialogBox
	DialogBox(hInst, "DATAENTRY", hwnd, PhysicsDlgProc);
}

BandColors GetIndexFromString(char* searchColor)
{
	for (int i = 0; i < 12; i++)
	{
		if (strcmp(searchColor, BandColorName[i]) == 0)
		{
			return (BandColors)i;
		}
	}
	return (BandColors)-1;
}

BOOL CALLBACK PhysicsDlgProc(HWND hDlg, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	char cmbText[15] = "";
	static RECT rc;
	wchar_t temp[50];
	HWND cbo;
	ResistanceToleranceResult theResult;
	static HWND hwndTemp;

	static HMODULE hLib = NULL;
	typedef ResistanceToleranceResult(*pfnCalcFourBandResistanceTolerance)(BandColors, BandColors, BandColors, BandColors);
	static pfnCalcFourBandResistanceTolerance calcFourBandResistanceTolerance = NULL;

	switch (iMsg)
	{
	case WM_INITDIALOG:
		cbo = GetDlgItem(hDlg, ID_CBBAND1);
		for (int i = 0; i < 12; i++)
		{
			if (i == Black || i == Gold || i == Silver)
				continue;
			SendMessage(cbo, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCTSTR)BandColorName[i]);
			SendMessage(cbo, CB_SETCURSEL, (WPARAM)0, (LPARAM)0);
		}

		cbo = GetDlgItem(hDlg, ID_CBBAND2);
		for (int i = 0; i < 12; i++)
		{
			if (i == Gold || i == Silver)
				continue;
			SendMessage(cbo, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCTSTR)BandColorName[i]);
			SendMessage(cbo, CB_SETCURSEL, (WPARAM)0, (LPARAM)0);
		}

		cbo = GetDlgItem(hDlg, ID_CBBAND3);
		for (int i = 0; i < 12; i++)
		{
			SendMessage(cbo, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCTSTR)BandColorName[i]);
			SendMessage(cbo, CB_SETCURSEL, (WPARAM)0, (LPARAM)0);
		}

		cbo = GetDlgItem(hDlg, ID_CBBAND4);
		for (int i = 0; i < 12; i++)
		{
			if (i == Black || i == White)
				continue;
			SendMessage(cbo, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCTSTR)BandColorName[i]);
			SendMessage(cbo, CB_SETCURSEL, (WPARAM)0, (LPARAM)0);
		}

		return (TRUE);

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case ID_CALCULATE_PHYSICS:
			int idx_row;
			
			hwndTemp = GetDlgItem(hDlg, ID_CBBAND1);
			idx_row = SendMessage(hwndTemp, CB_GETCURSEL, 0, 0);
			SendMessage(hwndTemp, CB_GETLBTEXT, idx_row, (LPARAM)cmbText);
			b1 = GetIndexFromString(cmbText);

			hwndTemp = GetDlgItem(hDlg, ID_CBBAND2);
			idx_row = SendMessage(hwndTemp, CB_GETCURSEL, 0, 0);
			SendMessage(hwndTemp, CB_GETLBTEXT, idx_row, (LPARAM)cmbText);
			b2 = GetIndexFromString(cmbText);

			hwndTemp = GetDlgItem(hDlg, ID_CBBAND3);
			idx_row = SendMessage(hwndTemp, CB_GETCURSEL, 0, 0);
			SendMessage(hwndTemp, CB_GETLBTEXT, idx_row, (LPARAM)cmbText);
			b3 = GetIndexFromString(cmbText);

			hwndTemp = GetDlgItem(hDlg, ID_CBBAND4);
			idx_row = SendMessage(hwndTemp, CB_GETCURSEL, 0, 0);
			SendMessage(hwndTemp, CB_GETLBTEXT, idx_row, (LPARAM)cmbText);
			b4 = GetIndexFromString(cmbText);

			hLib = LoadLibrary(TEXT("PhysicsDefDLLServer.dll")); // Step 1 Load DLL
			if (hLib == NULL)
			{
				MessageBox(hDlg, TEXT("PhysicsDefDLLServer.dll not found!!"), "Error", MB_OK | MB_ICONERROR);
				DestroyWindow(hDlg);
			}
			calcFourBandResistanceTolerance = (pfnCalcFourBandResistanceTolerance)GetProcAddress(hLib, "CalcFourBandResistanceTolerance"); // Step 2

			theResult = calcFourBandResistanceTolerance(b1, b2, b3, b4); // Step 3 Explicitly linked custom DLL function call
			FreeLibrary(hLib); // Step 4

			if (theResult.IsToleranceSuccessful)
			{
				SetDlgItemTextW(hDlg, ID_TOLERANCE, theResult.Tolerance);
			}
			if (theResult.IsResistanceSuccessful)
			{
				hwndTemp = GetDlgItem(hDlg, ID_RESISTANCE);
				swprintf(temp, 40, L"%.2lf", theResult.Resistance); // NOTE: Important This is how float/double values can be displayed. 2 digits after decimal point
				SetWindowTextW(hwndTemp, temp);
			}
			return TRUE;
			break;

		case IDCANCEL:
			EndDialog(hDlg, 0);
			break;
		}

	case WM_CTLCOLORDLG:
		// TODO
		break;

		return (TRUE);
	}

	return (FALSE);
}
