/*
Project part 3] Create Physics DLL and test client for it.
	Used implicit linking for easy implementation and 
	majorly for intellisense, to know if code is correct
	and called functions param passing, type checks, etc.
*/

// headers
#include <windows.h>
#include "PhysicsDefDLLServer.h"
#include <wchar.h>

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

BandColors band1 = Red;// Brown;
BandColors band2 = Green;// Brown;
BandColors band3 = Yellow;// Brown;
BandColors band4 = Blue;// Brown;

// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("PhysicsClient");

	// code
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc; // NOTE
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindow(szAppName,
		TEXT("Physics Client"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		500,
		500,
		NULL,
		NULL,
		hInstance,
		NULL);

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	// message loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	static wchar_t theMessage[500] = L"";
	static wchar_t tempStr[250] = L"";
	static ResistanceToleranceResult theResult;
	size_t tempSize = 0;
	errno_t tempErrno;

	HDC hdc;
	PAINTSTRUCT ps;
	RECT rc;

	// code
	switch (iMsg)
	{
	case WM_PAINT:
		theResult = CalcFourBandResistanceTolerance(band1, band2, band3, band4); // Implicitly linked custom DLL function call

		memset(theMessage, '\0', sizeof(theMessage));
		memset(tempStr, '\0', sizeof(tempStr));
		wcscpy_s(theMessage, L"***************************\n  Test Client for Physics  \n***************************\n");

		wcscat_s(theMessage, TEXT(L"\nBand 1 = "));
		tempErrno = mbstowcs_s(&tempSize, tempStr, BandColorName[band1], 15);
		wcscat_s(theMessage, tempStr);

		wcscat_s(theMessage, TEXT(L"\nBand 2 = "));
		tempErrno = mbstowcs_s(&tempSize, tempStr, BandColorName[band2], 15);
		wcscat_s(theMessage, tempStr);

		wcscat_s(theMessage, TEXT(L"\nBand 3 = "));
		tempErrno = mbstowcs_s(&tempSize, tempStr, BandColorName[band3], 15);
		wcscat_s(theMessage, tempStr);

		wcscat_s(theMessage, TEXT(L"\nBand 4 = "));
		tempErrno = mbstowcs_s(&tempSize, tempStr, BandColorName[band4], 15);
		wcscat_s(theMessage, tempStr);
		wcscat_s(theMessage, L"\n");

		if (theResult.IsToleranceSuccessful)
		{
			wcscat_s(theMessage, TEXT(L"\nTolerance = "));
			wcscat_s(theMessage, theResult.Tolerance);
		}
		if (theResult.IsResistanceSuccessful)
		{
			wcscat_s(theMessage, TEXT(L"\nResistance = "));
			swprintf(tempStr, 40, L"%.2lf", theResult.Resistance); // NOTE: Important This is how float/double values can be displayed. 2 digits after decimal point
			wcscat_s(theMessage, tempStr);
		}
		// Report errors, if any
		if (!theResult.IsResistanceSuccessful || !theResult.IsToleranceSuccessful)
		{
			mbstowcs_s(&tempSize, tempStr, theResult.Messages, 248);
			wcscat_s(theMessage, tempStr);
		}

		hdc = BeginPaint(hwnd, &ps);
		GetClientRect(hwnd, &rc);
		SetTextColor(hdc, RGB(255, 0, 0));
		SetBkColor(hdc, RGB(0, 0, 0));
		DrawTextW(hdc, theMessage, -1, &rc, DT_LEFT );
		EndPaint(hwnd, &ps);

		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}
