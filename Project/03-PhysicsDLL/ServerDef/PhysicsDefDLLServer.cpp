#include <Windows.h>
#include "PhysicsDefDLLServer.h"
#include <wchar.h>

// Global variables
// 1 extra char for null terminator '\0'
char ResistanceList[12][5] =
{
	"",
	"1",
	"2",
	"3",
	"4",
	"0.5",
	"0.25",
	"0.10",
	"0.05",
	"",
	"5",
	"10"
};

int MultiplierDivisorList[12] =
{
	1,
	10,
	100,
	1000,
	10000,
	100000,
	1000000,
	10000000,
	100000000,
	1000000000, // WHITE
	10,
	100
};

BOOL WINAPI DllMain(HMODULE hDll, DWORD dwReason, LPVOID lpReserved)
{
	// Code
	switch (dwReason)
	{
	case DLL_PROCESS_ATTACH:
	case DLL_PROCESS_DETACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
		break;
	}

	return TRUE; // Important
}

ResistanceToleranceResult* GetTolerance(BandColors band4, ResistanceToleranceResult* theResult)
{
	char toleranceTemp[10] = "";
	const wchar_t prefix[5] = { 0XC2, 0XB1 }; // L"Â±" or "\xC2\xB1"
	WCHAR buff2[10] = L"", buff3[10] = L"";

	strcpy(theResult->Messages, "");
	memset(buff2, '\0', sizeof(buff2));
	memset(buff3, '\0', sizeof(buff3));

	switch (band4)
	{
	case Brown:
	case Red:
	case Orange:
	case Yellow:
	case Green:
	case Blue:
	case Violet:
	case Grey:
	case Gold:
	case Silver:
		strcpy(toleranceTemp, ResistanceList[band4]);

		// Prepare output formatted string
		wcscpy(buff2, prefix); // Part A]
		mbstowcs(buff3, toleranceTemp, 10); // Part B] // NOTE: Required to convert ANSI string to wide char string. Without this string was getting garbage values.
		wcscat(buff2, buff3);
		wcscat(buff2, (wchar_t*)"%"); // Part C] // Just for learning explored different way to append wide string from const ANSI string
		wcscpy(theResult->Tolerance, buff2);

		theResult->IsToleranceSuccessful = true; // Don't forget to mark true
		break;

	default:
		theResult->IsToleranceSuccessful = false; // NOTE : This is helpful if same struct passed repeatedly. Else value will be same for all.
		strcpy(theResult->Messages, "Invalid band 4! Please input correct band 4. ");
		break;
	}

	return theResult;
}

ResistanceToleranceResult* GetResistance(BandColors band1, BandColors band2, BandColors band3, ResistanceToleranceResult* theResult)
{
	double theResistance = 0.0;

	// Validations
	theResult->IsResistanceSuccessful = true;

	// Consider valid cases as client may edit header and can insert own enums
	// Don't return from cases immediately. Complete all bands and validate all in one go.
	switch (band1)
	{
	case Brown:
	case Red:
	case Orange:
	case Yellow:
	case Green:
	case Blue:
	case Violet:
	case Grey:
	case White:
		break;

	default:
		theResult->IsResistanceSuccessful = false;
		strcat(theResult->Messages, "Invalid band 1! Please input correct band 1. ");
		break;
	}

	switch (band2)
	{
	case Black:
	case Brown:
	case Red:
	case Orange:
	case Yellow:
	case Green:
	case Blue:
	case Violet:
	case Grey:
	case White:
		break;

	default:
		theResult->IsResistanceSuccessful = false;
		strcat(theResult->Messages, "Invalid band 2! Please input correct band 2. ");
		break;
	}

	switch (band3)
	{
	case Black:
	case Brown:
	case Red:
	case Orange:
	case Yellow:
	case Green:
	case Blue:
	case Violet:
	case Grey:
	case White:
	case Gold:
	case Silver:
		break;

	default:
		theResult->IsResistanceSuccessful = false;
		strcat(theResult->Messages, "Invalid band 3! Please input correct band 3. ");
		break;
	}

	// inputs are valid. So calculate resistance. LOGIC
	if (theResult->IsResistanceSuccessful)
	{
		theResistance = (band1 * 10) + band2;
		if (band3 - 9 > 0) // LOGIC
		{
			theResistance = theResistance / MultiplierDivisorList[band3];
		}
		else
		{
			theResistance = theResistance * MultiplierDivisorList[band3];
		}
		theResult->Resistance = theResistance;
	}

	return theResult;
}

extern "C" ResistanceToleranceResult CalcFourBandResistanceTolerance(BandColors band1,
	BandColors band2,
	BandColors band3,
	BandColors band4)
{
	ResistanceToleranceResult theResult;

	theResult.IsResistanceSuccessful = false;
	theResult.IsToleranceSuccessful = false;
	memset(theResult.Messages, '\0', sizeof(theResult.Messages));
	memset(theResult.Tolerance, '\0', sizeof(theResult.Tolerance));
	theResult.Resistance = 0.0;

	GetTolerance(band4, &theResult);
	GetResistance(band1, band2, band3, &theResult);

	return theResult;
}
