Project part 3] Create Physics DLL and test client for it.
	Used implicit linking for easy implementation and 
	majorly for intellisense, to know if code is correct
	and called functions param passing, type checks, etc.

Steps:-
-Create server DLL with module definition .def
-Copy physics server DLL in SysWOW64.
-Put server .h and .lib in client proj directory.

-DLL can be debugged only if built in Debug mode.
-Did DLL debugging successfully from client. Keep 
.pdb file in client which has debug symbols as needed.

-String formatting done in DLL Server itself for UNICODE Tolerance.
-Also done string processig at client side for printing formatted output.
-Struggled a lot for string operations (ANSI, UNICODE), printing
 output, double/float to Win32 console window. Not sure if these
 are the best suitable methods used. Ok for learning purpose.

Inputs
band 1: 9 colors, no black, gold, silver
band 2: 10 colors, has black, no gold, silver
band 3: 12 colors, has black, gold, silver
band 4: 10 colors, no black, white

Logic
1st digit = Band 1
2nd digit = Band 2
Resistance = Dig1Dig2 * or / Band 3
Tolerance = Band 4
