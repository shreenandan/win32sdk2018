#import "MyMathsMatrixInterop.tlb" no_namespace, raw_interfaces_only
#include "Debug\mymathsmatrixinterop.tlh"

#include <Windows.h>
#include "MathsCOMInteropWrapper.h"
#include <stdio.h>
#define MAX_ROWS 4
#define MAX_COLS 4

// global variable declarations
CLSID CLSID_MyMatrix;
IMatrix *pIMatrix;
SAFEARRAYBOUND sab1[2];
SAFEARRAYBOUND sab2[2];
SAFEARRAY* sa1;
SAFEARRAY* sa2;
SAFEARRAY* theResult;

void ComErrorDescriptionString(HWND hwnd, HRESULT hr);

BOOL WINAPI DllMain(HMODULE hDll, DWORD dwReason, LPVOID lpReserved)
{
	// Code
	switch (dwReason)
	{
	case DLL_PROCESS_ATTACH:
	case DLL_PROCESS_DETACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
		break;
	}

	return TRUE; // Important
}

// NOTE: There are 2 possible ways to use Class Factory Server in Wrapper
// 1] Way 1 Call CoCreateInstance() every time in each chemistry function
// by passing riid any Required Interface. Can be Released then and there in the same function itself.
// 2] Way 2 Call CoCreateInstance() only once in Initialize function
// by passing riid fixed Primary Interface. Here maintain primary interface object till last.
// QI other interfaces on this primary. Non primary can be released immediately after use.

// Using Way 2

// Decision: Temperature will be primary interface. First as per Adjustor Thunk rule.
// It will be maintained till last. Other interfaces obtained by QI on Temperature.
// Can be released during intermediate processing.
extern "C" bool InitializeMathsWrapper(HWND hwnd)
{
	HRESULT hr;

	hr = CLSIDFromProgID(L"MyMathsMatrixInterop.MyMatrix", &CLSID_MyMatrix);
	if (FAILED(hr))
	{
		ComErrorDescriptionString(hwnd, hr);
		return false;
	}

	hr = CoCreateInstance(CLSID_MyMatrix, NULL, CLSCTX_INPROC_SERVER, __uuidof(IMatrix), (void **)&pIMatrix);
	if (FAILED(hr))
	{
		ComErrorDescriptionString(hwnd, hr);
		return false;
	}
	return true;
}

void PrepareArrays(int arr1[4][4], int arr2[4][4])
{
	sab1[0].cElements = MAX_ROWS;
	sab1[0].lLbound = 0;
	sab1[1].cElements = MAX_ROWS;
	sab1[1].lLbound = 0;

	sa1 = SafeArrayCreate(VT_I4, 2, sab1);
	for (int i = 0; i < MAX_ROWS; i++)
	{
		for (int j = 0; j < MAX_COLS; j++)
		{
			LONG index[2] = { i,j };
			//int value = 1 + i * 2 + j;
			int value = arr1[i][j];
			SafeArrayPutElement(sa1, index, &value);
		}
	}

	sab2[0].cElements = MAX_ROWS;
	sab2[0].lLbound = 0;
	sab2[1].cElements = MAX_ROWS;
	sab2[1].lLbound = 0;

	sa2 = SafeArrayCreate(VT_I4, 2, sab2);
	for (int i = 0; i < MAX_ROWS; i++)
	{
		for (int j = 0; j < MAX_COLS; j++)
		{
			LONG index[2] = { i,j };
			//int value = 1 + i * 2 + j;
			int value = arr2[i][j];
			SafeArrayPutElement(sa2, index, &value);
		}
	}

}

void ExtractResult(int resultArray[4][4])
{
	VARTYPE vt;
	SafeArrayGetVartype(theResult, &vt);

	if (vt == VT_I4)
	{
		LONG begin[2]{ 0 };
		LONG end[2]{ 0 };

		SafeArrayGetLBound(theResult, 1, &begin[0]);
		SafeArrayGetLBound(theResult, 2, &begin[1]);
		SafeArrayGetUBound(theResult, 1, &end[0]);
		SafeArrayGetUBound(theResult, 2, &end[1]);

		for (LONG i = begin[0]; i <= end[0]; ++i)
		{
			for (LONG j = begin[1]; j <= end[1]; ++j)
			{
				LONG index[2]{ i,j };
				int value;
				SafeArrayGetElement(theResult, index, &value);
				resultArray[i][j] = value;
			}
		}
	}

}

extern "C" bool GetAddition(HWND hwnd, int arr1[4][4], int arr2[4][4], int result[4][4])
{
	HRESULT hr;
	
	// One level of safety. Avoid null reference exception.
	// Will come into picture if wrapper consumer didn't call Initialize.
	// Second case - Anytime earlier if Initialize failed.
	if (!pIMatrix)
	{
		if (!InitializeMathsWrapper(hwnd))
			return false;
	}

	bool isSuccessful = false;
	
	PrepareArrays(arr1, arr2);
	hr = pIMatrix->Add(sa1, sa2, &theResult);

	if (FAILED(hr))
	{
		ComErrorDescriptionString(hwnd, hr);
	}
	else
	{
		isSuccessful = true;
		ExtractResult(result);
	}

	return isSuccessful;
}

extern "C" void UninitializeMathsWrapper()
{
	// SafeInterfaceRelease
	// Ideally should be null considering normal flow. If not, on safer side release explicitly.
	if (pIMatrix)
	{
		pIMatrix->Release();
		pIMatrix = NULL;
	}
}

extern "C" bool GetSubtraction(HWND hwnd, int arr1[4][4], int arr2[4][4], int result[4][4])
{
	HRESULT hr;

	// One level of safety. Avoid null reference exception.
	// Will come into picture if wrapper consumer didn't call Initialize.
	// Second case - Anytime earlier if Initialize failed.
	if (!pIMatrix)
	{
		if (!InitializeMathsWrapper(hwnd))
			return false;
	}

	bool isSuccessful = false;

	PrepareArrays(arr1, arr2);
	hr = pIMatrix->Subtract(sa1, sa2, &theResult);

	if (FAILED(hr))
	{
		ComErrorDescriptionString(hwnd, hr);
	}
	else
	{
		isSuccessful = true;
		ExtractResult(result);
	}

	return isSuccessful;
}

extern "C" bool GetMultiplication(HWND hwnd, int arr1[4][4], int arr2[4][4], int result[4][4])
{
	HRESULT hr;

	// One level of safety. Avoid null reference exception on QI call.
	// Will come into picture if wrapper consumer didn't call Initialize.
	// Second case - Anytime earlier if Initialize failed.
	if (!pIMatrix)
	{
		if (!InitializeMathsWrapper(hwnd))
			return false;
	}

	bool isSuccessful = false;

	PrepareArrays(arr1, arr2);
	hr = pIMatrix->Multiply(sa1, sa2, &theResult);

	if (FAILED(hr))
	{
		ComErrorDescriptionString(hwnd, hr);
	}
	else
	{
		isSuccessful = true;
		ExtractResult(result);
	}

	return isSuccessful;
}

void ComErrorDescriptionString(HWND hwnd, HRESULT hr)
{
	TCHAR *szErrorMessage = NULL;
	TCHAR str[255];

	if (FACILITY_WINDOWS == HRESULT_FACILITY(hr))
	{
		hr = HRESULT_CODE(hr);
	}
	// typecast
	if (FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, NULL,
		hr, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&szErrorMessage, 0, NULL) != 0)
	{
		sprintf_s(str, TEXT("%#x : %s"), hr, szErrorMessage);
		LocalFree(szErrorMessage);
	}
	else
		sprintf_s(str, TEXT("[Could not find a description for error # %#x.]\n"), hr);

	MessageBox(hwnd, str, TEXT("Com Error"), MB_OK);
}
