﻿using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace MyMathsMatrixInterop
{
    public interface IMatrix
    {
        int[,] MultiplyResult { get; set; }
        int[,] AddResult { get; set; }
        int[,] SubtractResult { get; set; }
        int[,] Multiply(int[,] arr1, int[,] arr2);
        int[,] Add(int[,] arr1, int[,] arr2);
        int[,] Subtract(int[,] arr1, int[,] arr2);
    }

    [ClassInterface(ClassInterfaceType.None)]
    public class MyMatrix : IMatrix
    {
        public MyMatrix()
        {
            // Defaults
            MultiplyResult = new int[4,4];
            AddResult = new int[4,4];
            SubtractResult = new int[4,4];
        }

        public int[,] MultiplyResult { get; set; }
        public int[,] AddResult { get; set; }
        public int[,] SubtractResult { get; set; }

        public int[,] Multiply(int[,] arr1, int[,] arr2)
        {
            MultiplyResult = new int[arr1.GetLength(0), arr2.GetLength(1)];
            for (int i = 0; i < arr1.GetLength(0); i++)
            {
                for (int j = 0; j < arr2.GetLength(1); j++)
                {
                    for (int k = 0; k < arr1.GetLength(0); k++)
                    {
                        MultiplyResult[i, j] += arr1[i, k] * arr2[k, j];
                    }
                }
            }
            return MultiplyResult;
        }

        public int[,] Add(int[,] arr1, int[,] arr2)
        {
            AddResult = new int[arr1.GetLength(0), arr2.GetLength(1)];
            for (int i = 0; i < arr1.GetLength(0); i++)
            {
                for (int j = 0; j < arr2.GetLength(1); j++)
                {
                    AddResult[i, j] = arr1[i, j] + arr2[i, j];
                }
            }
            return AddResult;
        }

        public int[,] Subtract(int[,] arr1, int[,] arr2)
        {
            SubtractResult = new int[arr1.GetLength(0), arr2.GetLength(1)];
            for (int i = 0; i < arr1.GetLength(0); i++)
            {
                for (int j = 0; j < arr2.GetLength(1); j++)
                {
                    SubtractResult[i, j] = arr1[i, j] - arr2[i, j];
                }
            }
            return SubtractResult;
        }

    }
}
