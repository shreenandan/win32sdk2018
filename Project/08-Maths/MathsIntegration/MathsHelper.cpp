#pragma once
#include "MathsHelper.h"
#include "MathsDialogBox.h"
#include <stdio.h>

MathsHelper::MathsHelper()
{

}

void MathsHelper::EnableMaths(HWND hDlg)
{
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R1C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R1C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R1C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R1C4), TRUE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R2C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R2C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R2C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R2C4), TRUE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R3C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R3C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R3C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R3C4), TRUE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R4C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R4C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R4C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R4C4), TRUE);


	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R1C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R1C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R1C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R1C4), TRUE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R2C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R2C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R2C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R2C4), TRUE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R3C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R3C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R3C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R3C4), TRUE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R4C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R4C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R4C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R4C4), TRUE);


	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R1C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R1C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R1C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R1C4), TRUE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R2C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R2C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R2C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R2C4), TRUE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R3C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R3C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R3C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R3C4), TRUE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R4C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R4C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R4C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R4C4), TRUE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_CB_OPERATION), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_CALCULATE_MATHS), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_SUBMIT), TRUE);
}

void MathsHelper::DisableMaths(HWND hDlg)
{
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R1C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R1C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R1C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R1C4), FALSE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R2C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R2C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R2C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R2C4), FALSE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R3C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R3C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R3C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R3C4), FALSE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R4C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R4C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R4C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R4C4), FALSE);


	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R1C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R1C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R1C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R1C4), FALSE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R2C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R2C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R2C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R2C4), FALSE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R3C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R3C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R3C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R3C4), FALSE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R4C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R4C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R4C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R4C4), FALSE);


	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R1C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R1C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R1C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R1C4), FALSE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R2C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R2C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R2C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R2C4), FALSE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R3C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R3C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R3C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R3C4), FALSE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R4C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R4C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R4C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R4C4), FALSE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_CB_OPERATION), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_CALCULATE_MATHS), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_SUBMIT), FALSE);
}

void MathsHelper::LoadMathsCombo(HWND hDlg)
{
	HWND cbo;
	cbo = GetDlgItem(hDlg, ID_MATHS_CB_OPERATION);
	SendMessage(cbo, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCTSTR)"+");
	SendMessage(cbo, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCTSTR)"-");
	SendMessage(cbo, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCTSTR)"*");
	SendMessage(cbo, CB_SETCURSEL, (WPARAM)0, (LPARAM)0);
}

void MathsHelper::ReadArray(HWND hDlg, ArrayId id)
{
	char theText[20];
	BOOL translated;
	UINT res;

	switch (id)
	{
	case Arr1:
		this->arr1[0][0] = (int)GetDlgItemInt(hDlg, ID_MATHS_M1_R1C1, &translated, TRUE);
		this->arr1[0][1] = (int)GetDlgItemInt(hDlg, ID_MATHS_M1_R1C2, &translated, TRUE);
		this->arr1[0][2] = (int)GetDlgItemInt(hDlg, ID_MATHS_M1_R1C3, &translated, TRUE);
		this->arr1[0][3] = (int)GetDlgItemInt(hDlg, ID_MATHS_M1_R1C4, &translated, TRUE);
		this->arr1[1][0] = (int)GetDlgItemInt(hDlg, ID_MATHS_M1_R2C1, &translated, TRUE);
		this->arr1[1][1] = (int)GetDlgItemInt(hDlg, ID_MATHS_M1_R2C2, &translated, TRUE);
		this->arr1[1][2] = (int)GetDlgItemInt(hDlg, ID_MATHS_M1_R2C3, &translated, TRUE);
		this->arr1[1][3] = (int)GetDlgItemInt(hDlg, ID_MATHS_M1_R2C4, &translated, TRUE);
		this->arr1[2][0] = (int)GetDlgItemInt(hDlg, ID_MATHS_M1_R3C1, &translated, TRUE);
		this->arr1[2][1] = (int)GetDlgItemInt(hDlg, ID_MATHS_M1_R3C2, &translated, TRUE);
		this->arr1[2][2] = (int)GetDlgItemInt(hDlg, ID_MATHS_M1_R3C3, &translated, TRUE);
		this->arr1[2][3] = (int)GetDlgItemInt(hDlg, ID_MATHS_M1_R3C4, &translated, TRUE);
		this->arr1[3][0] = (int)GetDlgItemInt(hDlg, ID_MATHS_M1_R4C1, &translated, TRUE);
		this->arr1[3][1] = (int)GetDlgItemInt(hDlg, ID_MATHS_M1_R4C2, &translated, TRUE);
		this->arr1[3][2] = (int)GetDlgItemInt(hDlg, ID_MATHS_M1_R4C3, &translated, TRUE);
		this->arr1[3][3] = (int)GetDlgItemInt(hDlg, ID_MATHS_M1_R4C4, &translated, TRUE);
		break;

	case Arr2:
		this->arr2[0][0] = (int)GetDlgItemInt(hDlg, ID_MATHS_M2_R1C1, &translated, TRUE);
		this->arr2[0][1] = (int)GetDlgItemInt(hDlg, ID_MATHS_M2_R1C2, &translated, TRUE);
		this->arr2[0][2] = (int)GetDlgItemInt(hDlg, ID_MATHS_M2_R1C3, &translated, TRUE);
		this->arr2[0][3] = (int)GetDlgItemInt(hDlg, ID_MATHS_M2_R1C4, &translated, TRUE);
		this->arr2[1][0] = (int)GetDlgItemInt(hDlg, ID_MATHS_M2_R2C1, &translated, TRUE);
		this->arr2[1][1] = (int)GetDlgItemInt(hDlg, ID_MATHS_M2_R2C2, &translated, TRUE);
		this->arr2[1][2] = (int)GetDlgItemInt(hDlg, ID_MATHS_M2_R2C3, &translated, TRUE);
		this->arr2[1][3] = (int)GetDlgItemInt(hDlg, ID_MATHS_M2_R2C4, &translated, TRUE);
		this->arr2[2][0] = (int)GetDlgItemInt(hDlg, ID_MATHS_M2_R3C1, &translated, TRUE);
		this->arr2[2][1] = (int)GetDlgItemInt(hDlg, ID_MATHS_M2_R3C2, &translated, TRUE);
		this->arr2[2][2] = (int)GetDlgItemInt(hDlg, ID_MATHS_M2_R3C3, &translated, TRUE);
		this->arr2[2][3] = (int)GetDlgItemInt(hDlg, ID_MATHS_M2_R3C4, &translated, TRUE);
		this->arr2[3][0] = (int)GetDlgItemInt(hDlg, ID_MATHS_M2_R4C1, &translated, TRUE);
		this->arr2[3][1] = (int)GetDlgItemInt(hDlg, ID_MATHS_M2_R4C2, &translated, TRUE);
		this->arr2[3][2] = (int)GetDlgItemInt(hDlg, ID_MATHS_M2_R4C3, &translated, TRUE);
		this->arr2[3][3] = (int)GetDlgItemInt(hDlg, ID_MATHS_M2_R4C4, &translated, TRUE);
		break;

	case ArrRes:
		// No reading only writing
		break;

	default:
		break;
	}

}



void MathsHelper::WriteResult(HWND hDlg)
{
	char temp[8];

	sprintf_s(temp, 7, "%d", this->arrRes[0][0]); SetDlgItemText(hDlg, ID_MATHS_M3_R1C1, temp);
	sprintf_s(temp, 7, "%d", this->arrRes[0][1]); SetDlgItemText(hDlg, ID_MATHS_M3_R1C2, temp);
	sprintf_s(temp, 7, "%d", this->arrRes[0][2]); SetDlgItemText(hDlg, ID_MATHS_M3_R1C3, temp);
	sprintf_s(temp, 7, "%d", this->arrRes[0][3]); SetDlgItemText(hDlg, ID_MATHS_M3_R1C4, temp);
	sprintf_s(temp, 7, "%d", this->arrRes[1][0]); SetDlgItemText(hDlg, ID_MATHS_M3_R2C1, temp);
	sprintf_s(temp, 7, "%d", this->arrRes[1][1]); SetDlgItemText(hDlg, ID_MATHS_M3_R2C2, temp);
	sprintf_s(temp, 7, "%d", this->arrRes[1][2]); SetDlgItemText(hDlg, ID_MATHS_M3_R2C3, temp);
	sprintf_s(temp, 7, "%d", this->arrRes[1][3]); SetDlgItemText(hDlg, ID_MATHS_M3_R2C4, temp);
	sprintf_s(temp, 7, "%d", this->arrRes[2][0]); SetDlgItemText(hDlg, ID_MATHS_M3_R3C1, temp);
	sprintf_s(temp, 7, "%d", this->arrRes[2][1]); SetDlgItemText(hDlg, ID_MATHS_M3_R3C2, temp);
	sprintf_s(temp, 7, "%d", this->arrRes[2][2]); SetDlgItemText(hDlg, ID_MATHS_M3_R3C3, temp);
	sprintf_s(temp, 7, "%d", this->arrRes[2][3]); SetDlgItemText(hDlg, ID_MATHS_M3_R3C4, temp);
	sprintf_s(temp, 7, "%d", this->arrRes[3][0]); SetDlgItemText(hDlg, ID_MATHS_M3_R4C1, temp);
	sprintf_s(temp, 7, "%d", this->arrRes[3][1]); SetDlgItemText(hDlg, ID_MATHS_M3_R4C2, temp);
	sprintf_s(temp, 7, "%d", this->arrRes[3][2]); SetDlgItemText(hDlg, ID_MATHS_M3_R4C3, temp);
	sprintf_s(temp, 7, "%d", this->arrRes[3][3]); SetDlgItemText(hDlg, ID_MATHS_M3_R4C4, temp);
}