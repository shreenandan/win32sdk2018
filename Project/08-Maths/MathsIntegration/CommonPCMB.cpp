#pragma once 
#include "CommonPCMB.h"

void ShowPCMBDialog(HINSTANCE hInst, HWND hwnd, char* theMessage, RECT rc)
{
	hInst = (HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE);
	// call to DialogBox
	DialogBox(hInst, "PCMBDlg", hwnd, PCMBDlgProc);
}

int GetComboSelectedIndex(HWND hDlg, int dlgItemId)
{
	HWND hwndTemp;
	hwndTemp = GetDlgItem(hDlg, dlgItemId);
	return SendMessage(hwndTemp, CB_GETCURSEL, 0, 0);
}
