//#import "MyMathsMatrixInterop.tlb" no_namespace, raw_interfaces_only
//#include "Debug\mymathsmatrixinterop.tlh"
#import "MyMathsMatrixInterop.tlb" no_namespace, raw_interfaces_only
#include "Debug\mymathsmatrixinterop.tlh"

// headers
#include <Windows.h>
#include <stdio.h>

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// global variable declarations
SAFEARRAY arr1[4][4];
int arr2[4][4];
int arr3[4][4];
SAFEARRAYBOUND sab1[2];
SAFEARRAYBOUND sab2[2];
SAFEARRAY* sa1;
SAFEARRAY* sa2;
SAFEARRAY* theResult;

// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("Com Class Factory Client");
	HRESULT hr; // New wrt COM interface

				// COM Initialization
	hr = CoInitialize(NULL);
	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("COM library can not be initialized.\nProgram will now exit."), TEXT("Program Error"), MB_OK);
		exit(0);
	}

	// code
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc; // NOTE
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindow(szAppName,
		TEXT("Client Of CSharp Dll Server"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	// message loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	// COM UnInitialization
	CoUninitialize();

	return((int)msg.wParam);
}

void PrepareArrays()
{
	/*arr1[0][0] = 1; arr1[0][1] = 2; arr1[0][2] = 3; arr1[0][3] = 4;
	arr1[1][0] = 1; arr1[1][1] = 5; arr1[1][2] = 6; arr1[1][3] = 1;
	arr1[2][0] = 1; arr1[2][1] = 8; arr1[2][2] = 9; arr1[2][3] = 1;
	arr1[3][0] = 1; arr1[3][1] = 1; arr1[3][2] = 10; arr1[3][3] = 10;

	arr2[0][0] = 1; arr2[0][1] = 15; arr2[0][2] = 1; arr2[0][3] = 22;
	arr2[1][0] = 1; arr2[1][1] = 33; arr2[1][2] = 1; arr2[1][3] = 1;
	arr2[2][0] = 121; arr2[2][1] = 1; arr2[2][2] = 23; arr2[2][3] = 1;
	arr2[3][0] = 23; arr2[3][1] = 23; arr2[3][2] = 1; arr2[3][3] = 55;*/
	//SAFEARRAYBOUND sab[2];
	sab1[0].cElements = 4;
	sab1[0].lLbound = 0;

	sab1[1].cElements = 4;
	sab1[1].lLbound = 0;

	sa1 = SafeArrayCreate(VT_I4, 2, sab1);
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			LONG index[2] = { i,j };
			int value = 1 + i * 2 + j;
			SafeArrayPutElement(sa1, index, &value);
		}
	}

	/*ptr->TestInt2DArray(sa1);
	SafeArrayDestroy(sa1);*/
	sab2[0].cElements = 4;
	sab2[0].lLbound = 0;
	sab2[1].cElements = 4;
	sab2[1].lLbound = 0;

	sa2 = SafeArrayCreate(VT_I4, 2, sab2);
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			LONG index[2] = { i,j };
			int value = 1 + i * 2 + j;
			SafeArrayPutElement(sa2, index, &value);
		}
	}

}

void ExtractResult()
{
	VARTYPE vt;
	SafeArrayGetVartype(theResult, &vt);

	if (vt == VT_I4)
	{
		LONG begin[2]{ 0 };
		LONG end[2]{ 0 };

		SafeArrayGetLBound(theResult, 1, &begin[0]);
		SafeArrayGetLBound(theResult, 2, &begin[1]);
		SafeArrayGetUBound(theResult, 1, &end[0]);
		SafeArrayGetUBound(theResult, 2, &end[1]);

		for (LONG i = begin[0]; i <= end[0]; ++i)
		{
			for (LONG j = begin[1]; j <= end[1]; ++j)
			{
				LONG index[2]{ i,j };
				int value;
				SafeArrayGetElement(theResult, index, &value);
				arr3[i][j] = value;
			}
		}
	}

}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declarations
	void SafeInterfaceRelease(void);
	void ComErrorDescriptionString(HWND, HRESULT);

	// Variable declarations
	HRESULT hr;
	int iNum1, iNum2;
	TCHAR str[255];

	CLSID clsidMyMathIDisp;
	DISPID dispid;
	IMatrix *pIMatrix;
	wchar_t fnMultiply[] = L"Multiply";
	wchar_t fnAdd[] = L"Add";
	wchar_t fnSubtract[] = L"Subtract";
	OLECHAR *szFunctionName = fnMultiply;
	VARIANT vArg[2], vRet;
	DISPPARAMS param = { vArg, 0, 2, NULL };

	// code
	switch (iMsg)
	{
	case WM_CREATE:

		hr = CLSIDFromProgID(L"MyMathsMatrixInterop.MyMatrix", &clsidMyMathIDisp);
		if (FAILED(hr))
		{
			ComErrorDescriptionString(hwnd, hr);
			DestroyWindow(hwnd);
		}

		hr = CoCreateInstance(clsidMyMathIDisp, NULL, CLSCTX_INPROC_SERVER, __uuidof(IMatrix), (void **)&pIMatrix);
		if (FAILED(hr))
		{
			ComErrorDescriptionString(hwnd, hr);
			DestroyWindow(hwnd);
		}

		PrepareArrays();
		pIMatrix->Multiply(sa1, sa2, &theResult);
		/**
		//wcscpy_s(szFunctionName, 8, L"Multiply");
		iNum1 = 75;
		iNum2 = 25;

		VariantInit(vArg);
		PrepareArrays();
		vArg[0].vt = VT_ARRAY;
		vArg[0].parray = sa1;
		vArg[1].vt = VT_ARRAY;
		vArg[1].parray = sa2;
		param.cArgs = 2;
		param.cNamedArgs = 0;
		param.rgdispidNamedArgs = NULL;
		param.rgvarg = vArg;

		VariantInit(&vRet);

		// part 1] 
		hr = pIDispatch->GetIDsOfNames(IID_NULL,
			&szFunctionName,
			1,
			GetUserDefaultLCID(),
			&dispid);

		if (FAILED(hr))
		{
			ComErrorDescriptionString(hwnd, hr);
			MessageBox(hwnd, TEXT("Can not get ID for Multiply."), TEXT("Com Error"), MB_OK | MB_ICONERROR | MB_TOPMOST);
			pIDispatch->Release();
			DestroyWindow(hwnd);
		}

		hr = pIDispatch->Invoke(dispid,
			IID_NULL,
			GetUserDefaultLCID(),
			DISPATCH_METHOD,
			&param,
			&vRet,
			NULL,
			NULL);
			*/
		
		if (FAILED(hr))
		{
			ComErrorDescriptionString(hwnd, hr);
			MessageBox(hwnd, TEXT("Can not invoke function Multiply."), TEXT("Com Error"), MB_OK | MB_ICONERROR | MB_TOPMOST);
			pIMatrix->Release();
			DestroyWindow(hwnd);
		}
		else
		{
			//theResult = vRet.parray;
			ExtractResult();
			//wsprintf(str, TEXT("Multiply of %d and %d = %d"), iNum1, iNum2, vRet.lVal);
			MessageBox(hwnd, str, TEXT("Multiply"), MB_OK);
		}

		hr = pIMatrix->Add(sa1, sa2, &theResult);
		ExtractResult();
		if (FAILED(hr))
		{
			ComErrorDescriptionString(hwnd, hr);
			MessageBox(hwnd, TEXT("Can not invoke function Add."), TEXT("Com Error"), MB_OK | MB_ICONERROR | MB_TOPMOST);
			pIMatrix->Release();
			DestroyWindow(hwnd);
		}
		else
		{
			//theResult = vRet.parray;
			ExtractResult();
			//wsprintf(str, TEXT("Multiply of %d and %d = %d"), iNum1, iNum2, vRet.lVal);
			MessageBox(hwnd, "Yahhooo!! Add", TEXT("Multiply"), MB_OK);
		}

		// clean up
		SafeArrayDestroy(sa1);
		SafeArrayDestroy(sa2);
		VariantClear(vArg);
		VariantClear(&vRet);
		pIMatrix->Release();
		pIMatrix = NULL;

		DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ComErrorDescriptionString(HWND hwnd, HRESULT hr)
{
	TCHAR *szErrorMessage = NULL;
	TCHAR str[255];

	if (FACILITY_WINDOWS == HRESULT_FACILITY(hr))
	{
		hr = HRESULT_CODE(hr);
	}
	// typecast
	if (FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, NULL,
		hr, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&szErrorMessage, 0, NULL) != 0)
	{
		sprintf_s(str, TEXT("%#x : %s"), hr, szErrorMessage);
		LocalFree(szErrorMessage);
	}
	else
		sprintf_s(str, TEXT("[Could not find a description for error # %#x.]\n"), hr);

	MessageBox(hwnd, str, TEXT("Com Error"), MB_OK);
}
