#pragma once
extern "C" bool InitializeMathsWrapper(HWND hwnd);
extern "C" void UninitializeMathsWrapper();
extern "C" bool GetAddition(HWND hwnd, int arr1[4][4], int arr2[4][4], int result[4][4]);
extern "C" bool GetSubtraction(HWND hwnd, int arr1[4][4], int arr2[4][4], int result[4][4]);
extern "C" bool GetMultiplication(HWND hwnd, int arr1[4][4], int arr2[4][4], int result[4][4]);
