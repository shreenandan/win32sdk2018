#pragma once 
#include "PhysicsHelper.h"
#include <string.h>
#include "PhysicsDialogBox.h"
#include "PCMBRadioButton.h"

BandColors GetIndexFromString(char* searchColor)
{
	for (int i = 0; i < 12; i++)
	{
		if (strcmp(searchColor, BandColorName[i]) == 0)
		{
			return (BandColors)i;
		}
	}
	return (BandColors)-1;
}

BandColors FetchBand(HWND hDlg, int dlgItemId)
{
	HWND hwndTemp;
	int idx_row;
	char cmbText[15] = "";

	hwndTemp = GetDlgItem(hDlg, dlgItemId);
	idx_row = SendMessage(hwndTemp, CB_GETCURSEL, 0, 0);
	SendMessage(hwndTemp, CB_GETLBTEXT, idx_row, (LPARAM)cmbText);
	return GetIndexFromString(cmbText);
}

void LoadPhysicsCombos(HWND hDlg)
{
	HWND cbo;

	cbo = GetDlgItem(hDlg, ID_PHYSICS_CBBAND1);
	for (int i = 0; i < 12; i++)
	{
		if (i == Black || i == Gold || i == Silver)
			continue;
		SendMessage(cbo, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCTSTR)BandColorName[i]);
	}
	SendMessage(cbo, CB_SETCURSEL, (WPARAM)0, (LPARAM)0);

	cbo = GetDlgItem(hDlg, ID_PHYSICS_CBBAND2);
	for (int i = 0; i < 12; i++)
	{
		if (i == Gold || i == Silver)
			continue;
		SendMessage(cbo, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCTSTR)BandColorName[i]);
	}
	SendMessage(cbo, CB_SETCURSEL, (WPARAM)0, (LPARAM)0);

	cbo = GetDlgItem(hDlg, ID_PHYSICS_CBBAND3);
	for (int i = 0; i < 12; i++)
	{
		SendMessage(cbo, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCTSTR)BandColorName[i]);
	}
	SendMessage(cbo, CB_SETCURSEL, (WPARAM)0, (LPARAM)0);

	cbo = GetDlgItem(hDlg, ID_PHYSICS_CBBAND4);
	for (int i = 0; i < 12; i++)
	{
		if (i == Black || i == White)
			continue;
		SendMessage(cbo, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCTSTR)BandColorName[i]);
	}
	SendMessage(cbo, CB_SETCURSEL, (WPARAM)0, (LPARAM)0);

}

void EnablePhysics(HWND hDlg)
{
	EnableWindow(GetDlgItem(hDlg, ID_PHYSICS_CBBAND1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_PHYSICS_CBBAND2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_PHYSICS_CBBAND3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_PHYSICS_CBBAND4), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_PHYSICS_RESISTANCE), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_PHYSICS_TOLERANCE), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_CALCULATE_PHYSICS), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_PHYSICS_SUBMIT), TRUE);
}

void DisablePhysics(HWND hDlg)
{
	EnableWindow(GetDlgItem(hDlg, ID_PHYSICS_CBBAND1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_PHYSICS_CBBAND2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_PHYSICS_CBBAND3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_PHYSICS_CBBAND4), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_PHYSICS_RESISTANCE), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_PHYSICS_TOLERANCE), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_CALCULATE_PHYSICS), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_PHYSICS_SUBMIT), FALSE);
}

bool WriteResultToFilePhysics(char *fileName, BandColors b1, BandColors b2, BandColors b3, BandColors b4, ResistanceToleranceResult theResult, const char* BandColorName[])
{
	FILE *fp;
	fopen_s(&fp, fileName, "w");

	fprintf(fp, "\tPhysics Result\n");
	fputs("=============================\n", fp);
	fprintf(fp, "%-15.15s\t%-15.15s \n", "Band 1", BandColorName[b1]);
	fprintf(fp, "%-15.15s\t%-15.15s \n", "Band 2", BandColorName[b2]);
	fprintf(fp, "%-15.15s\t%-15.15s \n", "Band 3", BandColorName[b3]);
	fprintf(fp, "%-15.15s\t%-15.15s \n", "Band 4", BandColorName[b4]);
	fprintf(fp, "%-15.15s\t", "Tolerance");
	fwprintf(fp, L"%-10.10s \n", theResult.Tolerance);
	fprintf(fp, "%-15.15s\t%.5lf \n", "Resistance", theResult.Resistance);
	fclose(fp);

	return true;
}
