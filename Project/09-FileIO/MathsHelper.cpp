#pragma once
#include "MathsHelper.h"
#include "MathsDialogBox.h"
#include <stdio.h>
#include "CommonPCMB.h"
#include <sys/types.h>
#include <direct.h>
#include <stdlib.h>
#include <string.h>

MathsHelper::MathsHelper()
{

}

void MathsHelper::EnableMaths(HWND hDlg)
{
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R1C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R1C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R1C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R1C4), TRUE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R2C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R2C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R2C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R2C4), TRUE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R3C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R3C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R3C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R3C4), TRUE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R4C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R4C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R4C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R4C4), TRUE);


	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R1C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R1C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R1C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R1C4), TRUE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R2C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R2C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R2C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R2C4), TRUE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R3C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R3C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R3C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R3C4), TRUE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R4C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R4C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R4C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R4C4), TRUE);


	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R1C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R1C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R1C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R1C4), TRUE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R2C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R2C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R2C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R2C4), TRUE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R3C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R3C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R3C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R3C4), TRUE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R4C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R4C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R4C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R4C4), TRUE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_CB_OPERATION), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_CALCULATE_MATHS), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_SUBMIT), TRUE);
}

void MathsHelper::DisableMaths(HWND hDlg)
{
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R1C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R1C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R1C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R1C4), FALSE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R2C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R2C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R2C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R2C4), FALSE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R3C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R3C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R3C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R3C4), FALSE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R4C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R4C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R4C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R4C4), FALSE);


	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R1C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R1C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R1C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R1C4), FALSE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R2C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R2C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R2C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R2C4), FALSE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R3C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R3C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R3C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R3C4), FALSE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R4C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R4C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R4C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R4C4), FALSE);


	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R1C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R1C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R1C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R1C4), FALSE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R2C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R2C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R2C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R2C4), FALSE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R3C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R3C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R3C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R3C4), FALSE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R4C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R4C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R4C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R4C4), FALSE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_CB_OPERATION), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_CALCULATE_MATHS), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_SUBMIT), FALSE);
}

void MathsHelper::LoadMathsCombo(HWND hDlg)
{
	HWND cbo;
	cbo = GetDlgItem(hDlg, ID_MATHS_CB_OPERATION);
	SendMessage(cbo, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCTSTR)"+");
	SendMessage(cbo, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCTSTR)"-");
	SendMessage(cbo, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCTSTR)"*");
	SendMessage(cbo, CB_SETCURSEL, (WPARAM)0, (LPARAM)0);
}

void MathsHelper::ReadArray(HWND hDlg, ArrayId id)
{
	char theText[20];
	BOOL translated;
	UINT res;

	switch (id)
	{
	case Arr1:
		this->arr1[0][0] = (int)GetDlgItemInt(hDlg, ID_MATHS_M1_R1C1, &translated, TRUE);
		this->arr1[0][1] = (int)GetDlgItemInt(hDlg, ID_MATHS_M1_R1C2, &translated, TRUE);
		this->arr1[0][2] = (int)GetDlgItemInt(hDlg, ID_MATHS_M1_R1C3, &translated, TRUE);
		this->arr1[0][3] = (int)GetDlgItemInt(hDlg, ID_MATHS_M1_R1C4, &translated, TRUE);
		this->arr1[1][0] = (int)GetDlgItemInt(hDlg, ID_MATHS_M1_R2C1, &translated, TRUE);
		this->arr1[1][1] = (int)GetDlgItemInt(hDlg, ID_MATHS_M1_R2C2, &translated, TRUE);
		this->arr1[1][2] = (int)GetDlgItemInt(hDlg, ID_MATHS_M1_R2C3, &translated, TRUE);
		this->arr1[1][3] = (int)GetDlgItemInt(hDlg, ID_MATHS_M1_R2C4, &translated, TRUE);
		this->arr1[2][0] = (int)GetDlgItemInt(hDlg, ID_MATHS_M1_R3C1, &translated, TRUE);
		this->arr1[2][1] = (int)GetDlgItemInt(hDlg, ID_MATHS_M1_R3C2, &translated, TRUE);
		this->arr1[2][2] = (int)GetDlgItemInt(hDlg, ID_MATHS_M1_R3C3, &translated, TRUE);
		this->arr1[2][3] = (int)GetDlgItemInt(hDlg, ID_MATHS_M1_R3C4, &translated, TRUE);
		this->arr1[3][0] = (int)GetDlgItemInt(hDlg, ID_MATHS_M1_R4C1, &translated, TRUE);
		this->arr1[3][1] = (int)GetDlgItemInt(hDlg, ID_MATHS_M1_R4C2, &translated, TRUE);
		this->arr1[3][2] = (int)GetDlgItemInt(hDlg, ID_MATHS_M1_R4C3, &translated, TRUE);
		this->arr1[3][3] = (int)GetDlgItemInt(hDlg, ID_MATHS_M1_R4C4, &translated, TRUE);
		break;

	case Arr2:
		this->arr2[0][0] = (int)GetDlgItemInt(hDlg, ID_MATHS_M2_R1C1, &translated, TRUE);
		this->arr2[0][1] = (int)GetDlgItemInt(hDlg, ID_MATHS_M2_R1C2, &translated, TRUE);
		this->arr2[0][2] = (int)GetDlgItemInt(hDlg, ID_MATHS_M2_R1C3, &translated, TRUE);
		this->arr2[0][3] = (int)GetDlgItemInt(hDlg, ID_MATHS_M2_R1C4, &translated, TRUE);
		this->arr2[1][0] = (int)GetDlgItemInt(hDlg, ID_MATHS_M2_R2C1, &translated, TRUE);
		this->arr2[1][1] = (int)GetDlgItemInt(hDlg, ID_MATHS_M2_R2C2, &translated, TRUE);
		this->arr2[1][2] = (int)GetDlgItemInt(hDlg, ID_MATHS_M2_R2C3, &translated, TRUE);
		this->arr2[1][3] = (int)GetDlgItemInt(hDlg, ID_MATHS_M2_R2C4, &translated, TRUE);
		this->arr2[2][0] = (int)GetDlgItemInt(hDlg, ID_MATHS_M2_R3C1, &translated, TRUE);
		this->arr2[2][1] = (int)GetDlgItemInt(hDlg, ID_MATHS_M2_R3C2, &translated, TRUE);
		this->arr2[2][2] = (int)GetDlgItemInt(hDlg, ID_MATHS_M2_R3C3, &translated, TRUE);
		this->arr2[2][3] = (int)GetDlgItemInt(hDlg, ID_MATHS_M2_R3C4, &translated, TRUE);
		this->arr2[3][0] = (int)GetDlgItemInt(hDlg, ID_MATHS_M2_R4C1, &translated, TRUE);
		this->arr2[3][1] = (int)GetDlgItemInt(hDlg, ID_MATHS_M2_R4C2, &translated, TRUE);
		this->arr2[3][2] = (int)GetDlgItemInt(hDlg, ID_MATHS_M2_R4C3, &translated, TRUE);
		this->arr2[3][3] = (int)GetDlgItemInt(hDlg, ID_MATHS_M2_R4C4, &translated, TRUE);
		break;

	case ArrRes:
		// No reading only writing
		break;

	default:
		break;
	}

}

void MathsHelper::WriteResult(HWND hDlg)
{
	char temp[8];

	sprintf_s(temp, 7, "%d", this->arrRes[0][0]); SetDlgItemText(hDlg, ID_MATHS_M3_R1C1, temp);
	sprintf_s(temp, 7, "%d", this->arrRes[0][1]); SetDlgItemText(hDlg, ID_MATHS_M3_R1C2, temp);
	sprintf_s(temp, 7, "%d", this->arrRes[0][2]); SetDlgItemText(hDlg, ID_MATHS_M3_R1C3, temp);
	sprintf_s(temp, 7, "%d", this->arrRes[0][3]); SetDlgItemText(hDlg, ID_MATHS_M3_R1C4, temp);
	sprintf_s(temp, 7, "%d", this->arrRes[1][0]); SetDlgItemText(hDlg, ID_MATHS_M3_R2C1, temp);
	sprintf_s(temp, 7, "%d", this->arrRes[1][1]); SetDlgItemText(hDlg, ID_MATHS_M3_R2C2, temp);
	sprintf_s(temp, 7, "%d", this->arrRes[1][2]); SetDlgItemText(hDlg, ID_MATHS_M3_R2C3, temp);
	sprintf_s(temp, 7, "%d", this->arrRes[1][3]); SetDlgItemText(hDlg, ID_MATHS_M3_R2C4, temp);
	sprintf_s(temp, 7, "%d", this->arrRes[2][0]); SetDlgItemText(hDlg, ID_MATHS_M3_R3C1, temp);
	sprintf_s(temp, 7, "%d", this->arrRes[2][1]); SetDlgItemText(hDlg, ID_MATHS_M3_R3C2, temp);
	sprintf_s(temp, 7, "%d", this->arrRes[2][2]); SetDlgItemText(hDlg, ID_MATHS_M3_R3C3, temp);
	sprintf_s(temp, 7, "%d", this->arrRes[2][3]); SetDlgItemText(hDlg, ID_MATHS_M3_R3C4, temp);
	sprintf_s(temp, 7, "%d", this->arrRes[3][0]); SetDlgItemText(hDlg, ID_MATHS_M3_R4C1, temp);
	sprintf_s(temp, 7, "%d", this->arrRes[3][1]); SetDlgItemText(hDlg, ID_MATHS_M3_R4C2, temp);
	sprintf_s(temp, 7, "%d", this->arrRes[3][2]); SetDlgItemText(hDlg, ID_MATHS_M3_R4C3, temp);
	sprintf_s(temp, 7, "%d", this->arrRes[3][3]); SetDlgItemText(hDlg, ID_MATHS_M3_R4C4, temp);
}

void MathsHelper::ResetValues()
{
	this->isSuccessful = false;
	this->currentSelection = MathsOps::ADD;
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			this->arr1[i][j] = 0;
			this->arr2[i][j] = 0;
			this->arrRes[i][j] = 0;
		}
	}
}


class FileGetter {
	WIN32_FIND_DATAA found;
	HANDLE hfind;
	char folderstar[255];
	int chk;

public:
	FileGetter(const char* folder) {
		sprintf_s(folderstar, "%s*.*", folder);
		hfind = FindFirstFileA(folderstar, &found);
		//skip .
		FindNextFileA(hfind, &found);
	}

	int getNextFile(char* fname) {
		//skips .. when called for the first time
		chk = FindNextFileA(hfind, &found);
		if (chk)
			strcpy_s(fname, 256, found.cFileName);
		return chk;
	}

};

int myCompareInt(const void * a, const void * b)
{
	//return (*(int*)a > *(int*)b) ? 1 : ((*(int*)a = *(int*)b) ? 0 : -1);
	return (*(int*)b - *(int*)a); // we want descending
}

int myCompare(const void * a, const void * b)
{
	return strcmp((char *)a, (char *)b);
	//return strcmp(*( char **)a, *( char **)b);
}

void sort(char(*arr)[256], int n)
{
	qsort((void *)arr, 1, sizeof(char *), myCompare);
	//qsort(arr, n, 256, myCompare);
}

void MathsHelper::list_dir(const char *path, HWND hDlg)
{
	/*struct dirent *entry;
	DIR *dir = opendir(path);
	if (dir == NULL) {
		return;
	}

	while ((entry = readdir(dir)) != NULL) {
		printf("%s\n", entry->d_name);
	}

	closedir(dir);*/
	/*DIR *dp;
	struct dirent *ep;

	dp = opendir("./");
	if (dp != NULL)
	{
		while (ep = readdir(dp))
			puts(ep->d_name);
		(void)closedir(dp);
	}
	else
		perror("Couldn't open the directory");*/
	char command[50] = "dir /B";
	printf("%d", system(command));
	getchar();
	char buff[FILENAME_MAX];
	_getcwd(buff, FILENAME_MAX);
	const char *folder = "";
	FileGetter *fg = new FileGetter(folder);
	SetDlgItemText(hDlg, ID_FOLDER, buff);

	strcat_s(buff, MAX_PATH, "\\");
	strcat_s(buff, MAX_PATH, MATHS_FILE_SEARCHPATTERN); // TEXT("\\Chemistry*"));
	HANDLE hFind = INVALID_HANDLE_VALUE;
	WIN32_FIND_DATA ffd;
	char foundFiles[9][256];
	memset(foundFiles, '0', 9 * 256 * sizeof(char));
	int index = 0;
	int fileNos[256];
	char *next_token1 = NULL;

	// Find the first file in the directory.
	hFind = FindFirstFile(buff, &ffd);

	if (INVALID_HANDLE_VALUE == hFind)
	{
		MessageBox(hDlg, TEXT("No files found"), "0 Records", MB_OK | MB_ICONERROR);
	}

	// List all the files in the directory with some info about them.
	do
	{
		if (!(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
		{
			SetDlgItemText(hDlg, ID_FOLDER, (TEXT("%s "), ffd.cFileName));
			strcpy_s(foundFiles[index], 256, ffd.cFileName);
			//foundFiles[index++]=ffd.cFileName;

			char *token = strtok_s(ffd.cFileName, "-", &next_token1);
			unsigned short cntr = 1;
			// Keep printing tokens while one of the 
			// delimiters present in str[]. 
			while (token != NULL && cntr <= 2)
			{
				printf("%s\n", token);
				token = strtok_s(NULL, "-", &next_token1);
				cntr++;
			}
			fileNos[index] = atoi(token);
			index++;
		}
	} while (FindNextFile(hFind, &ffd) != 0);

	DWORD dwError = 0;
	dwError = GetLastError();
	if (dwError != ERROR_NO_MORE_FILES)
	{
		MessageBox(hDlg, TEXT("FindFirstFile Error occured"), "Error", MB_OK | MB_ICONERROR);
	}

	FindClose(hFind);

	//sort(foundFiles, index);
	qsort((void *)foundFiles, (size_t)index, 256, myCompare);
	qsort((void *)fileNos, (size_t)index, sizeof(int), myCompareInt);

}

bool MathsHelper::WriteResultToFile(char *fileName)
{
	FILE *fp;
	fopen_s(&fp, fileName, "w");

	fprintf(fp, "\t\tMaths Result\n");
	fputs("======================================\n", fp);
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			fprintf(fp, "\t%d", this->arr1[i][j]);
		}
		fprintf(fp, "\n");
	}
	fprintf(fp, "\n\t\t");
	fprintf(fp, this->currentSelection == MathsOps::ADD ? "+" : (this->currentSelection == MathsOps::MUL ? "X" : "-"));
	fprintf(fp, "\n\n");

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			fprintf(fp, "\t%d", this->arr2[i][j]);
		}
		fprintf(fp, "\n");
	}
	fprintf(fp, "\n\t\tResult\n\n");
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			fprintf(fp, "\t%d", this->arrRes[i][j]);
		}
		fprintf(fp, "\n");
	}

	fclose(fp);

	return true;
}
