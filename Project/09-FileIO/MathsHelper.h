#pragma once
#include <Windows.h>

enum MathsOps
{
	ADD = 0,
	SUB,
	MUL
};

enum ArrayId
{
	Arr1 = 0,
	Arr2,
	ArrRes
};

class MathsHelper
{
public:
	MathsHelper();
	void EnableMaths(HWND hDlg);
	void DisableMaths(HWND hDlg);
	MathsOps currentSelection;
	bool isSuccessful;
	void LoadMathsCombo(HWND hDlg);
	int arr1[4][4] = { { 0,0,0,0 },{ 0,0,0,0 },{ 0,0,0,0 },{ 0,0,0,0 } };
	int arr2[4][4] = { { 0,0,0,0 },{ 0,0,0,0 },{ 0,0,0,0 },{ 0,0,0,0 } };
	int arrRes[4][4] = { { 0,0,0,0 },{ 0,0,0,0 },{ 0,0,0,0 },{ 0,0,0,0 } };
	void ReadArray(HWND hDlg, ArrayId id);
	void WriteResult(HWND hDlg);
	void ResetValues();
	void list_dir(const char *path, HWND hDlg);
	bool WriteResultToFile(char *fileName);
};

static const char *MATHS_LIB_NAME = "MathsCOMInteropWrapper.dll";
static const char *MATHS_FN_INIT = "InitializeMathsWrapper";
static const char *MATHS_FN_UNINIT = "UninitializeMathsWrapper";
static const char *MATHS_FN_ADD = "GetAddition";
static const char *MATHS_FN_SUB = "GetSubtraction";
static const char *MATHS_FN_MUL = "GetMultiplication";

typedef bool(*pfnMathsFunc)(HWND, int [4][4], int [4][4], int [4][4]);
typedef bool(*pfnInitFuncHwnd)(HWND);
static pfnMathsFunc mathsFunc = NULL;
static pfnInitFuncHwnd initFuncHwnd = NULL;

static const char *MATHS_FILE_NAME = "Maths-State-1-Log.txt";
static const char *MATHS_FILE_SEARCHPATTERN = "Maths-State-*-Log.txt";
static const char *MATHS_FILE_SAVE_TITLE = "MATHS FILE SAVE MESSAGE";
static const char *MATHS_FILE_SAVE_MESSAGE = "The current state of Maths file has been saved in a file by the name 'thefilename' in the current directory.";
