#pragma once 
#include "CommonPCMB.h"
#include <stdio.h>
#include <direct.h>

void ShowPCMBDialog(HINSTANCE hInst, HWND hwnd, char* theMessage, RECT rc)
{
	hInst = (HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE);
	// call to DialogBox
	DialogBox(hInst, "PCMBDlg", hwnd, PCMBDlgProc);
}

int GetComboSelectedIndex(HWND hDlg, int dlgItemId)
{
	HWND hwndTemp;
	hwndTemp = GetDlgItem(hDlg, dlgItemId);
	return SendMessage(hwndTemp, CB_GETCURSEL, 0, 0);
}

int myCompareInt(const void * a, const void * b)
{
	return (*(int*)b - *(int*)a); // we want descending
}

// NOTE: Reference https://docs.microsoft.com/en-us/cpp/c-runtime-library/reference/qsort?view=vs-2017
// NOTE: Reference https://docs.microsoft.com/en-us/windows/desktop/fileio/listing-the-files-in-a-directory
char* myReplace(char* str, const char* a, const char* b)
{
	int len = strlen(str);
	int lena = strlen(a), lenb = strlen(b);
	for (char* p = str; p = strstr(p, a); ++p) {
		if (lena != lenb) // shift end as needed
			memmove(p + lenb, p + lena,
				len - (p - str) + lenb);
		memcpy(p, b, lenb);
	}
	return str;
}

void GetNextFileName(const char* searchPattern, char theFileName[], char onlyFileName[])
{
	char buff[FILENAME_MAX];
	_getcwd(buff, FILENAME_MAX);

	strcat_s(buff, MAX_PATH, "\\");
	strcat_s(buff, MAX_PATH, searchPattern);

	int index = 0;
	int fileNos[50];
	char *next_token1 = NULL;
	char temp[260], nextNo[5];
	strcpy_s(temp, 260, buff);
	unsigned short cntr = 1;

	HANDLE hFind = INVALID_HANDLE_VALUE;
	WIN32_FIND_DATA ffd;

	strcpy_s(onlyFileName, 260, searchPattern);
	// Find the first file in the directory.
	hFind = FindFirstFile(buff, &ffd);
	if (INVALID_HANDLE_VALUE == hFind)
	{
		FindClose(hFind);
		myReplace(temp, "*", "1");
		strcpy_s(theFileName, 260, temp);
		myReplace(onlyFileName, "*", "1");
		return;
	}

	// List all the files in the directory with some info about them.
	do
	{
		if (!(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
		{
			char *token = strtok_s(ffd.cFileName, "-", &next_token1);
			cntr = 1;
			// Keep printing tokens while one of the 
			// delimiters present in str[]. 
			while (token != NULL && cntr <= 2)
			{
				token = strtok_s(NULL, "-", &next_token1);
				cntr++;
			}
			fileNos[index] = atoi(token);
			index++;
		}
	} while (FindNextFile(hFind, &ffd) != 0);

	DWORD dwError = 0;
	dwError = GetLastError();
	if (dwError != ERROR_NO_MORE_FILES)
	{
		FindClose(hFind);
		myReplace(temp, "*", "1");
		strcpy_s(theFileName, 260, temp);
		myReplace(onlyFileName, "*", "1");
		return;
	}

	FindClose(hFind);
	qsort((void *)fileNos, (size_t)index, sizeof(int), myCompareInt);
	sprintf_s(nextNo, 5, "%d", fileNos[0] + 1);
	strcpy_s(temp, 260, buff);
	myReplace(temp, "*", nextNo);
	myReplace(onlyFileName, "*", nextNo);

	strcpy_s(theFileName, 260, temp);
}

void GetNextFileNameBase(const char* searchPattern, char theFileName[])
{
	char buff[FILENAME_MAX];
	_getcwd(buff, FILENAME_MAX);

	strcat_s(buff, MAX_PATH, "\\");
	strcat_s(buff, MAX_PATH, searchPattern);

	int index = 0;
	int fileNos[50];
	//memset(fileNos, '0', 256 * sizeof(int));
	char *next_token1 = NULL;
	//char *temp = (char*)malloc(sizeof(searchPattern)), *nextNo = (char*)malloc(sizeof(10));
	char temp[30], nextNo[5];
	strcpy_s(temp, 30, searchPattern);
	unsigned short cntr = 1;

	HANDLE hFind = INVALID_HANDLE_VALUE;
	WIN32_FIND_DATA ffd;

	// Find the first file in the directory.
	hFind = FindFirstFile(buff, &ffd);
	if (INVALID_HANDLE_VALUE == hFind)
	{
		myReplace(temp, "-*-", "-1-");
		FindClose(hFind);
		//return temp;
		//theFileName = (char*)malloc(sizeof(temp));
		strcpy_s(theFileName, 30, temp);
	}

	// List all the files in the directory with some info about them.
	do
	{
		if (!(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
		{
			char *token = strtok_s(ffd.cFileName, "-", &next_token1);
			cntr = 1;
			// Keep printing tokens while one of the 
			// delimiters present in str[]. 
			while (token != NULL && cntr <= 2)
			{
				token = strtok_s(NULL, "-", &next_token1);
				cntr++;
			}
			fileNos[index] = atoi(token);
			index++;
		}
	} while (FindNextFile(hFind, &ffd) != 0);

	DWORD dwError = 0;
	dwError = GetLastError();
	if (dwError != ERROR_NO_MORE_FILES)
	{
		myReplace(temp, "-*-", "-1-");
		FindClose(hFind);
		//return temp;
		//theFileName = (char*)malloc(sizeof(temp));
		strcpy_s(theFileName, 30, temp);
	}

	FindClose(hFind);
	qsort((void *)fileNos, (size_t)index, sizeof(int), myCompareInt);
	sprintf_s(nextNo, 5, "%d", fileNos[0] + 1);
	strcpy_s(temp, 30, searchPattern);
	myReplace(temp, "*", nextNo);

	//theFileName = (char*)malloc(sizeof(temp));
	strcpy_s(theFileName, 30, temp);
}
