#pragma once 
#include <Windows.h>

BOOL CALLBACK PCMBDlgProc(HWND, UINT, WPARAM, LPARAM);
void ShowPCMBDialog(HINSTANCE hInst, HWND hwnd, char* theMessage, RECT rc);
int GetComboSelectedIndex(HWND hDlg, int dlgItemId);

void GetNextFileName(const char* searchPattern, char* theFileName, char onlyFileName[]);
char* myReplace(char* str, const char* a, const char* b);
