/*
Project part 5] Design and improve UI as per requirement. Don't load lib before.
Extended Integrate splash screen, dialog box and consume Physics DLL.
Used explicit linking: LoadLibrary(), FreeLibrary(), function pointer.

NOTES: 1] Splash screen will be visible throughout the program.
2] Spalsh screen can not be maximized, resized. If needed to have, can be achieved. Did for learning diff window options.
3] UI Design is improved with validations, backcolor.
4] Has wide char support. Results are shown with UNICODE characters.
5] Main window is always shown at center of the screen.
6] Code is moderately structured.
7] Now DLL lib loaded only during calculation and not before.
*/

// headers
#include <windows.h>
#include "SplashBitmap.h"
#include <stdio.h>
#include "PhysicsDialogBox.h"
#include "PhysicsDefDLLServer.h"
#include "PCMBRadioButton.h"
#include "ChemistryDialogBox.h"
#include "MathsDialogBox.h"

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
BOOL CALLBACK PCMBDlgProc(HWND, UINT, WPARAM, LPARAM);
void ShowPhysicsDialog(HINSTANCE hInst, HWND hwnd, char* theMessage, RECT rc);

void EnablePhysics(HWND hDlg);
void DisablePhysics(HWND hDlg);
void EnableChemistry(HWND hDlg);
void DisableChemistry(HWND hDlg);
void EnableMaths(HWND hDlg);
void DisableMaths(HWND hDlg);

// Globally variables
HINSTANCE theInstance = NULL; // NOTE: Required when Loading Bitmap in WM_CREATE
BandColors b1 = Red, b2 = Green, b3 = Yellow, b4 = Blue;
bool IsPhysicsSubmitted = false;
bool IsChemistrySubmitted = false;
bool IsMathsSubmitted = false;
bool IsBiologySubmitted = false;

// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("PCMB Integration");

	// code
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc; // NOTE
	wndclass.hInstance = hInstance;
	theInstance = hInstance; // Save instance
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// create window at center of the screen
	int xPos = (GetSystemMetrics(SM_CXSCREEN) - 1356) / 2;
	int yPos = 0; // (GetSystemMetrics(SM_CYSCREEN) - 740) / 2; // CW_USEDEFAULT

	hwnd = CreateWindow(szAppName,
		TEXT("PCMB Integration"),
		WS_OVERLAPPEDWINDOW ^ WS_THICKFRAME ^ WS_MAXIMIZEBOX,
		xPos,
		yPos,
		1356,
		739,
		NULL,
		NULL,
		hInstance,
		NULL);

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	// message loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	PAINTSTRUCT ps;
	static RECT rc;
	static HBITMAP hBitmap = NULL; // NOTE: Needs to be maintained across calls
	HDC hdcBitmap = NULL;
	HGDIOBJ rt_gdi1;
	int rt_getObj;
	BOOL rt_copyDc;
	BOOL rt_Del;
	BITMAP bmp;
	static bool IsInitiated = false;
	static char theMessage[350] = "";
	static HINSTANCE hInst = NULL;

	// code
	switch (iMsg)
	{
	case WM_CREATE:
		hBitmap = LoadBitmap(theInstance, MAKEINTRESOURCE(PCMB_SPLASH_BITMAP));
		if (hBitmap == NULL)
		{
			MessageBox(hwnd, TEXT("Error loading bitmap!!"), "NULL", MB_OK | MB_ICONERROR);
			DestroyWindow(hwnd);
		}
		wsprintf(theMessage, "Please press space bar to proceed.");
		break;

	case WM_PAINT:
		// Keep splash screen loaded all the time
		HFONT hFont;
		hdc = BeginPaint(hwnd, &ps);
		hdcBitmap = CreateCompatibleDC(hdc);
		rt_gdi1 = SelectObject(hdcBitmap, hBitmap); //hgdiobj
		rt_getObj = GetObject(hBitmap, sizeof(BITMAP), (LPVOID*)&bmp);
		rt_copyDc = BitBlt(hdc, 0, 0, bmp.bmWidth, bmp.bmHeight, hdcBitmap, 0, 0, SRCCOPY);
		rt_gdi1 = SelectObject(hdc, hBitmap);
		rt_Del = DeleteDC(hdcBitmap); // NOTE: This will retain the image as "hdc" is maintained by deleting hdcBitmap

		GetClientRect(hwnd, &rc);
		SetTextColor(hdc, RGB(255, 0, 0));
		SetBkColor(hdc, RGB(255, 255, 255)); // RGB(60, 24, 114)
		hFont = CreateFont(28, 14, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_OUTLINE_PRECIS,
			CLIP_DEFAULT_PRECIS, CLEARTYPE_QUALITY, VARIABLE_PITCH, TEXT("Times New Roman"));
		SelectObject(hdc, hFont);
		DrawText(hdc, theMessage, -1, &rc, DT_BOTTOM | DT_SINGLELINE | DT_CENTER);
		DeleteObject(hFont);

		rt_Del = DeleteDC(hdc);
		EndPaint(hwnd, &ps);
		break;

	case WM_DESTROY:
		rt_Del = DeleteObject(hBitmap);
		PostQuitMessage(0);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_SPACE:
			wsprintf(theMessage, "Press space bar to display dialog box.");
			IsPhysicsSubmitted = false;
			IsChemistrySubmitted = false;
			IsMathsSubmitted = false;
			IsBiologySubmitted = false;
			InvalidateRect(hwnd, &rc, TRUE);
			ShowPhysicsDialog(hInst, hwnd, theMessage, rc);
			break;
		}
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ShowPhysicsDialog(HINSTANCE hInst, HWND hwnd, char* theMessage, RECT rc)
{
	hInst = (HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE);
	// call to DialogBox
	DialogBox(hInst, "PCMBDlg", hwnd, PCMBDlgProc);
}

BandColors GetIndexFromString(char* searchColor)
{
	for (int i = 0; i < 12; i++)
	{
		if (strcmp(searchColor, BandColorName[i]) == 0)
		{
			return (BandColors)i;
		}
	}
	return (BandColors)-1;
}

BandColors FetchBand(HWND hDlg, int dlgItemId)
{
	HWND hwndTemp;
	int idx_row;
	char cmbText[15] = "";

	hwndTemp = GetDlgItem(hDlg, dlgItemId);
	idx_row = SendMessage(hwndTemp, CB_GETCURSEL, 0, 0);
	SendMessage(hwndTemp, CB_GETLBTEXT, idx_row, (LPARAM)cmbText);
	return GetIndexFromString(cmbText);
}

void LoadPhysicsCombos(HWND hDlg)
{
	HWND cbo;

	cbo = GetDlgItem(hDlg, ID_PHYSICS_CBBAND1);
	for (int i = 0; i < 12; i++)
	{
		if (i == Black || i == Gold || i == Silver)
			continue;
		SendMessage(cbo, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCTSTR)BandColorName[i]);
		SendMessage(cbo, CB_SETCURSEL, (WPARAM)0, (LPARAM)0);
	}

	cbo = GetDlgItem(hDlg, ID_PHYSICS_CBBAND2);
	for (int i = 0; i < 12; i++)
	{
		if (i == Gold || i == Silver)
			continue;
		SendMessage(cbo, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCTSTR)BandColorName[i]);
		SendMessage(cbo, CB_SETCURSEL, (WPARAM)0, (LPARAM)0);
	}

	cbo = GetDlgItem(hDlg, ID_PHYSICS_CBBAND3);
	for (int i = 0; i < 12; i++)
	{
		SendMessage(cbo, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCTSTR)BandColorName[i]);
		SendMessage(cbo, CB_SETCURSEL, (WPARAM)0, (LPARAM)0);
	}

	cbo = GetDlgItem(hDlg, ID_PHYSICS_CBBAND4);
	for (int i = 0; i < 12; i++)
	{
		if (i == Black || i == White)
			continue;
		SendMessage(cbo, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCTSTR)BandColorName[i]);
		SendMessage(cbo, CB_SETCURSEL, (WPARAM)0, (LPARAM)0);
	}

}

BOOL CALLBACK PCMBDlgProc(HWND hDlg, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	static RECT rc;
	wchar_t temp[50];
	ResistanceToleranceResult theResult;
	static HWND hwndTemp;
	static HBRUSH hDlgBrush = CreateSolidBrush(RGB(127, 127, 127));
	long isSelected = 0;

	static HMODULE hLib = NULL;
	typedef ResistanceToleranceResult(*pfnCalcFourBandResistanceTolerance)(BandColors, BandColors, BandColors, BandColors);
	static pfnCalcFourBandResistanceTolerance calcFourBandResistanceTolerance = NULL;

	switch (iMsg)
	{
	case WM_INITDIALOG:
		return (TRUE);

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
			// Now DLL lib loaded only during calculation and not before
		case ID_CALCULATE_PHYSICS:
			isSelected = SendDlgItemMessage(hDlg, ID_ARB_PHYSICS, BM_GETCHECK, 0, 0);
			if (isSelected != 1) // Do only if current radio selected
			{
				MessageBox(hDlg, TEXT("Please select radio option."), TEXT("No Selection!"), MB_ICONEXCLAMATION | MB_OK);
				return TRUE;
			}
			if (IsPhysicsSubmitted) // Do only if not submitted
			{
				MessageBox(hDlg, TEXT("Can not re-submit, already submitted."), TEXT("Submitted!"), MB_ICONEXCLAMATION | MB_OK);
				return TRUE;
			}
			b1 = FetchBand(hDlg, ID_PHYSICS_CBBAND1);
			b2 = FetchBand(hDlg, ID_PHYSICS_CBBAND2);
			b3 = FetchBand(hDlg, ID_PHYSICS_CBBAND3);
			b4 = FetchBand(hDlg, ID_PHYSICS_CBBAND4);

			hLib = LoadLibrary(TEXT("PhysicsDefDLLServer.dll")); // Step 1 Load DLL
			if (hLib == NULL)
			{
				MessageBox(hDlg, TEXT("PhysicsDefDLLServer.dll not found!!"), "Error", MB_OK | MB_ICONERROR);
				DestroyWindow(hDlg);
			}
			calcFourBandResistanceTolerance = (pfnCalcFourBandResistanceTolerance)GetProcAddress(hLib, "CalcFourBandResistanceTolerance"); // Step 2
			theResult = calcFourBandResistanceTolerance(b1, b2, b3, b4); // Step 3 Explicitly linked custom DLL function call
			FreeLibrary(hLib); // Step 4

			if (theResult.IsToleranceSuccessful)
			{
				SetDlgItemTextW(hDlg, ID_PHYSICS_TOLERANCE, theResult.Tolerance);
			}
			if (theResult.IsResistanceSuccessful)
			{
				hwndTemp = GetDlgItem(hDlg, ID_PHYSICS_RESISTANCE);
				swprintf(temp, 40, L"%.2lf", theResult.Resistance); // NOTE: Important This is how float/double values can be displayed. 2 digits after decimal point
				SetWindowTextW(hwndTemp, temp);
			}
			return TRUE;
			break;

		case ID_CALCULATE_CHEMISTRY:
			isSelected = SendDlgItemMessage(hDlg, ID_ARB_CHEMISTRY, BM_GETCHECK, 0, 0);
			if (isSelected != 1) // Do only if current radio selected
			{
				MessageBox(hDlg, TEXT("Please select radio option."), TEXT("No Selection!"), MB_ICONEXCLAMATION | MB_OK);
				return TRUE;
			}
			if (IsChemistrySubmitted) // Do only if not submitted
			{
				MessageBox(hDlg, TEXT("Can not re-submit, already submitted."), TEXT("Submitted!"), MB_ICONEXCLAMATION | MB_OK);
				return TRUE;
			}
			return TRUE;
			break;

		case IDCANCEL:
			EndDialog(hDlg, 0);
			break;

		case ID_ARB_PHYSICS:
			if (IsPhysicsSubmitted) // Do only if not submitted
			{
				MessageBox(hDlg, TEXT("Already submitted."), TEXT("Submitted!"), MB_ICONEXCLAMATION | MB_OK);
				return TRUE;
			}
			LoadPhysicsCombos(hDlg);
			EnablePhysics(hDlg);
			DisableChemistry(hDlg);
			DisableMaths(hDlg);
			break;

		case ID_ARB_CHEMISTRY:
			if (IsChemistrySubmitted)
			{
				MessageBox(hDlg, TEXT("Already submitted."), TEXT("Submitted!"), MB_ICONEXCLAMATION | MB_OK);
				return TRUE;
			}
			EnableChemistry(hDlg);
			DisablePhysics(hDlg);
			DisableMaths(hDlg);
			break;

		case ID_ARB_MATHEMATICS:
			if (IsMathsSubmitted) // Do only if not submitted
			{
				MessageBox(hDlg, TEXT("Already submitted."), TEXT("Submitted!"), MB_ICONEXCLAMATION | MB_OK);
				return TRUE;
			}
			EnableMaths(hDlg);
			DisablePhysics(hDlg);
			DisableChemistry(hDlg);
			return TRUE;
			break;

		case ID_ARB_BIOLOGY:
			if (IsBiologySubmitted)
			{
				MessageBox(hDlg, TEXT("Already submitted."), TEXT("Submitted!"), MB_ICONEXCLAMATION | MB_OK);
				return TRUE;
			}
			DisablePhysics(hDlg);
			DisableChemistry(hDlg);
			DisableMaths(hDlg);
			return TRUE;
			break;

		case ID_PHYSICS_SUBMIT:
			isSelected = SendDlgItemMessage(hDlg, ID_ARB_PHYSICS, BM_GETCHECK, 0, 0);
			if (isSelected != 1) // Do only if current radio selected
			{
				MessageBox(hDlg, TEXT("Please select radio option."), TEXT("No Selection!"), MB_ICONEXCLAMATION | MB_OK);
				return TRUE;
			}
			if (IsPhysicsSubmitted) // Do only if not submitted
			{
				MessageBox(hDlg, TEXT("Can not re-submit, already submitted."), TEXT("Submitted!"), MB_ICONEXCLAMATION | MB_OK);
				return TRUE;
			}
			IsPhysicsSubmitted = true;
			DisablePhysics(hDlg);
			return TRUE;
			break;
		}

	case WM_CTLCOLORDLG:
		//hDlgBrush = CreateSolidBrush(RGB(127, 127, 127));
		return ((BOOL)hDlgBrush);
		break;

	case WM_CTLCOLORSTATIC:
		HDC hdcStatic = (HDC)wParam;
		// Way 1]
		// Reference: http://winprog.org/tutorial/dlgfaq.html
		//SetTextColor(hdcStatic, RGB(255, 0, 0)); // NOTE: Learned new thing to set text color for control defined in RC file
		//SetBkMode(hdcStatic, TRANSPARENT);

		// Way 2]
		// Reference: https://docs.microsoft.com/en-us/windows/desktop/controls/wm-ctlcolorstatic
		SetBkColor(hdcStatic, RGB(127, 127, 127));
		return ((INT_PTR)hDlgBrush);

		// Make EDITTEXT read only. NOTE: Disabled is different than this
		// https://docs.microsoft.com/en-us/windows/desktop/Controls/edit-control-styles
		break;

		return (TRUE);
	}

	return (FALSE);
}

void EnablePhysics(HWND hDlg)
{
	EnableWindow(GetDlgItem(hDlg, ID_PHYSICS_CBBAND1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_PHYSICS_CBBAND2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_PHYSICS_CBBAND3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_PHYSICS_CBBAND4), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_PHYSICS_RESISTANCE), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_PHYSICS_TOLERANCE), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_CALCULATE_PHYSICS), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_PHYSICS_SUBMIT), TRUE);
}

void DisablePhysics(HWND hDlg)
{
	EnableWindow(GetDlgItem(hDlg, ID_PHYSICS_CBBAND1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_PHYSICS_CBBAND2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_PHYSICS_CBBAND3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_PHYSICS_CBBAND4), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_PHYSICS_RESISTANCE), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_PHYSICS_TOLERANCE), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_CALCULATE_PHYSICS), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_PHYSICS_SUBMIT), FALSE);
}

void EnableChemistry(HWND hDlg)
{
	EnableWindow(GetDlgItem(hDlg, ID_CHEM_CB), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_CHEM_FINALPRESSURE), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_CHEM_FINALTEMPERATURE), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_CHEM_INITIALPRESSURE), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_CHEM_INITIALTEMPERATURE), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_CHEM_SUBMIT), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_CALCULATE_CHEMISTRY), TRUE);
}

void DisableChemistry(HWND hDlg)
{
	EnableWindow(GetDlgItem(hDlg, ID_CHEM_CB), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_CHEM_FINALPRESSURE), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_CHEM_FINALTEMPERATURE), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_CHEM_INITIALPRESSURE), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_CHEM_INITIALTEMPERATURE), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_CHEM_SUBMIT), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_CALCULATE_CHEMISTRY), FALSE);
}

void EnableMaths(HWND hDlg)
{
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R1C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R1C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R1C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R1C4), TRUE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R2C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R2C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R2C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R2C4), TRUE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R3C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R3C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R3C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R3C4), TRUE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R4C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R4C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R4C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R4C4), TRUE);


	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R1C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R1C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R1C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R1C4), TRUE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R2C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R2C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R2C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R2C4), TRUE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R3C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R3C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R3C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R3C4), TRUE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R4C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R4C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R4C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R4C4), TRUE);


	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R1C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R1C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R1C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R1C4), TRUE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R2C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R2C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R2C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R2C4), TRUE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R3C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R3C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R3C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R3C4), TRUE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R4C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R4C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R4C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R4C4), TRUE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_CB_OPERATION), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_CALCULATE_MATHS), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_SUBMIT), TRUE);
}

void DisableMaths(HWND hDlg)
{
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R1C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R1C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R1C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R1C4), FALSE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R2C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R2C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R2C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R2C4), FALSE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R3C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R3C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R3C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R3C4), FALSE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R4C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R4C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R4C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R4C4), FALSE);


	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R1C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R1C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R1C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R1C4), FALSE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R2C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R2C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R2C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R2C4), FALSE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R3C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R3C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R3C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R3C4), FALSE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R4C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R4C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R4C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R4C4), FALSE);


	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R1C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R1C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R1C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R1C4), FALSE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R2C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R2C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R2C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R2C4), FALSE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R3C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R3C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R3C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R3C4), FALSE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R4C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R4C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R4C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R4C4), FALSE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_CB_OPERATION), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_CALCULATE_MATHS), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_SUBMIT), FALSE);
}
