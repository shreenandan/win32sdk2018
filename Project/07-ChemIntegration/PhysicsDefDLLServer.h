// NOTE: 1] Wherever required use __declspec(dllexport) or in comments //__declspec(dllexport) also works
// Had tried to see how to do export data from module definition. But didn't had suitable way in it.
// This is important otherwise type is not exposed to client. Faced problems, verified and rectified.

// NOTE: 2] Below macros are required for enum. These were required to get
// enum name in string format at client side. But then why in server?
// Ans: If not done in server, client has to implement logic to convert enum to string.
// But problem was that client had to again create another new enum, resulting in new
// enum type. As there are two different types of logically same enum, there were
// compiler type errors and couldn't pass params to function as specified.
// Second reason to keep logic in server is that code Reusability, Maintenance. Helpful
// if there are more than 1 clients of this same Physics DLL. Otherwise all clients must do
// implementation by themselves, can differ, chances of errors.
// Third reason - Client might alter enum as header is shared with him. New can be added, current
// enum sequence can be altered. If misused it should not affect server processing logic, only 
// the client will get ideally wrong results due to his mishaps. Also in case of multiple clients
// they will not affect each other. Achieve CONSISTENCY.
#pragma once 
#define MAP(X) \
	X(Black) \
	X(Brown) \
	X(Red) \
	X(Orange) \
	X(Yellow) \
	X(Green) \
	X(Blue) \
	X(Violet) \
	X(Grey) \
	X(White) \
	X(Gold) \
	X(Silver)

#define getInt(x) x,
#define getString(x) #x,

__declspec(dllexport)
typedef enum {
	MAP(getInt)
} BandColors;

__declspec(dllexport)
char* BandColorName[] = {
	MAP(getString)
};

// NOTE: Example to show even commented dllexport works.
//__declspec(dllexport)
struct ResistanceToleranceResult
{
	double Resistance;
	wchar_t Tolerance[10];
	char Messages[250];
	bool IsResistanceSuccessful;
	bool IsToleranceSuccessful;
};

// Returned will be ResistanceToleranceResult structure
// with 4 input params for band 1, 2, 3, 4
// and 5 output params - success flags, messages, result
extern "C" ResistanceToleranceResult CalcFourBandResistanceTolerance(BandColors band1,
	BandColors band2,
	BandColors band3,
	BandColors band4);
