#pragma once 
#include "PhysicsDefDLLServer.h"
#include <Windows.h>

BandColors GetIndexFromString(char* searchColor);
/*
NOTE: Important learning got during refactoring

Was stuck in this LNK2005 link error. Reference: https://msdn.microsoft.com/en-us/library/72zdcz6f.aspx
Error	1	error LNK2005 : "char * * BandColorName" (? BandColorName@@3PAPADA) already defined in PCMBIntegration.obj	G : \Win32Com\Project\ChemistryIntegrationSoln\ChemistryIntegration\PhysicsHelper.obj	ChemistryIntegration
Error	2	error LNK1169 : one or more multiply defined symbols found	G : \Win32Com\Project\ChemistryIntegrationSoln\Debug\ChemistryIntegration.exe	1	1	ChemistryIntegration

Added /FORCE:MULTIPLE linker option. Linked successfully and program ran correctly as expected.

Error explanation https://support.microsoft.com/en-us/help/148652/a-lnk2005-error-occurs-when-the-crt-library-and-mfc-libraries-are-link
*/

BandColors FetchBand(HWND hDlg, int dlgItemId);
void LoadPhysicsCombos(HWND hDlg);
void EnablePhysics(HWND hDlg);
void DisablePhysics(HWND hDlg);
