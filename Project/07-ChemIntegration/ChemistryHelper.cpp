#pragma once 
#include <Windows.h>
#include "ChemistryDialogBox.h"
#include "ChemistryHelper.h"

void ChemistryHelper::LoadChemistryCombo(HWND hDlg)
{
	HWND cbo;
	cbo = GetDlgItem(hDlg, ID_CHEM_CB);
	SendMessage(cbo, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCTSTR)"Initial Pressure(Pi)");
	SendMessage(cbo, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCTSTR)"Initial Temperature(Ti)");
	SendMessage(cbo, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCTSTR)"Final Pressure(Pf)");
	SendMessage(cbo, CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCTSTR)"Final Temperature(Tf)");
	SendMessage(cbo, CB_SETCURSEL, (WPARAM)0, (LPARAM)0);
}

ChemistryHelper::ChemistryHelper()
{
	this->initPress = -1.0;
	this->initTemp = -1.0;
	this->finalPress = -1.0;
	this->finalTemp = -1.0;
	this->currentSelection = ChemOps::INIT_PRESS;
}

void ChemistryHelper::SetVisibility(HWND hDlg, int controlToMakeReadOnly)
{
	switch (controlToMakeReadOnly)
	{
		// 0 = Pi 
	case INIT_PRESS:
		SendDlgItemMessage(hDlg, ID_CHEM_INITIALPRESSURE, EM_SETREADONLY, TRUE, 0);
		SendDlgItemMessage(hDlg, ID_CHEM_INITIALTEMPERATURE, EM_SETREADONLY, FALSE, 0);
		SendDlgItemMessage(hDlg, ID_CHEM_FINALPRESSURE, EM_SETREADONLY, FALSE, 0);
		SendDlgItemMessage(hDlg, ID_CHEM_FINALTEMPERATURE, EM_SETREADONLY, FALSE, 0);
		break;

		// 1 = Ti 
	case INIT_TEMP:
		SendDlgItemMessage(hDlg, ID_CHEM_INITIALPRESSURE, EM_SETREADONLY, FALSE, 0);
		SendDlgItemMessage(hDlg, ID_CHEM_INITIALTEMPERATURE, EM_SETREADONLY, TRUE, 0);
		SendDlgItemMessage(hDlg, ID_CHEM_FINALPRESSURE, EM_SETREADONLY, FALSE, 0);
		SendDlgItemMessage(hDlg, ID_CHEM_FINALTEMPERATURE, EM_SETREADONLY, FALSE, 0);
		break;

		// 2 = Pf 
	case FINAL_PRESS:
		SendDlgItemMessage(hDlg, ID_CHEM_INITIALPRESSURE, EM_SETREADONLY, FALSE, 0);
		SendDlgItemMessage(hDlg, ID_CHEM_INITIALTEMPERATURE, EM_SETREADONLY, FALSE, 0);
		SendDlgItemMessage(hDlg, ID_CHEM_FINALPRESSURE, EM_SETREADONLY, TRUE, 0);
		SendDlgItemMessage(hDlg, ID_CHEM_FINALTEMPERATURE, EM_SETREADONLY, FALSE, 0);
		break;

		// 3 = Tf
	case FINAL_TEMP:
		SendDlgItemMessage(hDlg, ID_CHEM_INITIALPRESSURE, EM_SETREADONLY, FALSE, 0);
		SendDlgItemMessage(hDlg, ID_CHEM_INITIALTEMPERATURE, EM_SETREADONLY, FALSE, 0);
		SendDlgItemMessage(hDlg, ID_CHEM_FINALPRESSURE, EM_SETREADONLY, FALSE, 0);
		SendDlgItemMessage(hDlg, ID_CHEM_FINALTEMPERATURE, EM_SETREADONLY, TRUE, 0);
		break;
	}
}

void ChemistryHelper::GetAllValues(HWND hDlg, double *initPress, double *initTemp, double *finalPress, double *finalTemp)
{
	char theText[20];

	GetDlgItemText(hDlg, ID_CHEM_INITIALPRESSURE, theText, 20);
	*initPress = atof(theText);

	GetDlgItemText(hDlg, ID_CHEM_INITIALTEMPERATURE, theText, 20);
	*initTemp = atof(theText);

	GetDlgItemText(hDlg, ID_CHEM_FINALPRESSURE, theText, 20);
	*finalPress = atof(theText);

	GetDlgItemText(hDlg, ID_CHEM_FINALTEMPERATURE, theText, 20);
	*finalTemp = atof(theText);
}

void ChemistryHelper::ResetValues()
{
	this->initPress = -1.0;
	this->initTemp = -1.0;
	this->finalPress = -1.0;
	this->finalTemp = -1.0;
	this->currentSelection = ChemOps::INIT_PRESS;
}

void ChemistryHelper::EnableChemistry(HWND hDlg)
{
	EnableWindow(GetDlgItem(hDlg, ID_CHEM_CB), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_CHEM_FINALPRESSURE), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_CHEM_FINALTEMPERATURE), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_CHEM_INITIALPRESSURE), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_CHEM_INITIALTEMPERATURE), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_CHEM_SUBMIT), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_CALCULATE_CHEMISTRY), TRUE);
}

void ChemistryHelper::DisableChemistry(HWND hDlg)
{
	EnableWindow(GetDlgItem(hDlg, ID_CHEM_CB), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_CHEM_FINALPRESSURE), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_CHEM_FINALTEMPERATURE), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_CHEM_INITIALPRESSURE), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_CHEM_INITIALTEMPERATURE), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_CHEM_SUBMIT), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_CALCULATE_CHEMISTRY), FALSE);
}