#pragma once
#include "MathsHelper.h"
#include "MathsDialogBox.h"

MathsHelper::MathsHelper()
{

}

void MathsHelper::EnableMaths(HWND hDlg)
{
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R1C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R1C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R1C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R1C4), TRUE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R2C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R2C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R2C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R2C4), TRUE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R3C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R3C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R3C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R3C4), TRUE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R4C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R4C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R4C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R4C4), TRUE);


	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R1C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R1C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R1C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R1C4), TRUE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R2C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R2C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R2C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R2C4), TRUE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R3C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R3C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R3C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R3C4), TRUE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R4C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R4C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R4C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R4C4), TRUE);


	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R1C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R1C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R1C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R1C4), TRUE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R2C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R2C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R2C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R2C4), TRUE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R3C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R3C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R3C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R3C4), TRUE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R4C1), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R4C2), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R4C3), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R4C4), TRUE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_CB_OPERATION), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_CALCULATE_MATHS), TRUE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_SUBMIT), TRUE);
}

void MathsHelper::DisableMaths(HWND hDlg)
{
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R1C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R1C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R1C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R1C4), FALSE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R2C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R2C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R2C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R2C4), FALSE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R3C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R3C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R3C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R3C4), FALSE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R4C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R4C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R4C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M1_R4C4), FALSE);


	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R1C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R1C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R1C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R1C4), FALSE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R2C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R2C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R2C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R2C4), FALSE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R3C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R3C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R3C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R3C4), FALSE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R4C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R4C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R4C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M2_R4C4), FALSE);


	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R1C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R1C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R1C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R1C4), FALSE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R2C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R2C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R2C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R2C4), FALSE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R3C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R3C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R3C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R3C4), FALSE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R4C1), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R4C2), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R4C3), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_M3_R4C4), FALSE);

	EnableWindow(GetDlgItem(hDlg, ID_MATHS_CB_OPERATION), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_CALCULATE_MATHS), FALSE);
	EnableWindow(GetDlgItem(hDlg, ID_MATHS_SUBMIT), FALSE);
}