#pragma once 
#include <Windows.h>

BOOL CALLBACK PCMBDlgProc(HWND, UINT, WPARAM, LPARAM);
void ShowPCMBDialog(HINSTANCE hInst, HWND hwnd, char* theMessage, RECT rc);
int GetComboSelectedIndex(HWND hDlg, int dlgItemId);
