#pragma once 
#include <Windows.h>

enum ChemOps
{
	INIT_PRESS = 0,
	INIT_TEMP,
	FINAL_PRESS,
	FINAL_TEMP
};

class ChemistryHelper
{
public:
	ChemistryHelper();
	void LoadChemistryCombo(HWND hDlg);
	void SetVisibility(HWND hDlg, int controlToDisable);
	void GetAllValues(HWND hDlg, double *initPress, double *initTemp, double *finalPress, double *finalTemp);
	double initPress;
	double initTemp;
	double finalPress;
	double finalTemp;
	ChemOps currentSelection;
	void ResetValues(); 
	void EnableChemistry(HWND hDlg);
	void DisableChemistry(HWND hDlg);
};

static const char *CHEM_LIB_NAME = "ChemGayLussacCFWrapper.dll";
static const char *CHEM_FN_INIT = "InitializeChemistryWrapper";
static const char *CHEM_FN_UNINIT = "UninitializeChemistryWrapper";
static const char *CHEM_FN_INIT_PRESS = "GetInitPressure";
static const char *CHEM_FN_INIT_TEMP = "GetInitTemperature";
static const char *CHEM_FN_FINAL_PRESS = "GetFinalPressure";
static const char *CHEM_FN_FINAL_TEMP = "GetFinalTemperature";

typedef double(*pfnChemFunc)(double, double, double);
typedef bool(*pfnInitFunc)();
typedef void(*pfnUninitFunc)();
static pfnChemFunc chemFunc = NULL;
static pfnInitFunc initFunc = NULL;
static pfnUninitFunc uninitFunc = NULL;
