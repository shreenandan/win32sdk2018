/*
Project part 7] Implement Chemistry Integration into PCMB project.
Win32 UI EXE Client -> classic DLL Wrapper -> COM DLL Server ClassFactory
Things already in place: UI, Physics, explicit linking: LoadLibrary(), FreeLibrary(), function pointer.

NOTES: 1] Splash screen will be visible throughout the program.
2] Spalsh screen can not be maximized, resized. If needed to have, can be achieved. Did for learning diff window options.
3] UI Design is improved with validations, backcolor.
4] Has wide char support. Results are shown with UNICODE characters.
5] Main window is always shown at center of the screen.
6] Code is refactored and structured into seperate helpers.
7] Now DLL lib loaded only during calculation and not before.
8] Refactoring helped taught new things. Please add /FORCE:MULTIPLE linker option before building project.
*/

// headers
#pragma once 
#include <windows.h>
#include "SplashBitmap.h"
#include <stdio.h>
#include "PhysicsDialogBox.h"
#include "PhysicsDefDLLServer.h"
#include "PCMBRadioButton.h"
#include "ChemistryDialogBox.h"
#include "MathsDialogBox.h"
#include "ChemistryHelper.h"
#include "ChemGayLussacCFWrapper.h"
#include "CommonPCMB.h"
#include "PhysicsHelper.h"
#include "MathsHelper.h"

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// Globally variables
HINSTANCE theInstance = NULL; // NOTE: Required when Loading Bitmap in WM_CREATE
BandColors b1 = Red, b2 = Green, b3 = Yellow, b4 = Blue;

bool IsPhysicsSubmitted = false;
bool IsChemistrySubmitted = false;
bool IsMathsSubmitted = false;
bool IsBiologySubmitted = false;

bool IsPhysicsComboLoaded = false;
bool IsChemistryComboLoaded = false;
bool IsMathsComboLoaded = false;

ChemistryHelper *chemistryHelper = new ChemistryHelper;
MathsHelper *mathsHelper = new MathsHelper;

// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("PCMB Integration");

	// code
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc; // NOTE
	wndclass.hInstance = hInstance;
	theInstance = hInstance; // Save instance
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// create window at center of the screen
	int xPos = (GetSystemMetrics(SM_CXSCREEN) - 1356) / 2;
	int yPos = 0; // (GetSystemMetrics(SM_CYSCREEN) - 740) / 2; // CW_USEDEFAULT

	hwnd = CreateWindow(szAppName,
		TEXT("PCMB Integration"),
		WS_OVERLAPPEDWINDOW ^ WS_THICKFRAME ^ WS_MAXIMIZEBOX,
		xPos,
		yPos,
		1356,
		739,
		NULL,
		NULL,
		hInstance,
		NULL);

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	// message loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	PAINTSTRUCT ps;
	static RECT rc;
	static HBITMAP hBitmap = NULL; // NOTE: Needs to be maintained across calls
	HDC hdcBitmap = NULL;
	HGDIOBJ rt_gdi1;
	int rt_getObj;
	BOOL rt_copyDc;
	BOOL rt_Del;
	BITMAP bmp;
	static bool IsInitiated = false;
	static char theMessage[350] = "";
	static HINSTANCE hInst = NULL;

	// code
	switch (iMsg)
	{
	case WM_CREATE:
		hBitmap = LoadBitmap(theInstance, MAKEINTRESOURCE(PCMB_SPLASH_BITMAP));
		if (hBitmap == NULL)
		{
			MessageBox(hwnd, TEXT("Error loading bitmap!!"), "NULL", MB_OK | MB_ICONERROR);
			DestroyWindow(hwnd);
		}
		wsprintf(theMessage, "Please press space bar to proceed.");
		break;

	case WM_PAINT:
		// Keep splash screen loaded all the time
		HFONT hFont;
		hdc = BeginPaint(hwnd, &ps);
		hdcBitmap = CreateCompatibleDC(hdc);
		rt_gdi1 = SelectObject(hdcBitmap, hBitmap); //hgdiobj
		rt_getObj = GetObject(hBitmap, sizeof(BITMAP), (LPVOID*)&bmp);
		rt_copyDc = BitBlt(hdc, 0, 0, bmp.bmWidth, bmp.bmHeight, hdcBitmap, 0, 0, SRCCOPY);
		rt_gdi1 = SelectObject(hdc, hBitmap);
		rt_Del = DeleteDC(hdcBitmap); // NOTE: This will retain the image as "hdc" is maintained by deleting hdcBitmap

		GetClientRect(hwnd, &rc);
		SetTextColor(hdc, RGB(255, 0, 0));
		SetBkColor(hdc, RGB(255, 255, 255)); // RGB(60, 24, 114)
		hFont = CreateFont(28, 14, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_OUTLINE_PRECIS,
			CLIP_DEFAULT_PRECIS, CLEARTYPE_QUALITY, VARIABLE_PITCH, TEXT("Times New Roman"));
		SelectObject(hdc, hFont);
		DrawText(hdc, theMessage, -1, &rc, DT_BOTTOM | DT_SINGLELINE | DT_CENTER);
		DeleteObject(hFont);

		rt_Del = DeleteDC(hdc);
		EndPaint(hwnd, &ps);
		break;

	case WM_DESTROY:
		rt_Del = DeleteObject(hBitmap);
		PostQuitMessage(0);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_SPACE:
			wsprintf(theMessage, "Press space bar to display dialog box.");
			IsPhysicsSubmitted = false;
			IsChemistrySubmitted = false;
			IsMathsSubmitted = false;
			IsBiologySubmitted = false;
			IsPhysicsComboLoaded = IsChemistryComboLoaded = false;
			InvalidateRect(hwnd, &rc, TRUE);
			ShowPCMBDialog(hInst, hwnd, theMessage, rc);
			break;
		}
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}


BOOL CALLBACK PCMBDlgProc(HWND hDlg, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	static RECT rc;
	wchar_t temp[50];
	ResistanceToleranceResult theResult;
	static HWND hwndTemp;
	static HBRUSH hDlgBrush = CreateSolidBrush(RGB(127, 127, 127));
	long isSelected = 0;
	static int choice = 0;
	bool successful = false;

	static HMODULE hLib = NULL;
	typedef ResistanceToleranceResult(*pfnCalcFourBandResistanceTolerance)(BandColors, BandColors, BandColors, BandColors);
	static pfnCalcFourBandResistanceTolerance calcFourBandResistanceTolerance = NULL;

	switch (iMsg)
	{
	case WM_INITDIALOG:
		return (TRUE);

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
			// Now DLL lib loaded only during calculation and not before
		case ID_CALCULATE_PHYSICS:
			isSelected = SendDlgItemMessage(hDlg, ID_ARB_PHYSICS, BM_GETCHECK, 0, 0);
			if (isSelected != 1) // Do only if current radio selected
			{
				MessageBox(hDlg, TEXT("Please select radio option."), TEXT("No Selection!"), MB_ICONEXCLAMATION | MB_OK);
				return TRUE;
			}
			if (IsPhysicsSubmitted) // Do only if not submitted
			{
				MessageBox(hDlg, TEXT("Can not re-submit, already submitted."), TEXT("Submitted!"), MB_ICONEXCLAMATION | MB_OK);
				return TRUE;
			}
			b1 = FetchBand(hDlg, ID_PHYSICS_CBBAND1);
			b2 = FetchBand(hDlg, ID_PHYSICS_CBBAND2);
			b3 = FetchBand(hDlg, ID_PHYSICS_CBBAND3);
			b4 = FetchBand(hDlg, ID_PHYSICS_CBBAND4);

			hLib = LoadLibrary(TEXT("PhysicsDefDLLServer.dll")); // Step 1 Load DLL
			if (hLib == NULL)
			{
				MessageBox(hDlg, TEXT("PhysicsDefDLLServer.dll not found!!"), "Error", MB_OK | MB_ICONERROR);
				DestroyWindow(hDlg);
			}
			calcFourBandResistanceTolerance = (pfnCalcFourBandResistanceTolerance)GetProcAddress(hLib, "CalcFourBandResistanceTolerance"); // Step 2
			theResult = calcFourBandResistanceTolerance(b1, b2, b3, b4); // Step 3 Explicitly linked custom DLL function call
			FreeLibrary(hLib); // Step 4

			if (theResult.IsToleranceSuccessful)
			{
				SetDlgItemTextW(hDlg, ID_PHYSICS_TOLERANCE, theResult.Tolerance);
			}
			if (theResult.IsResistanceSuccessful)
			{
				hwndTemp = GetDlgItem(hDlg, ID_PHYSICS_RESISTANCE);
				swprintf(temp, 40, L"%.2lf", theResult.Resistance); // NOTE: Important This is how float/double values can be displayed. 2 digits after decimal point
				SetWindowTextW(hwndTemp, temp);
			}
			return TRUE;
			break;

		case ID_CHEM_CB:
			switch (HIWORD(wParam))
			{
			case CBN_SELCHANGE:
				choice = GetComboSelectedIndex(hDlg, ID_CHEM_CB);
				chemistryHelper->currentSelection = (ChemOps)choice;
				chemistryHelper->SetVisibility(hDlg, choice);
				break;
			}
			// Reference: https://docs.microsoft.com/en-us/windows/desktop/controls/cbn-selchange
			return TRUE;

		case ID_CALCULATE_CHEMISTRY:
			try
			{
				isSelected = SendDlgItemMessage(hDlg, ID_ARB_CHEMISTRY, BM_GETCHECK, 0, 0);
				if (isSelected != 1) // Do only if current radio selected
				{
					MessageBox(hDlg, TEXT("Please select radio option."), TEXT("No Selection!"), MB_ICONEXCLAMATION | MB_OK);
					return TRUE;
				}
				if (IsChemistrySubmitted) // Do only if not submitted
				{
					MessageBox(hDlg, TEXT("Can not re-submit, already submitted."), TEXT("Submitted!"), MB_ICONEXCLAMATION | MB_OK);
					return TRUE;
				}

				chemistryHelper->GetAllValues(hDlg, &chemistryHelper->initPress, &chemistryHelper->initTemp, &chemistryHelper->finalPress, &chemistryHelper->finalTemp);

				hLib = LoadLibrary(TEXT(CHEM_LIB_NAME)); // Step 1 Load DLL
				if (hLib == NULL)
				{
					MessageBox(hDlg, TEXT("ChemGayLussacCFWrapper.dll not found!!"), TEXT("Error"), MB_OK | MB_ICONERROR);
					return TRUE;
				}

				initFunc = (pfnInitFunc)GetProcAddress(hLib, CHEM_FN_INIT);
				successful = initFunc();
				if (!successful)
				{
					MessageBox(hDlg, TEXT("IChemGayLussac_Temperature interface can not be obtained."), TEXT("Error"), MB_OK);
					FreeLibrary(hLib); // Step 4
					return TRUE;
				}

				switch (chemistryHelper->currentSelection)
				{
				case INIT_PRESS:
					chemFunc = (pfnChemFunc)GetProcAddress(hLib, CHEM_FN_INIT_PRESS); // Step 2
					chemistryHelper->initPress = chemFunc(chemistryHelper->initTemp, chemistryHelper->finalPress, chemistryHelper->finalTemp); // Step 3
					if (chemistryHelper->initPress <= -1.0)
					{
						MessageBox(hDlg, TEXT("Either IChemGayLussac_Pressure interface can not be obtained or Something went wrong when calculating Initial Pressure."), TEXT("Error"), MB_OK);
					}
					swprintf(temp, 40, L"%.4lf", chemistryHelper->initPress);
					SetDlgItemTextW(hDlg, ID_CHEM_INITIALPRESSURE, temp);
					break;

				case INIT_TEMP:
					chemFunc = (pfnChemFunc)GetProcAddress(hLib, CHEM_FN_INIT_TEMP); // Step 2
					chemistryHelper->initTemp = chemFunc(chemistryHelper->initPress, chemistryHelper->finalPress, chemistryHelper->finalTemp); // Step 3
					if (chemistryHelper->initTemp <= -1.0)
					{
						MessageBox(hDlg, TEXT("Something went wrong when calculating Initial Temperature."), TEXT("Error"), MB_OK);
					}
					swprintf(temp, 40, L"%.4lf", chemistryHelper->initTemp);
					SetDlgItemTextW(hDlg, ID_CHEM_INITIALTEMPERATURE, temp);
					break;

				case FINAL_PRESS:
					chemFunc = (pfnChemFunc)GetProcAddress(hLib, CHEM_FN_FINAL_PRESS); // Step 2
					chemistryHelper->finalPress = chemFunc(chemistryHelper->initPress, chemistryHelper->initTemp, chemistryHelper->finalTemp); // Step 3
					if (chemistryHelper->finalPress <= -1.0)
					{
						MessageBox(hDlg, TEXT("Either IChemGayLussac_Pressure interface can not be obtained or Something went wrong when calculating Final Pressure."), TEXT("Error"), MB_OK);
					}
					swprintf(temp, 40, L"%.4lf", chemistryHelper->finalPress);
					SetDlgItemTextW(hDlg, ID_CHEM_FINALPRESSURE, temp);
					break;

				case FINAL_TEMP:
					chemFunc = (pfnChemFunc)GetProcAddress(hLib, CHEM_FN_FINAL_TEMP); // Step 2
					chemistryHelper->finalTemp = chemFunc(chemistryHelper->initPress, chemistryHelper->initTemp, chemistryHelper->finalPress); // Step 3
					if (chemistryHelper->finalTemp <= -1.0)
					{
						MessageBox(hDlg, TEXT("Something went wrong when calculating Final Temperature."), TEXT("Error"), MB_OK);
					}
					swprintf(temp, 40, L"%.4lf", chemistryHelper->finalTemp); 
					SetDlgItemTextW(hDlg, ID_CHEM_FINALTEMPERATURE, temp);
					break;
				}

				uninitFunc = (pfnUninitFunc)GetProcAddress(hLib, CHEM_FN_UNINIT);
				uninitFunc();
				FreeLibrary(hLib); // step 4
			}
			catch (...)
			{
				if (hLib)
				{
					FreeLibrary(hLib);
				}
			}
			return TRUE;
			break;

		case IDCANCEL:
			chemistryHelper->ResetValues(); // Useful on next load
			EndDialog(hDlg, 0);
			break;

		case ID_ARB_PHYSICS:
			if (IsPhysicsSubmitted) // Do only if not submitted
			{
				MessageBox(hDlg, TEXT("Already submitted."), TEXT("Submitted!"), MB_ICONEXCLAMATION | MB_OK);
				return TRUE;
			}
			if (!IsPhysicsComboLoaded)
			{
				LoadPhysicsCombos(hDlg);
			}
			IsPhysicsComboLoaded = true;
			EnablePhysics(hDlg);
		 	chemistryHelper->DisableChemistry(hDlg);
			mathsHelper->DisableMaths(hDlg);
			break;

		case ID_ARB_CHEMISTRY:
			if (IsChemistrySubmitted)
			{
				MessageBox(hDlg, TEXT("Already submitted."), TEXT("Submitted!"), MB_ICONEXCLAMATION | MB_OK);
				return TRUE;
			}
			if (!IsChemistryComboLoaded)
			{
				chemistryHelper->LoadChemistryCombo(hDlg);
				choice = 0;
				chemistryHelper->currentSelection = (ChemOps)choice;
				chemistryHelper->SetVisibility(hDlg, choice);
			}
			IsChemistryComboLoaded = true;
			chemistryHelper->EnableChemistry(hDlg);
			DisablePhysics(hDlg);
			mathsHelper->DisableMaths(hDlg);
			break;

		case ID_ARB_MATHEMATICS:
			if (IsMathsSubmitted) // Do only if not submitted
			{
				MessageBox(hDlg, TEXT("Already submitted."), TEXT("Submitted!"), MB_ICONEXCLAMATION | MB_OK);
				return TRUE;
			}
			mathsHelper->EnableMaths(hDlg);
			DisablePhysics(hDlg);
			chemistryHelper->DisableChemistry(hDlg);
			return TRUE;
			break;

		case ID_ARB_BIOLOGY:
			if (IsBiologySubmitted)
			{
				MessageBox(hDlg, TEXT("Already submitted."), TEXT("Submitted!"), MB_ICONEXCLAMATION | MB_OK);
				return TRUE;
			}
			DisablePhysics(hDlg);
			chemistryHelper->DisableChemistry(hDlg);
			mathsHelper->DisableMaths(hDlg);
			return TRUE;
			break;

		case ID_PHYSICS_SUBMIT:
			isSelected = SendDlgItemMessage(hDlg, ID_ARB_PHYSICS, BM_GETCHECK, 0, 0);
			if (isSelected != 1) // Do only if current radio selected
			{
				MessageBox(hDlg, TEXT("Please select radio option."), TEXT("No Selection!"), MB_ICONEXCLAMATION | MB_OK);
				return TRUE;
			}
			if (IsPhysicsSubmitted) // Do only if not submitted
			{
				MessageBox(hDlg, TEXT("Can not re-submit, already submitted."), TEXT("Submitted!"), MB_ICONEXCLAMATION | MB_OK);
				return TRUE;
			}
			IsPhysicsSubmitted = true;
			DisablePhysics(hDlg);
			return TRUE;
			break;

		case ID_CHEM_SUBMIT:
			isSelected = SendDlgItemMessage(hDlg, ID_ARB_CHEMISTRY, BM_GETCHECK, 0, 0);
			if (isSelected != 1) // Do only if current radio selected
			{
				MessageBox(hDlg, TEXT("Please select radio option."), TEXT("No Selection!"), MB_ICONEXCLAMATION | MB_OK);
				return TRUE;
			}
			if (IsChemistrySubmitted) // Do only if not submitted
			{
				MessageBox(hDlg, TEXT("Can not re-submit, already submitted."), TEXT("Submitted!"), MB_ICONEXCLAMATION | MB_OK);
				return TRUE;
			}
			IsChemistrySubmitted = true;
			chemistryHelper->DisableChemistry(hDlg);
			return TRUE;
			break;


		case ID_MATHS_SUBMIT:
			isSelected = SendDlgItemMessage(hDlg, ID_ARB_MATHEMATICS, BM_GETCHECK, 0, 0);
			if (isSelected != 1) // Do only if current radio selected
			{
				MessageBox(hDlg, TEXT("Please select radio option."), TEXT("No Selection!"), MB_ICONEXCLAMATION | MB_OK);
				return TRUE;
			}
			if (IsMathsSubmitted) // Do only if not submitted
			{
				MessageBox(hDlg, TEXT("Can not re-submit, already submitted."), TEXT("Submitted!"), MB_ICONEXCLAMATION | MB_OK);
				return TRUE;
			}
			IsMathsSubmitted = true;
			mathsHelper->DisableMaths(hDlg);
			return TRUE;
			break;

		case ID_CALCULATE_MATHS:
			isSelected = SendDlgItemMessage(hDlg, ID_ARB_MATHEMATICS, BM_GETCHECK, 0, 0);
			if (isSelected != 1) // Do only if current radio selected
			{
				MessageBox(hDlg, TEXT("Please select radio option."), TEXT("No Selection!"), MB_ICONEXCLAMATION | MB_OK);
				return TRUE;
			}
			if (IsMathsSubmitted) // Do only if not submitted
			{
				MessageBox(hDlg, TEXT("Can not re-submit, already submitted."), TEXT("Submitted!"), MB_ICONEXCLAMATION | MB_OK);
				return TRUE;
			}
			return TRUE;
			break;
		}

	case WM_CTLCOLORDLG:
		//hDlgBrush = CreateSolidBrush(RGB(127, 127, 127));
		return ((BOOL)hDlgBrush);
		break;

	case WM_CTLCOLORSTATIC:
		HDC hdcStatic = (HDC)wParam;
		// Way 1]
		// Reference: http://winprog.org/tutorial/dlgfaq.html
		//SetTextColor(hdcStatic, RGB(255, 0, 0)); // NOTE: Learned new thing to set text color for control defined in RC file
		//SetBkMode(hdcStatic, TRANSPARENT);

		// Way 2]
		// Reference: https://docs.microsoft.com/en-us/windows/desktop/controls/wm-ctlcolorstatic
		SetBkColor(hdcStatic, RGB(127, 127, 127));
		return ((INT_PTR)hDlgBrush);

		// Make EDITTEXT read only. NOTE: Disabled is different than this
		// https://docs.microsoft.com/en-us/windows/desktop/Controls/edit-control-styles
		break;

		return (TRUE);
	}

	return (FALSE);
}
