#pragma once
#include <Windows.h>

class MathsHelper
{
public:
	MathsHelper();
	void EnableMaths(HWND hDlg);
	void DisableMaths(HWND hDlg);
};