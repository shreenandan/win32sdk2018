#pragma once

class IMyMath : public IDispatch
{
public:
	virtual HRESULT __stdcall Sum(int, int, int *) = 0; // pure virtual
	virtual HRESULT __stdcall Sub(int, int, int *) = 0; // pure virtual
};

// CLSID of CoClass CSumSub // {A69B200B-62AB-4BAC-A205-4311D4A6D4FA}
const CLSID CLSID_MyMath = { 0xa69b200b, 0x62ab, 0x4bac,{ 0xa2, 0x5, 0x43, 0x11, 0xd4, 0xa6, 0xd4, 0xfa } };

// interface // {AC175B43-7F24-492B-9A8F-7F4923E6FC84}
const IID IID_IMyMath = { 0xac175b43, 0x7f24, 0x492b,{ 0x9a, 0x8f, 0x7f, 0x49, 0x23, 0xe6, 0xfc, 0x84 } };
