#include <Windows.h>
#include <stdio.h> // change 1] Later used for getting HR Failure Reason
#include "AutomationServer.h"

// class declarations
class CMyMath : public IMyMath // change 2] Only 1 interface inherited(and not more like class factory)
{
private:
	long m_cRef;
	ITypeInfo *m_pITypeInfo = NULL; // change 3]
public:
	CMyMath(void);
	~CMyMath(void);

	// IUnknown specific method declarations (inherited)
	HRESULT __stdcall QueryInterface(REFIID, void **);
	ULONG __stdcall AddRef(void);
	ULONG __stdcall Release(void);

	// change 4] A new methods
	// IDispatch specific method declarations (inherited)
	HRESULT __stdcall GetTypeInfoCount(UINT *);
	HRESULT __stdcall GetTypeInfo(UINT, LCID, ITypeInfo **);
	HRESULT __stdcall GetIDsOfNames(REFIID, LPOLESTR *, UINT, LCID, DISPID *);
	HRESULT __stdcall Invoke(DISPID, REFIID, LCID, WORD, DISPPARAMS *, VARIANT *, EXCEPINFO *, UINT *);

	// IMyMath specific methods
	HRESULT __stdcall Sum(int, int, int *);
	HRESULT __stdcall Sub(int, int, int *);

	// change 4] B custom methods
	HRESULT InitInstance(void); // like InitializeInnerComponent
};

class CMyMathClassFactory : public IClassFactory
{
private:
	long m_cRef;
public:
	CMyMathClassFactory(void);
	~CMyMathClassFactory(void);

	// IUnknown specific method declarations (inherited)
	HRESULT __stdcall QueryInterface(REFIID, void **);
	ULONG __stdcall AddRef(void);
	ULONG __stdcall Release(void);

	// IUnknown specific method declarations (inherited)
	HRESULT __stdcall CreateInstance(IUnknown *, REFIID, void **);
	HRESULT __stdcall LockServer(BOOL);
};

// global DLL handle
HMODULE ghModule = NULL; // new variable

// global variable declarations
long glNoOfActiveComponents = 0;
long glNoOfServerLocks = 0; // no of locks on this dll

// change 5] GUID for TypeLibrary // {B58FAAE0-C8DA-4585-86F2-A608DDD18CCF}
const GUID LIBID_AutomationServer = { 0xb58faae0, 0xc8da, 0x4585,{ 0x86, 0xf2, 0xa6, 0x8, 0xdd, 0xd1, 0x8c, 0xcf } };

BOOL WINAPI DllMain(HINSTANCE hDll, DWORD dwReason, LPVOID reserved)
{
	// code
	switch (dwReason)
	{
	case DLL_PROCESS_ATTACH:
		ghModule = hDll; // new holding reference to hmodule
		break;

	case DLL_PROCESS_DETACH:
		break;

	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
		break;
	}

	return (TRUE);
}

// CMyMath Implementation
CMyMath::CMyMath(void)
{
	m_pITypeInfo = NULL; // change 5] A
	m_cRef = 1; // hardcoded initialization to anticipate possible failure of QueryInterface()
	InterlockedIncrement(&glNoOfActiveComponents);
}

CMyMath::~CMyMath(void)
{
	if (m_pITypeInfo != NULL)
	{
		m_pITypeInfo->Release();
	}
	InterlockedDecrement(&glNoOfActiveComponents);
}

// Implementation of IUnknown's 3 methods
HRESULT CMyMath::QueryInterface(REFIID riid, void **ppv) // ptr to ptr to void
{
	// code
	if (riid == IID_IUnknown) // Order is important, Adjustor Thunk
		*ppv = static_cast<IMyMath *>(this);
	else if (riid == IID_IDispatch) // change 6] 
		*ppv = static_cast<IMyMath *>(this);
	else if (riid == IID_IMyMath)
		*ppv = static_cast<IMyMath *>(this);
	else
	{
		*ppv = NULL;
		return (E_NOINTERFACE);
	}

	reinterpret_cast<IUnknown *>(*ppv)->AddRef();
	return (S_OK);
}

ULONG CMyMath::AddRef(void)
{
	InterlockedIncrement(&m_cRef);
	return (m_cRef);
}

ULONG CMyMath::Release(void)
{
	InterlockedDecrement(&m_cRef);

	// logic
	if (m_cRef == 0)
	{
		m_pITypeInfo->Release(); // change 7] 
		m_pITypeInfo = NULL; // change 7] 
		delete(this);
		return (0);
	}

	return (m_cRef);
}

// Implementation IMyMath's methods
HRESULT CMyMath::Sum(int num1, int num2, int *pSum)
{
	*pSum = num1 + num2;
	return (S_OK);
}

HRESULT CMyMath::Sub(int num1, int num2, int *pSum)
{
	*pSum = num1 - num2;
	return (S_OK);
}

// custom method implementation
// Aim of this method is to get m_pITypeInfo
HRESULT CMyMath::InitInstance(void)
{
	// function declarations
	void ComErrorDescriptionString(HWND, HRESULT);

	// variable decalrations
	HRESULT hr;
	ITypeLib *pITypeLib = NULL;

	// code 
	if (m_pITypeInfo == NULL)
	{
		// change 8] SDK function
		hr = LoadRegTypeLib(LIBID_AutomationServer,
			1, 0, // major/minor version numbers
			0x00, //LANG_NEUTRAL
			&pITypeLib);

		if (FAILED(hr))
		{
			ComErrorDescriptionString(NULL, hr);
			return(hr);
		}

		hr = pITypeLib->GetTypeInfoOfGuid(IID_IMyMath, &m_pITypeInfo);

		if (FAILED(hr))
		{
			ComErrorDescriptionString(NULL, hr);
			pITypeLib->Release();
			return(hr);
		}

		pITypeLib->Release();
	}

	return(S_OK);
}


// Class Factory Implementation
CMyMathClassFactory::CMyMathClassFactory(void)
{
	m_cRef = 1; // hardcoded initialization to anticipate possible failure of QueryInterface()
}

CMyMathClassFactory::~CMyMathClassFactory(void)
{

}

// Implementation of IUnknown's 3 methods
HRESULT CMyMathClassFactory::QueryInterface(REFIID riid, void **ppv) // ptr to ptr to void
{
	// code
	if (riid == IID_IUnknown) // Order is important, Adjustor Thunk
		*ppv = static_cast<IClassFactory *>(this);
	else if (riid == IID_IClassFactory)
		*ppv = static_cast<IClassFactory *>(this);
	else
	{
		*ppv = NULL;
		return (E_NOINTERFACE);
	}

	reinterpret_cast<IUnknown *>(*ppv)->AddRef();
	return (S_OK);
}

ULONG CMyMathClassFactory::AddRef(void)
{
	InterlockedIncrement(&m_cRef);
	return (m_cRef);
}

ULONG CMyMathClassFactory::Release(void)
{
	InterlockedDecrement(&m_cRef);

	// logic
	if (m_cRef == 0)
	{
		delete(this);
		return (0);
	}

	return (m_cRef);
}


// Implementation of IClassFacory's 2 methods
HRESULT CMyMathClassFactory::CreateInstance(IUnknown *pUnkOuter, REFIID riid, void **ppv)
{
	CMyMath *pCMyMath = NULL;
	HRESULT hr;

	if (pUnkOuter != NULL)
		return (CLASS_E_NOAGGREGATION);

	// create the instance of component
	pCMyMath = new CMyMath;
	if (pCMyMath == NULL)
	{
		return (E_OUTOFMEMORY);
	}

	// change 9] call automation related init method
	pCMyMath->InitInstance();

	// get the requested interface
	hr = pCMyMath->QueryInterface(riid, ppv);
	pCMyMath->Release(); // anticipate possible failure of QueryInterface()
	return (hr);
}

HRESULT CMyMathClassFactory::LockServer(BOOL fLock)
{
	if (fLock)
		InterlockedIncrement(&glNoOfServerLocks);
	else
		InterlockedDecrement(&glNoOfServerLocks);

	return (S_OK);
}


// Implementation of IDispatch methods
HRESULT CMyMath::GetTypeInfoCount(UINT *pCountTypeInfo)
{
	// as we have type library it is 1, else 0
	*pCountTypeInfo = 1; // Always one

	return(S_OK);
}

// change 10] 
HRESULT CMyMath::GetTypeInfo(UINT iTypeInfo, LCID lcid, ITypeInfo **ppITypeInfo)
{
	*ppITypeInfo = NULL;

	if (iTypeInfo != 0)
	{
		return(DISP_E_BADINDEX);
	}

	m_pITypeInfo->AddRef();
	*ppITypeInfo = m_pITypeInfo;
	return(S_OK);
}

// change 11] 
HRESULT CMyMath::GetIDsOfNames(REFIID riid, LPOLESTR *rgszNames, UINT cNames, LCID lcid, DISPID *rgDispId)
{
	return(DispGetIDsOfNames(m_pITypeInfo, rgszNames, cNames, rgDispId));
}

// change 12] 
HRESULT CMyMath::Invoke(DISPID dispIdMember, REFIID iid, LCID lcid, WORD wFlags,
	DISPPARAMS *pDispParams, VARIANT *pVarResult, EXCEPINFO *pExcepInfo, UINT *puArgErr)
{
	HRESULT hr;

	hr = DispInvoke(this,
		m_pITypeInfo,
		dispIdMember,
		wFlags,
		pDispParams,
		pVarResult,
		pExcepInfo,
		puArgErr);

	return(hr);
}

// Implementation of gloabl Exported functions from this DLL
// extern "C" required from 2000 onwards
extern "C" HRESULT __stdcall DllGetClassObject(REFCLSID rclsid, REFIID riid, void **ppv)
{
	CMyMathClassFactory *pCMyMathClassFactory = NULL;
	HRESULT hr;

	if (rclsid != CLSID_MyMath)
		return (CLASS_E_CLASSNOTAVAILABLE);

	// create class factory
	pCMyMathClassFactory = new CMyMathClassFactory;
	if (pCMyMathClassFactory == NULL)
		return (E_OUTOFMEMORY);

	hr = pCMyMathClassFactory->QueryInterface(riid, ppv);
	pCMyMathClassFactory->Release(); // anticipate possible failure of QueryInterface()

	return (hr);
}

extern "C" HRESULT __stdcall DllCanUnloadNow(void)
{
	if (glNoOfActiveComponents == 0 && glNoOfServerLocks == 0)
		return (S_OK);
	else
		return (S_FALSE);
}

void ComErrorDescriptionString(HWND hwnd, HRESULT hr)
{
	TCHAR *szErrorMessage = NULL;
	//TCHAR str[255];
	wchar_t str[255];
	size_t theSize = 252;

	if (FACILITY_WINDOWS == HRESULT_FACILITY(hr))
	{
		hr = HRESULT_CODE(hr);
	}
	// typecast
	if (FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, NULL,
		hr, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR)&szErrorMessage, 0, NULL) != 0)
	{
		//swprintf_s(str, TEXT("%#x : %s"), hr, szErrorMessage);
		swprintf_s(str, 252, L"%#x : %s", hr, szErrorMessage);
		LocalFree(szErrorMessage);
	}
	else
	{
		//swprintf_s(str, TEXT("[Could not find a description for error # %#x.]\n"), hr);
		swprintf_s(str, 252, L"[Could not find a description for error # %#x.]\n", hr);
	}

	//MessageBox(hwnd, str, TEXT("Com Error"), MB_OK);
	MessageBoxW(hwnd, str, L"Com Error", MB_OK);
}

// change 13] 14] 
// NOTE: We are neither going to do registration by function DllRegisterServer(),
// DllUnregisterServer nor by macro ;REGISTER_PROXY_DLL; Doing by .Reg registry file
