using System;
using System.Runtime.InteropServices;
using AutomationServerTypeLibForDotNet;

public class CSharpAutomation
{
	public static void Main()
	{
        Console.WriteLine("Hello from C# Automation Client");
        CMyMathClass objCMyMathClass = new CMyMathClass();
        Console.WriteLine("Got CoClass CMyMathClass");
        IMyMath objIMyMath = (IMyMath)objCMyMathClass;
        Console.WriteLine("Type Casted CoClass to IMyMath");
        int n1 = 500, n2 = 200, res;
        res = objIMyMath.Sum(n1, n2);
        Console.WriteLine("Sum of " + n1 + " and " + n2 + " = " + res);
        res = objIMyMath.Sub(n1, n2);
        Console.WriteLine("Sub of " + n1 + " and " + n2 + " = " + res);
        Console.ReadKey();
	}
}