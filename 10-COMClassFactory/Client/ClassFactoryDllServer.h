class ISum : public IUnknown
{
	public:
		virtual HRESULT __stdcall Sum(int, int, int *) = 0; // pure virtual
};

class ISub : public IUnknown
{
	public:
		virtual HRESULT __stdcall Sub(int, int, int *) = 0; // pure virtual
};

// CLSID of CoClass CSumSub // {F99A74C3-7634-4E4A-8F83-6D7C2FE31953}
const CLSID CLSID_CSumSub = { 0xf99a74c3, 0x7634, 0x4e4a, { 0x8f, 0x83, 0x6d, 0x7c, 0x2f, 0xe3, 0x19, 0x53 } };

// interface
const IID IID_ISum = { 0xb62c51e1, 0xa289, 0x4de4, { 0xbc, 0x70, 0xd0, 0x6a, 0x41, 0xaf, 0xcd, 0x23 } }; // {B62C51E1-A289-4DE4-BC70-D06A41AFCD23}
const IID IID_ISub = { 0xd650b2ea, 0x6fad, 0x4a27, { 0xaf, 0xe8, 0x6a, 0xe8, 0x4f, 0xe9, 0x88, 0x45 } }; // {D650B2EA-6FAD-4A27-AFE8-6AE84FE98845}