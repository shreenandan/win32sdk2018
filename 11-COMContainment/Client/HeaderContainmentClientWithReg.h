class ISum : public IUnknown
{
public:
	virtual HRESULT __stdcall SumTwoInt(int, int, int *) = 0; // pure virtual
};

class ISub : public IUnknown
{
public:
	virtual HRESULT __stdcall SubTwoInt(int, int, int *) = 0; // pure virtual
};

// CLSID of SumSub component {D7DEEB77-9101-4348-924C-4541BA4BD64F} CLSID of only outer shared with client
const CLSID CLSID_CSumSub = { 0xd7deeb77, 0x9101, 0x4348, { 0x92, 0x4c, 0x45, 0x41, 0xba, 0x4b, 0xd6, 0x4f } };

// IID of ISum Interface {2D8EBF3F-FD03-47F4-8F1D-734D36DDA25B}
const IID IID_ISum = { 0x2d8ebf3f, 0xfd03, 0x47f4, { 0x8f, 0x1d, 0x73, 0x4d, 0x36, 0xdd, 0xa2, 0x5b } };

// IID of ISub Interface {C8BE2D7D-2DBA-4657-A0DC-205DA9739EA2}
const IID IID_ISub = { 0xc8be2d7d, 0x2dba, 0x4657, { 0xa0, 0xdc, 0x20, 0x5d, 0xa9, 0x73, 0x9e, 0xa2 } };

class IMultiplication : public IUnknown
{
public:
	virtual HRESULT __stdcall MultiplyTwoInt(int, int, int *) = 0; // pure virtual
};

class IDivision : public IUnknown
{
public:
	virtual HRESULT __stdcall DivisionTwoInt(int, int, int *) = 0; // pure virtual
};

// NOTE: In Containment, don't give CLSID of inner MultiplicationDivision component to client

// IID of IMultiplication Interface {553ACA47-BF09-4B11-9DA2-5A1B2C185298}
const IID IID_IMultiplication = { 0x553aca47, 0xbf09, 0x4b11, { 0x9d, 0xa2, 0x5a, 0x1b, 0x2c, 0x18, 0x52, 0x98 } };

// IID of IDivision Interface {35E5CB73-04A9-4077-BA4F-9DE4D234281E}
const IID IID_IDivision = { 0x35e5cb73, 0x4a9, 0x4077, { 0xba, 0x4f, 0x9d, 0xe4, 0xd2, 0x34, 0x28, 0x1e } };
