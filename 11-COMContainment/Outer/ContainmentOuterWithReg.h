class ISum : public IUnknown
{
public:
	virtual HRESULT __stdcall SumTwoInt(int, int, int *) = 0; // pure virtual
};

class ISub : public IUnknown
{
public:
	virtual HRESULT __stdcall SubTwoInt(int, int, int *) = 0; // pure virtual
};

// CLSID of SumSub component {D7DEEB77-9101-4348-924C-4541BA4BD64F}
const CLSID CLSID_CSumSub = { 0xd7deeb77, 0x9101, 0x4348, { 0x92, 0x4c, 0x45, 0x41, 0xba, 0x4b, 0xd6, 0x4f } };

// IID of ISum Interface {2D8EBF3F-FD03-47F4-8F1D-734D36DDA25B}
const IID IID_ISum = { 0x2d8ebf3f, 0xfd03, 0x47f4, { 0x8f, 0x1d, 0x73, 0x4d, 0x36, 0xdd, 0xa2, 0x5b } };

// IID of ISub Interface {C8BE2D7D-2DBA-4657-A0DC-205DA9739EA2}
const IID IID_ISub = { 0xc8be2d7d, 0x2dba, 0x4657, { 0xa0, 0xdc, 0x20, 0x5d, 0xa9, 0x73, 0x9e, 0xa2 } };
