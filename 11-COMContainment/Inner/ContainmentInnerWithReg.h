class IMultiplication : public IUnknown
{
public:
	virtual HRESULT __stdcall MultiplyTwoInt(int, int, int *) = 0; // pure virtual
};

class IDivision : public IUnknown
{
public:
	virtual HRESULT __stdcall DivisionTwoInt(int, int, int *) = 0; // pure virtual
};

// NOTE: In Containment, don't give below CLSID of inner to client
// CLSID of MultiplicationDivision component {CE4E4B6B-97FC-4E80-8F85-20C072310B12}
const CLSID CLSID_CMultiplicationDivision = { 0xce4e4b6b, 0x97fc, 0x4e80, { 0x8f, 0x85, 0x20, 0xc0, 0x72, 0x31, 0xb, 0x12 } };

// IID of IMultiplication Interface {553ACA47-BF09-4B11-9DA2-5A1B2C185298}
const IID IID_IMultiplication = { 0x553aca47, 0xbf09, 0x4b11, { 0x9d, 0xa2, 0x5a, 0x1b, 0x2c, 0x18, 0x52, 0x98 } };

// IID of IDivision Interface {35E5CB73-04A9-4077-BA4F-9DE4D234281E}
const IID IID_IDivision = { 0x35e5cb73, 0x4a9, 0x4077, { 0xba, 0x4f, 0x9d, 0xe4, 0xd2, 0x34, 0x28, 0x1e } };