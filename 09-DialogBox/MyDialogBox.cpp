// headers
#include <windows.h>
#include "MyDialogBox.h"
#include <cstdio>

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
BOOL CALLBACK MyDlgProc(HWND, UINT, WPARAM, LPARAM);

// Globally decalred structure
struct MYINPUT
{
	char name[50], address[50];
	int age, mstatus;
	float sal;
} theInput;

// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("ShreeDialogBox");

	// code
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc; // NOTE
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindow(szAppName,
		TEXT("Shree Dialog Box"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	// message loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// code
	static HINSTANCE hInst = NULL;
	static HDC hdc = NULL;
	static PAINTSTRUCT ps;
	static RECT rc;
	static char str[350];
	static wchar_t strSal[256];

	switch (iMsg)
	{
	case WM_CREATE:
		wsprintf(str, "Please press space bar to make an entry.");
		break;

	case WM_PAINT:
		hdc = BeginPaint(hwnd, &ps);
		GetClientRect(hwnd, &rc);
		SetTextColor(hdc, RGB(255, 0, 0));
		SetBkColor(hdc, RGB(0, 0, 0));
		DrawText(hdc, str, -1, &rc, DT_VCENTER | DT_CENTER);
		EndPaint(hwnd, &ps);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_SPACE:
			hInst = (HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE);
			// call to DIalogBox
			DialogBox(hInst, "DATAENTRY", hwnd, MyDlgProc);

			swprintf(strSal, -1, L"%f", theInput.sal);
			wsprintf(str, "User details \n\n Name: %s \n Addr: %s \n Age: %d \n Married: %d \n Salary : %s", theInput.name, theInput.address, theInput.age, theInput.mstatus, TEXT(strSal));
			InvalidateRect(hwnd, &rc, TRUE);
			/*hdc = BeginPaint(hwnd, &ps);
			GetClientRect(hwnd, &rc);
			SetTextColor(hdc, RGB(255, 0, 0));
			SetBkColor(hdc, RGB(0, 20, 255));			
			DrawText(hdc, str, -1, &rc, DT_VCENTER | DT_CENTER | DT_SINGLELINE);
			EndPaint(hwnd, &ps);*/
			break;
		}
		break;

	case WM_DESTROY:
		//EndPaint(hwnd, &ps);
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}


BOOL CALLBACK MyDlgProc(HWND hDlg, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	char salrs[6], salps[3];

	switch (iMsg)
	{
	case WM_INITDIALOG:
		// set focus in name Edit Box
		SetFocus(GetDlgItem(hDlg, ID_ETNAME));
		// keep married radio button checked
		SendDlgItemMessage(hDlg, ID_RBMARRIED, BM_SETCHECK, 1, 0);
		return (TRUE);

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case ID_PBCONTINUE:
			EndDialog(hDlg, 0);
			break;

		case IDOK:
			GetDlgItemText(hDlg, ID_ETNAME, theInput.name, 50);
			GetDlgItemText(hDlg, ID_ETADDRESS, theInput.address, 50);
			theInput.age = GetDlgItemInt(hDlg, ID_ETAGE, NULL, TRUE);
			GetDlgItemText(hDlg, ID_ETSALRS, salrs, 6);
			GetDlgItemText(hDlg, ID_ETSALPS, salps, 3);
			theInput.sal = atoi(salrs) + (float)(atoi(salps) / 100.0f);
			theInput.mstatus = SendDlgItemMessage(hDlg, ID_RBMARRIED, BM_GETCHECK, 0, 0);
			EndDialog(hDlg, 0);
			break;

		case IDCANCEL:
			EndDialog(hDlg, 0);
			break;
		}

		return (TRUE);
	}

	return (FALSE);
}