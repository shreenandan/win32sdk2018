#pragma once
#include "pch.h"
#include "MyPage.h"
using namespace Windows::Foundation;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::Foundation;
using namespace Windows::UI;

MyPage::MyPage()
{
	Window::Current->CoreWindow->KeyDown += ref new 
		TypedEventHandler<CoreWindow^, KeyEventArgs^>(this, &MyPage::OnKeyDown);

	Grid^ grid = ref new Grid();

	this->textBlock = ref new TextBlock();
	textBlock->Text = "My Event Handling";
	textBlock->FontFamily = ref new Windows::UI::Xaml::Media::FontFamily("Segoe UI");
	textBlock->FontSize = 120;
	textBlock->FontStyle = Windows::UI::Text::FontStyle::Oblique;
	textBlock->FontWeight = Windows::UI::Text::FontWeights::Bold;
	textBlock->Foreground = ref new SolidColorBrush(Windows::UI::Colors::Gold);
	textBlock->HorizontalAlignment = Windows::UI::Xaml::HorizontalAlignment::Center;
	textBlock->VerticalAlignment = Windows::UI::Xaml::VerticalAlignment::Center;

	grid->Children->Append(textBlock);

	Button^ btn = ref new Button();
	btn->Content = "Click Me";
	btn->Width = 150;
	btn->Height = 50;
	btn->BorderThickness = 12;
	btn->BorderBrush = ref new SolidColorBrush(Colors::Gold);
	btn->Foreground = ref new SolidColorBrush(Colors::Red);
	btn->FontFamily = ref new Windows::UI::Xaml::Media::FontFamily("Segoe UI");
	btn->FontWeight = Windows::UI::Text::FontWeights::Bold;
	btn->VerticalAlignment = Windows::UI::Xaml::VerticalAlignment::Bottom;
	btn->HorizontalAlignment = Windows::UI::Xaml::HorizontalAlignment::Center;

	btn->Click += ref new RoutedEventHandler(this, &MyPage::OnButtonClick);

	grid->Children->Append(btn);
	this->Content = grid;
}

void MyPage::OnKeyDown(CoreWindow^ sender, KeyEventArgs^ args)
{
	this->textBlock->Text = "Key is pressed";
	//if(args->VirtualKey==VirtualKey:)
}

void MyPage::OnButtonClick(Object^ sender, RoutedEventArgs^ args)
{
	textBlock->Text = "Mouse is clicked";
}
