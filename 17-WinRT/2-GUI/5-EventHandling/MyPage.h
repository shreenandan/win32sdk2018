#pragma once
#include "pch.h"
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Core;
using namespace Windows::UI::Xaml;

ref class MyPage sealed : public Page
{
private:
	TextBlock^ textBlock;
public:
	MyPage();
	void OnKeyDown(CoreWindow^ sender, KeyEventArgs^ args);
	void OnButtonClick(Object^ sender, RoutedEventArgs^ args);
};
