#include "pch.h"
#include "App.h"
using namespace Platform;
#include "MyPage.h"

void MyCallbackMethod(Windows::UI::Xaml::ApplicationInitializationCallbackParams^ params)
{
	App^ app = ref new App();
}

int main(Array<String^>^ args)
{
	Windows::UI::Xaml::ApplicationInitializationCallback^ callback =
		ref new Windows::UI::Xaml::ApplicationInitializationCallback(MyCallbackMethod);

	//App^ app = ref new App();

	App::Start(callback);

	return (0);
}

void App::OnLaunched(Windows::ApplicationModel::Activation::LaunchActivatedEventArgs^ args)
{
	MyPage^ page = ref new MyPage();
	Window::Current->Content = page;
	Window::Current->Activate();
}
