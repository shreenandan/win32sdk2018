﻿//
// MainPage.xaml.cpp
// Implementation of the MainPage class.
//

#include "pch.h"
#include "MainPage.xaml.h"

using namespace _6_EventHandlingWithXaml;

using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409
void MainPage::OnButtonClick(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ args)
{
	if (textBlock->Text == "Hello")
	{
		textBlock->Text = "Go Back";
	}
	else//if (textBlock->Text == "Go Back")
	{
		textBlock->Text = "Hello";
	}
}

void MainPage::OnKeyDown(Windows::UI::Core::CoreWindow^ sender, Windows::UI::Core::KeyEventArgs^ args)
{
	if (args->VirtualKey != Windows::System::VirtualKey::Space && args->VirtualKey != Windows::System::VirtualKey::Enter)
	{
		textBlock->Text = args->VirtualKey.ToString();
	}
}

MainPage::MainPage()
{
	InitializeComponent();
	Window::Current->CoreWindow->KeyDown += ref new Windows::Foundation::TypedEventHandler<Windows::UI::Core::CoreWindow ^, Windows::UI::Core::KeyEventArgs^>(this, &MainPage::OnKeyDown);
}

