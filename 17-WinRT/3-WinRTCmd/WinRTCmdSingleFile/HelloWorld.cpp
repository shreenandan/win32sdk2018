
using namespace Platform;
using namespace Windows::UI::Xaml;

using namespace Windows::ApplicationModel::Activation;

using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Media;

ref class App sealed : public Application
{
public:
	virtual void OnLaunched(LaunchActivatedEventArgs^ args) override;
};

int main(Array<String^>^ args)
{
	Application::Start(ref new ApplicationInitializationCallback(
		[](ApplicationInitializationCallbackParams^ params)
	{
		App^ app = ref new App();
	}));
}

void App::OnLaunched(LaunchActivatedEventArgs^ args)
{
	Page^ page = ref new Page();
	Grid^ grid = ref new Grid();
	TextBlock^ textBlock = ref new TextBlock();

	textBlock->Text = "Hello World to Cmd";
	textBlock->FontFamily = ref new Windows::UI::Xaml::Media::FontFamily("Segoe UI");
	textBlock->FontSize = 40;
	textBlock->FontStyle = Windows::UI::Text::FontStyle::Oblique;
	textBlock->FontWeight = Windows::UI::Text::FontWeights::Bold;
	textBlock->Foreground = ref new SolidColorBrush(Windows::UI::Colors::Gold);
	textBlock->HorizontalAlignment = Windows::UI::Xaml::HorizontalAlignment::Center;
	textBlock->VerticalAlignment = Windows::UI::Xaml::VerticalAlignment::Center;

	grid->Children->Append(textBlock);
	page->Content = grid;
	Window::Current->Content = page;
	Window::Current->Activate();
}