class ISum : public IUnknown
{
public:
	virtual HRESULT __stdcall SumTwoInt(int, int, int *) = 0; // pure virtual
};

class ISub : public IUnknown
{
public:
	virtual HRESULT __stdcall SubTwoInt(int, int, int *) = 0; // pure virtual
};

// CLSID of SumSub component {9161F8D8-6438-4F89-B9BF-C4D5F63BED9A}
const CLSID CLSID_CSumSub = { 0x9161f8d8, 0x6438, 0x4f89, { 0xb9, 0xbf, 0xc4, 0xd5, 0xf6, 0x3b, 0xed, 0x9a } };

// IID of ISum Interface {B5AA6D4B-8918-4923-9F8A-217B0A4D9CD3}
const IID IID_ISum = { 0xb5aa6d4b, 0x8918, 0x4923, { 0x9f, 0x8a, 0x21, 0x7b, 0xa, 0x4d, 0x9c, 0xd3 } };

// IID of ISub Interface {1A5C32F5-A0A9-41D6-87E1-D490E832FE21}
const IID IID_ISub = { 0x1a5c32f5, 0xa0a9, 0x41d6, { 0x87, 0xe1, 0xd4, 0x90, 0xe8, 0x32, 0xfe, 0x21 } };
