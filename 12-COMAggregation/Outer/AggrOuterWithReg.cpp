#define UNICODE
#include <Windows.h>
#include "AggrOuterWithReg.h"
#include "AggrInnerWithReg.h"

// class declarations
// change 1] NO IMultiplication, IDivision
class CSumSub : public ISum, ISub // Adjustor Thunk order is important
{
private:
	long m_cRef;
	IUnknown *m_pIUnknownInner; // change 2] Outer saves inner object
	IMultiplication *m_pIMultiplication;
	IDivision *m_pIDivision;
public:
	CSumSub(void);
	~CSumSub(void);

	// IUnknown specific method declarations (inherited)
	HRESULT __stdcall QueryInterface(REFIID, void **);
	ULONG __stdcall AddRef(void);
	ULONG __stdcall Release(void);

	// ISum specific methods
	HRESULT __stdcall SumTwoInt(int, int, int *);
	// ISub specific methods
	HRESULT __stdcall SubTwoInt(int, int, int *);

	// custom method for inner component creation
	HRESULT __stdcall InitializeInnerComponent();
};

class CSumSubClassFactory : public IClassFactory
{
private:
	long m_cRef;
public:
	CSumSubClassFactory(void);
	~CSumSubClassFactory(void);

	// IUnknown specific method declarations (inherited)
	HRESULT __stdcall QueryInterface(REFIID, void **);
	ULONG __stdcall AddRef(void);
	ULONG __stdcall Release(void);

	// IUnknown specific method declarations (inherited)
	HRESULT __stdcall CreateInstance(IUnknown *, REFIID, void **);
	HRESULT __stdcall LockServer(BOOL);
};

// global variable declarations
long glNoOfActiveComponents = 0;
long glNoOfServerLocks = 0; // no of locks on this dll

BOOL WINAPI DllMain(HINSTANCE hDll, DWORD dwReason, LPVOID reserved)
{
	// code
	switch (dwReason)
	{
		// Why Sir wrote only these two? Remember this thing.
	case DLL_PROCESS_ATTACH:
	case DLL_PROCESS_DETACH:
		break;

	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
		break;
	}

	return (TRUE);
}

// CSumSub Implementation
CSumSub::CSumSub(void)
{
	// code
	// initialization of private data members
	m_pIUnknownInner = NULL; // change 3] A
	m_pIDivision = NULL; // change 3] B
	m_pIMultiplication = NULL; // change 3] B
	m_cRef = 1; // hardcoded initialization to anticipate possible failure of QueryInterface()
	InterlockedIncrement(&glNoOfActiveComponents);
}

CSumSub::~CSumSub(void)
{
	InterlockedDecrement(&glNoOfActiveComponents);

	// change 4] B
	if (m_pIMultiplication)
	{
		m_pIMultiplication->Release();
		m_pIMultiplication = NULL;
	}
	if (m_pIDivision)
	{
		m_pIDivision->Release();
		m_pIDivision = NULL;
	}
	// change 4] A
	if (m_pIUnknownInner)
	{
		m_pIUnknownInner->Release();
		m_pIUnknownInner = NULL;
	}
}

// Implementation of IUnknown's 3 methods
HRESULT CSumSub::QueryInterface(REFIID riid, void **ppv) // ptr to ptr to void
{
	// code
	if (riid == IID_IUnknown) // Order is important, Adjustor Thunk
		*ppv = static_cast<ISum *>(this);
	else if (riid == IID_ISum)
		*ppv = static_cast<ISum *>(this);
	else if (riid == IID_ISub)
		*ppv = static_cast<ISub *>(this);
	// NOTE: Remember Feature "Blind Aggregation" Can give anything from inner but Dangerous
	else if (riid == IID_IMultiplication)
		return (m_pIUnknownInner->QueryInterface(riid, ppv)); // change 5] calls QI_NoAggr
	else if (riid == IID_IDivision)
		return (m_pIUnknownInner->QueryInterface(riid, ppv)); // change 5] calls QI_NoAggr
	else
	{
		*ppv = NULL;
		return (E_NOINTERFACE);
	}

	reinterpret_cast<IUnknown *>(*ppv)->AddRef();
	return (S_OK);
}

ULONG CSumSub::AddRef(void)
{
	InterlockedIncrement(&m_cRef);
	return (m_cRef);
}

ULONG CSumSub::Release(void)
{
	InterlockedDecrement(&m_cRef);

	// logic
	if (m_cRef == 0)
	{
		delete(this);
		return (0);
	}

	return (m_cRef);
}


// Implementation ISum's methods
HRESULT CSumSub::SumTwoInt(int num1, int num2, int *pSum)
{
	*pSum = num1 + num2;
	return (S_OK);
}

// Implementation ISub's methods
HRESULT CSumSub::SubTwoInt(int num1, int num2, int *pSub)
{
	*pSub = num1 - num2;
	return (S_OK);
}


HRESULT CSumSub::InitializeInnerComponent(void)
{
	// variable declarations
	HRESULT hr;
	// code
	// change 6] A Passed 2nd param instead of NULL
	// change 6] B Passed 4th param IUknown
	// change 6] C fill up 5th param m_pIUknownInner
	// NOTE: Is hath se le aur us hath se de
	hr = CoCreateInstance(CLSID_CMultiplicationDivision,
		reinterpret_cast<IUnknown *>(this),
		CLSCTX_INPROC_SERVER,
		IID_IUnknown,
		(void **)&m_pIUnknownInner);

	// change 6] D
	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("IUnknown interface can not be obtained from inner component."), TEXT("Error"), MB_OK);
		return(E_FAIL);
	}

	// change 6] D
	hr = m_pIUnknownInner->QueryInterface(IID_IMultiplication, (void **)&m_pIMultiplication);
	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("IMultiplication interface can not be obtained from inner component."), TEXT("Error"), MB_OK);
		m_pIUnknownInner->Release();
		m_pIUnknownInner = NULL;
		return(E_FAIL);
	}

	// change 6] D
	hr = m_pIUnknownInner->QueryInterface(IID_IDivision, (void **)&m_pIDivision);
	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("IDivision interface can not be obtained from inner component."), TEXT("Error"), MB_OK);
		m_pIUnknownInner->Release();
		m_pIUnknownInner = NULL; 
		return(E_FAIL);
	}

	return(S_OK);
}


// Class Factory Implementation
CSumSubClassFactory::CSumSubClassFactory(void)
{
	m_cRef = 1; // hardcoded initialization to anticipate possible failure of QueryInterface()
}

CSumSubClassFactory::~CSumSubClassFactory(void)
{

}

// Implementation of IUnknown's 3 methods
HRESULT CSumSubClassFactory::QueryInterface(REFIID riid, void **ppv) // ptr to ptr to void
{
	// code
	if (riid == IID_IUnknown) // Order is important, Adjustor Thunk
		*ppv = static_cast<IClassFactory *>(this);
	else if (riid == IID_IClassFactory)
		*ppv = static_cast<IClassFactory *>(this);
	else
	{
		*ppv = NULL;
		return (E_NOINTERFACE);
	}

	reinterpret_cast<IUnknown *>(*ppv)->AddRef();
	return (S_OK);
}

ULONG CSumSubClassFactory::AddRef(void)
{
	InterlockedIncrement(&m_cRef);
	return (m_cRef);
}

ULONG CSumSubClassFactory::Release(void)
{
	InterlockedDecrement(&m_cRef);

	// logic
	if (m_cRef == 0)
	{
		delete(this);
		return (0);
	}

	return (m_cRef);
}


// Implementation of IClassFacory's 2 methods
HRESULT CSumSubClassFactory::CreateInstance(IUnknown *pUnkOuter, REFIID riid, void **ppv)
{
	CSumSub *pCSumSub = NULL;
	HRESULT hr;

	if (pUnkOuter != NULL) // NOTE
		return (CLASS_E_NOAGGREGATION);

	// create the instance of component
	pCSumSub = new CSumSub;
	if (pCSumSub == NULL)
	{
		return (E_OUTOFMEMORY);
	}

	hr = pCSumSub->InitializeInnerComponent(); // NOTE
	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("Failed to initialize inner component."), TEXT("Error"), MB_OK);
		pCSumSub->Release();
		return(hr);
	}

	// get the requested interface
	hr = pCSumSub->QueryInterface(riid, ppv);
	pCSumSub->Release(); // anticipate possible failure of QueryInterface()
	return (hr);
}

HRESULT CSumSubClassFactory::LockServer(BOOL fLock)
{
	if (fLock)
		InterlockedIncrement(&glNoOfServerLocks);
	else
		InterlockedDecrement(&glNoOfServerLocks);

	return (S_OK);
}


// Implementation of gloabl Exported functions from this DLL
// extern "C" required from 2000 onwards
extern "C" HRESULT __stdcall DllGetClassObject(REFCLSID rclsid, REFIID riid, void **ppv)
{
	CSumSubClassFactory *pCSumSubClassFactory = NULL;
	HRESULT hr;

	if (rclsid != CLSID_CSumSub)
		return (CLASS_E_CLASSNOTAVAILABLE);

	// create class factory
	pCSumSubClassFactory = new CSumSubClassFactory;
	if (pCSumSubClassFactory == NULL)
		return (E_OUTOFMEMORY);

	hr = pCSumSubClassFactory->QueryInterface(riid, ppv);
	pCSumSubClassFactory->Release(); // anticipate possible failure of QueryInterface()

	return (hr);
}

extern "C" HRESULT __stdcall DllCanUnloadNow(void)
{
	if (glNoOfActiveComponents == 0 && glNoOfServerLocks == 0)
		return (S_OK);
	else
		return (S_FALSE);
}
