#define UNICODE
#include <Windows.h>
#include "AggrInnerWithReg.h"

// change 1]
// interface declaration (for internal use only, not to be included in .h file)
interface INoAggregationIUnknown
{
	// NOTE: no public specifier
	virtual HRESULT __stdcall QueryInterface_NoAggregation(REFIID, void **) = 0;
	virtual ULONG __stdcall AddRef_NoAggregation(void) = 0;
	virtual ULONG __stdcall Release_NoAggregation(void) = 0;
};

// class declarations
// change 2] 1st Adjustor's Thunk
class CMultiplicationDivision : public INoAggregationIUnknown, IMultiplication, IDivision
{
private:
	long m_cRef;
	IUnknown *m_pIUnknownOuter; // change 3]
public:
	CMultiplicationDivision(IUnknown *); // Change 4] A Parameterized ctor
	~CMultiplicationDivision(void);

	// IUnknown specific method declarations (inherited)
	HRESULT __stdcall QueryInterface(REFIID, void **);
	ULONG __stdcall AddRef(void);
	ULONG __stdcall Release(void);

	// change 4] B
	// Aggregation non supported IUnknown specific method declarations (inherited)
	HRESULT __stdcall QueryInterface_NoAggregation(REFIID, void **);
	ULONG __stdcall AddRef_NoAggregation(void);
	ULONG __stdcall Release_NoAggregation(void);

	// IMultiplication specific methods
	HRESULT __stdcall MultiplyTwoInt(int, int, int *);

	// IDivision specific methods
	HRESULT __stdcall DivisionTwoInt(int, int, int *);
};

class CMultiplicationDivisionClassFactory : public IClassFactory
{
private:
	long m_cRef;
public:
	CMultiplicationDivisionClassFactory(void);
	~CMultiplicationDivisionClassFactory(void);

	// IUnknown specific method declarations (inherited)
	HRESULT __stdcall QueryInterface(REFIID, void **);
	ULONG __stdcall AddRef(void);
	ULONG __stdcall Release(void);

	// IUnknown specific method declarations (inherited)
	HRESULT __stdcall CreateInstance(IUnknown *, REFIID, void **);
	HRESULT __stdcall LockServer(BOOL);
};

// global variable declarations
long glNoOfActiveComponents = 0;
long glNoOfServerLocks = 0; // no of locks on this dll

BOOL WINAPI DllMain(HINSTANCE hDll, DWORD dwReason, LPVOID reserved)
{
	// code
	switch (dwReason)
	{
		// Why Sir wrote only these two? Remember this thing.
	case DLL_PROCESS_ATTACH:
	case DLL_PROCESS_DETACH:
		break;

	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
		break;
	}

	return (TRUE);
}

// CMultiplicationDivision Implementation
CMultiplicationDivision::CMultiplicationDivision(IUnknown *pIUnknownOuter)
{
	m_cRef = 1; // hardcoded initialization to anticipate possible failure of QueryInterface()
	InterlockedIncrement(&glNoOfActiveComponents);

	// change 5]
	// NOTE Logic keep outer object saved in inner
	if (pIUnknownOuter != NULL)
		m_pIUnknownOuter = pIUnknownOuter;
	else
		m_pIUnknownOuter = reinterpret_cast<IUnknown *>(static_cast<INoAggregationIUnknown *>(this));
}

CMultiplicationDivision::~CMultiplicationDivision(void)
{
	InterlockedDecrement(&glNoOfActiveComponents);
}


// Implementation of Aggregation Supporting IUnknown's 3 methods. Delegate call 
// change 6]
HRESULT CMultiplicationDivision::QueryInterface(REFIID riid, void **ppv) // ptr to ptr to void
{
	return (m_pIUnknownOuter->QueryInterface(riid, ppv));
}
// change 7]
ULONG CMultiplicationDivision::AddRef(void)
{
	return (m_pIUnknownOuter->AddRef());	
}
// change 8]
ULONG CMultiplicationDivision::Release(void)
{
	return (m_pIUnknownOuter->Release());
}


// Implementation of Aggregation NON Supporting IUnknown's 3 methods
HRESULT CMultiplicationDivision::QueryInterface_NoAggregation(REFIID riid, void **ppv) // ptr to ptr to void
{
	// code
	if (riid == IID_IUnknown) // Order is important, Adjustor Thunk
		*ppv = static_cast<INoAggregationIUnknown *>(this); // Change 9] Method Coloring
	else if (riid == IID_IMultiplication)
		*ppv = static_cast<IMultiplication *>(this);
	else if (riid == IID_IDivision)
		*ppv = static_cast<IDivision *>(this);
	else
	{
		*ppv = NULL;
		return (E_NOINTERFACE);
	}

	reinterpret_cast<IUnknown *>(*ppv)->AddRef();
	return (S_OK);
}

// Change 10]
ULONG CMultiplicationDivision::AddRef_NoAggregation(void)
{
	InterlockedIncrement(&m_cRef);
	return (m_cRef);
}

// Change 11]
ULONG CMultiplicationDivision::Release_NoAggregation(void)
{
	InterlockedDecrement(&m_cRef);

	// logic
	if (m_cRef == 0)
	{
		delete(this);
		return (0);
	}

	return (m_cRef);
}

// Implementation IMultiplication's methods
HRESULT CMultiplicationDivision::MultiplyTwoInt(int num1, int num2, int *pMul)
{
	*pMul = num1 * num2;
	return (S_OK);
}

// Implementation IDivision's methods
HRESULT CMultiplicationDivision::DivisionTwoInt(int num1, int num2, int *pDiv)
{
	*pDiv = num1 / num2;
	return (S_OK);
}


// Class Factory Implementation
CMultiplicationDivisionClassFactory::CMultiplicationDivisionClassFactory(void)
{
	m_cRef = 1; // hardcoded initialization to anticipate possible failure of QueryInterface()
}

CMultiplicationDivisionClassFactory::~CMultiplicationDivisionClassFactory(void)
{

}

// Implementation of IUnknown's 3 methods
HRESULT CMultiplicationDivisionClassFactory::QueryInterface(REFIID riid, void **ppv) // ptr to ptr to void
{
	// code
	if (riid == IID_IUnknown) // Order is important, Adjustor Thunk
		*ppv = static_cast<IClassFactory *>(this);
	else if (riid == IID_IClassFactory)
		*ppv = static_cast<IClassFactory *>(this);
	else
	{
		*ppv = NULL;
		return (E_NOINTERFACE);
	}

	reinterpret_cast<IUnknown *>(*ppv)->AddRef();
	return (S_OK);
}

ULONG CMultiplicationDivisionClassFactory::AddRef(void)
{
	InterlockedIncrement(&m_cRef);
	return (m_cRef);
}

ULONG CMultiplicationDivisionClassFactory::Release(void)
{
	InterlockedDecrement(&m_cRef);

	// logic
	if (m_cRef == 0)
	{
		delete(this);
		return (0);
	}

	return (m_cRef);
}


// Implementation of IClassFacory's 2 methods
HRESULT CMultiplicationDivisionClassFactory::CreateInstance(IUnknown *pUnkOuter, REFIID riid, void **ppv)
{
	CMultiplicationDivision *pCMultiplicationDivision = NULL;
	HRESULT hr;

	// change 12] A
	if ((pUnkOuter != NULL) && (riid != IID_IUnknown))
		return (CLASS_E_NOAGGREGATION);

	// create the instance of component
	// change 12] B
	pCMultiplicationDivision = new CMultiplicationDivision(pUnkOuter);
	if (pCMultiplicationDivision == NULL)
	{
		return (E_OUTOFMEMORY);
	}

	// get the requested interface
	// change 12] C _noAgg
	hr = pCMultiplicationDivision->QueryInterface_NoAggregation(riid, ppv);
	pCMultiplicationDivision->Release_NoAggregation(); // anticipate possible failure of QueryInterface()
	return (hr);
}

HRESULT CMultiplicationDivisionClassFactory::LockServer(BOOL fLock)
{
	if (fLock)
		InterlockedIncrement(&glNoOfServerLocks);
	else
		InterlockedDecrement(&glNoOfServerLocks);

	return (S_OK);
}


// Implementation of gloabl Exported functions from this DLL
// extern "C" required from 2000 onwards
extern "C" HRESULT __stdcall DllGetClassObject(REFCLSID rclsid, REFIID riid, void **ppv)
{
	CMultiplicationDivisionClassFactory *pCMultiplicationDivisionClassFactory = NULL;
	HRESULT hr;

	if (rclsid != CLSID_CMultiplicationDivision)
		return (CLASS_E_CLASSNOTAVAILABLE);

	// create class factory
	pCMultiplicationDivisionClassFactory = new CMultiplicationDivisionClassFactory;
	if (pCMultiplicationDivisionClassFactory == NULL)
		return (E_OUTOFMEMORY);

	hr = pCMultiplicationDivisionClassFactory->QueryInterface(riid, ppv);
	pCMultiplicationDivisionClassFactory->Release(); // anticipate possible failure of QueryInterface()

	return (hr);
}

extern "C" HRESULT __stdcall DllCanUnloadNow(void)
{
	if (glNoOfActiveComponents == 0 && glNoOfServerLocks == 0)
		return (S_OK);
	else
		return (S_FALSE);
}
