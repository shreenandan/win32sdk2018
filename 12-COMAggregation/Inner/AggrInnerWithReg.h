class IMultiplication : public IUnknown
{
public:
	virtual HRESULT __stdcall MultiplyTwoInt(int, int, int *) = 0; // pure virtual
};

class IDivision : public IUnknown
{
public:
	virtual HRESULT __stdcall DivisionTwoInt(int, int, int *) = 0; // pure virtual
};

// NOTE: In Aggregation, can give below CLSID of inner to client
// CLSID of MultiplicationDivision component {9D16D1DE-3022-4B39-AA51-A20ADE0264CA}
const CLSID CLSID_CMultiplicationDivision = { 0x9d16d1de, 0x3022, 0x4b39, { 0xaa, 0x51, 0xa2, 0xa, 0xde, 0x2, 0x64, 0xca } };

// IID of IMultiplication Interface {ED84FFB5-9445-4929-88A2-C218319BC594}
const IID IID_IMultiplication = { 0xed84ffb5, 0x9445, 0x4929, { 0x88, 0xa2, 0xc2, 0x18, 0x31, 0x9b, 0xc5, 0x94 } };

// IID of IDivision Interface {7516A1B8-DB2A-4C84-AA3E-5382C451FD1F}
const IID IID_IDivision = { 0x7516a1b8, 0xdb2a, 0x4c84, { 0xaa, 0x3e, 0x53, 0x82, 0xc4, 0x51, 0xfd, 0x1f } };
