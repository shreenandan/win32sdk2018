#define UNICODE
#include <Windows.h>
#include "HeaderAggrClientWithReg.h"

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// global variable declarations
ISum *pISum = NULL;
ISub *pISub = NULL;
IMultiplication *pIMultiplication = NULL; // NOTE 
IDivision *pIDivision = NULL; // NOTE 

// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("AggregationDllClient");
	HRESULT hr; // New wrt COM interface

	// COM Initialization
	hr = CoInitialize(NULL);
	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("COM library can not be initialized.\nProgram will now exit."), TEXT("Program Error"), MB_OK);
		exit(0);
	}

	// code
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc; // NOTE
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindow(szAppName,
		TEXT("Aggregation Test DllClient"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	// message loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	// COM UnInitialization
	CoUninitialize();

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declarations
	void SafeInterfaceRelease(void);
	// Variable declarations
	HRESULT hr;
	int iNum1, iNum2, iRes;
	TCHAR str[255];

	// code
	switch (iMsg)
	{
	case WM_CREATE:
		hr = CoCreateInstance(CLSID_CSumSub, NULL, CLSCTX_INPROC_SERVER, IID_ISum, (void **)&pISum);
		if (FAILED(hr))
		{
			MessageBox(hwnd, TEXT("ISum interface can not be obtained."), TEXT("Error"), MB_OK);
			DestroyWindow(hwnd);
		}

		iNum1 = 50;
		iNum2 = 20;
		pISum->SumTwoInt(iNum1, iNum2, &iRes);
		wsprintf(str, TEXT("Sum of %d and %d = %d"), iNum1, iNum2, iRes);
		MessageBox(hwnd, str, TEXT("Sum"), MB_OK);

		hr = pISum->QueryInterface(IID_ISub, (void **)&pISub);
		if (FAILED(hr))
		{
			MessageBox(hwnd, TEXT("ISub interface can not be obtained."), TEXT("Error"), MB_OK);
			DestroyWindow(hwnd);
		}
		// as ISum is not needed onwards, release it
		pISum->Release();
		pISum = NULL; // make released interface NULL


		pISub->SubTwoInt(iNum1, iNum2, &iRes);
		wsprintf(str, TEXT("Sub of %d and %d = %d"), iNum1, iNum2, iRes);
		MessageBox(hwnd, str, TEXT("Sub"), MB_OK);

		hr = pISub->QueryInterface(IID_IMultiplication, (void **)&pIMultiplication);
		if (FAILED(hr))
		{
			MessageBox(hwnd, TEXT("IMultiplication interface can not be obtained."), TEXT("Error"), MB_OK);
			DestroyWindow(hwnd);
		}
		pISub->Release();
		pISub = NULL;

		pIMultiplication->MultiplyTwoInt(iNum1, iNum2, &iRes);
		wsprintf(str, TEXT("Mult of %d and %d = %d"), iNum1, iNum2, iRes);
		MessageBox(hwnd, str, TEXT("Mult"), MB_OK);

		hr = pIMultiplication->QueryInterface(IID_IDivision, (void **)&pIDivision);
		if (FAILED(hr))
		{
			MessageBox(hwnd, TEXT("IDivision interface can not be obtained."), TEXT("Error"), MB_OK);
			DestroyWindow(hwnd);
		}
		pIMultiplication->Release();
		pIMultiplication = NULL;

		pIDivision->DivisionTwoInt(iNum1, iNum2, &iRes);
		wsprintf(str, TEXT("Division of %d and %d = %d"), iNum1, iNum2, iRes);
		MessageBox(hwnd, str, TEXT("Div"), MB_OK);
		pIDivision->Release();
		pIDivision = NULL;

		DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		SafeInterfaceRelease();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void SafeInterfaceRelease(void)
{
	if (pISum)
	{
		pISum->Release();
		pISum = NULL;
	}
	if (pISub)
	{
		pISub->Release();
		pISub = NULL;
	}
	if (pIMultiplication)
	{
		pIMultiplication->Release();
		pIMultiplication = NULL;
	}
	if (pIDivision)
	{
		pIDivision->Release();
		pIDivision = NULL;
	}
}
