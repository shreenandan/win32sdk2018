// headers
#include<windows.h>

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("ShreeMsgBox");

	// code
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc; // NOTE
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindow(szAppName,
		TEXT("Shree MsgBox"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	// message loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	char theMessage[45];

	// code
	switch (iMsg)
	{
	case WM_CREATE:
		wsprintf(theMessage, "Received message = WM_CREATE");
		MessageBox(hwnd, theMessage, TEXT("My MsgBx"), MB_OK | MB_ICONEXCLAMATION);
		break;

	case WM_LBUTTONDOWN:
		wsprintf(theMessage, "Received message = WM_LBUTTONDOWN");
		MessageBox(hwnd, theMessage, TEXT("My MsgBx"), MB_OKCANCEL | MB_ICONERROR);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
			//case 'L':
		case 0x4C:
			wsprintf(theMessage, "Received message = WM_KEYDOWN");
			MessageBox(hwnd, theMessage, TEXT("My MsgBx"), MB_ABORTRETRYIGNORE | MB_ICONQUESTION);
			break;
		}
		break;

	case WM_MOVE:
		wsprintf(theMessage, "Received message = WM_MOVE");
		MessageBox(hwnd, theMessage, TEXT("My MsgBx"), MB_RETRYCANCEL | MB_ICONWARNING);
		break;

	case WM_SIZE:
		wsprintf(theMessage, "Received message = WM_SIZE");
		MessageBox(hwnd, theMessage, TEXT("My MsgBx"), MB_YESNOCANCEL | MB_ICONINFORMATION);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}