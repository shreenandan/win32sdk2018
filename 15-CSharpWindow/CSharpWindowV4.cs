using System; // stdio.h
using System.Drawing; // gdi.h
using System.Windows.Forms; // Windows.h

public class CSharpWindow : Form
{
	public static void Main()
	{
		Application.Run(new CSharpWindow());
	}
	
	// ctor
	public CSharpWindow()
	{
		// Register, add new with existing
		this.KeyDown += new KeyEventHandler(MyKeyDown);
		this.MouseDown += new MouseEventHandler(MyMouseDown);
	
		Width = 800; // CreateWindow 6th param
		Height = 600; // CreateWindow 7th param
		
		BackColor = Color.Black; // GetStockObject
		
		ResizeRedraw = true; // CS_HREDRAW, CS_VREDRAW
		
		Icon = new Icon("Postman.ico"); // smIcon if csc.exe /win32icon:Postman.ico CSharpWindowV4.cs
	}
	
	// WM_PAINT, Paint = iMsg, EventArgs = wParam and lParam
	protected override void OnPaint(PaintEventArgs pea)
	{
		Graphics grfx = pea.Graphics; // PaintStruct ps
		
		StringFormat strFmt = new StringFormat();
		strFmt.Alignment = StringAlignment.Center; // DT_VCENTER
		strFmt.LineAlignment = StringAlignment.Center; // DT_HCENTER
		
		grfx.DrawString("Hello World!!!",
			Font, // Default system font
			new SolidBrush(System.Drawing.Color.Green), // SetTextColor
			ClientRectangle, // &rc
			strFmt);
	}
	
	// HWND, Key = iMsg, EventArgs = wParam, lParam
	void MyKeyDown(Object sender, KeyEventArgs e)
	{
		MessageBox.Show("Some key pressed");
		// Default OK button, MB_OK, Title, hwnd
	}
	
	void MyMouseDown(Object sender, MouseEventArgs e)
	{
		MessageBox.Show("Some mouse button clicked");
	}
}