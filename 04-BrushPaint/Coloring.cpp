/*
Assignment No: 4
KeyPress Colour Changing
Changing Colour with respect to Key Press (R,G,B,C,M,Y,K)
*/

// headers
#include<windows.h>

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("ShreeBrushPaint");

	// code
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc; // NOTE
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindow(szAppName,
		TEXT("Shree Brush Painting"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	// message loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// variable declarations
	HDC hdc;
	PAINTSTRUCT ps;
	RECT rc;
	HBRUSH myBrush;
	enum BackgroundColors
	{
		GREY,
		RED,
		GREEN,
		BLUE,
		CYAN,
		MAGENTA,
		YELLOW,
		BLACK
	};
	static BackgroundColors keyPressed; // Required to maintain value across calls
	BOOL rt_rc;
	HGDIOBJ rt_brsh;
	int rt_rect;
	BOOL rt_edPt;
	BOOL rt_Del;

	// code
	switch (iMsg)
	{
	case WM_PAINT:
		hdc = BeginPaint(hwnd, &ps);
		rt_rc = GetClientRect(hwnd, &rc);
		switch (keyPressed)
		{
		case RED:
			myBrush = CreateSolidBrush(RGB(255, 0, 0));
			break;
		case GREEN:
			myBrush = CreateSolidBrush(RGB(0, 255, 0));
			break;
		case BLUE:
			myBrush = CreateSolidBrush(RGB(0, 0, 255));
			break;
		case CYAN:
			myBrush = CreateSolidBrush(RGB(0, 255, 255));
			break;
		case MAGENTA:
			myBrush = CreateSolidBrush(RGB(255, 0, 255));
			break;
		case YELLOW:
			myBrush = CreateSolidBrush(RGB(255, 255, 0));
			break;
		case BLACK:
			myBrush = CreateSolidBrush(BLACK_BRUSH); // Both RGB(0,0,0) and BLACK_BRUSH works
			break;

		// By default color should be Grey
		case GREY:
		default:
			myBrush = CreateSolidBrush(RGB(128, 128, 128));
			//myBrush = CreateSolidBrush(GRAY_BRUSH); // NOTE: This does not work. Use RGB
			break;
		}
		rt_brsh = SelectObject(hdc, myBrush);
		rt_rect = FillRect(hdc, &rc, myBrush);
		rt_edPt = EndPaint(hwnd, &ps);
		rt_Del = DeleteObject(myBrush);
		break;

	case WM_KEYDOWN:
		rt_rc = GetClientRect(hwnd, &rc); // NOTE: This is important. Without this rc is garbage.
		switch (wParam)
		{
		case 'R':
			keyPressed = RED;
			break;

		case 'G':
			keyPressed = GREEN;
			break;

		case 'B':
			keyPressed = BLUE;
			break;

		case 'C':
			keyPressed = CYAN;
			break;

		case 'M':
			keyPressed = MAGENTA;
			break;

		case 'Y':
			keyPressed = YELLOW;
			break;

		case 'K':
			keyPressed = BLACK;
			break;

		default:
			keyPressed = GREY;
			break;
		}

		// NOTE: InvalidateRect means discard old painted rectangle and update with new one.
		// Sends the WM_ERASEBKGND and WM_NCPAINT messages before the function returns.
		InvalidateRect(hwnd, &rc, true);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}
