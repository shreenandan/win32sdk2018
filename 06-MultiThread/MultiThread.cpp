// headers
#include<windows.h>
#include<tchar.h>

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

DWORD WINAPI MyThreadProcInc(LPVOID param);
DWORD WINAPI MyThreadProcDec(LPVOID param);

// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("ShreeMultiThread");

	// code
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc; // NOTE
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindow(szAppName,
		TEXT("ShreeMultiThread"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	// message loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	static HANDLE hThread1=NULL, hThread2=NULL;
	DWORD dwID1, dwID2;
	DWORD threadProcOne;

	// code
	switch (iMsg)
	{
	case WM_CREATE:
		hThread1 = CreateThread(NULL,
			0,
			(LPTHREAD_START_ROUTINE)MyThreadProcInc,
			(LPVOID)hwnd,
			0,
			&dwID1);

		if (hThread1==NULL)
		{
			MessageBox(hwnd, TEXT("Thread 1 creation failed"), "Error", MB_ICONERROR | MB_OK);
			DestroyWindow(hwnd);
		}

		hThread2 = CreateThread(NULL,
			0,
			(LPTHREAD_START_ROUTINE)MyThreadProcDec,
			(LPVOID)hwnd,
			0,
			&dwID2);

		if (hThread2 == NULL)
		{
			MessageBox(hwnd, TEXT("Thread 2 creation failed"), "Error", MB_ICONERROR | MB_OK);
			DestroyWindow(hwnd);
		}
		break;

	case WM_LBUTTONDOWN:
		MessageBox(hwnd, TEXT("You clicked left mouse button"), "Click", MB_OK);
		break;

	case WM_DESTROY:
		CloseHandle(hThread1);
		CloseHandle(hThread2);
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

DWORD WINAPI MyThreadProcInc(LPVOID param)
{
	HWND hwnd;
	HDC hdc;
	int i;
	TCHAR str[255];

	hwnd = (HWND)param;
	hdc = GetDC(hwnd);

	for (i = 0; i <= 327670; i++)
	{
		wsprintf(str, TEXT("Thread 1 increamenting : %d"), i);
		TextOut(hdc, 5, 5, str, _tcslen(str));
	}

	ReleaseDC(hwnd, hdc);
	return(0);
}

DWORD WINAPI MyThreadProcDec(LPVOID param)
{
	HWND hwnd;
	HDC hdc;
	int i;
	TCHAR str[255];

	hwnd = (HWND)param;
	hdc = GetDC(hwnd);

	for (i = 327670; i >= 0; i--)
	{
		wsprintf(str, TEXT("Thread 2 decreamenting : %d"), i);
		TextOut(hdc, 5, 25, str, _tcslen(str));
	}

	ReleaseDC(hwnd, hdc);
	return(0);
}